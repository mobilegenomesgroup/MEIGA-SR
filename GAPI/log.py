'''
Module 'log' - Contains functions to report log information
'''

## DEPENDENCIES ##
# External
import logging
import multiprocessing as mp


## FUNCTIONS ##

def header(string):
    '''
    Display  subheader
    '''
    logger = getLogger()
    logger.info('######### ' +  string + ' #########')

def subHeader(string):
    '''
    Display  subheader
    '''
    logger = getLogger()
    logger.info( '** ' + string + ' **')

def info(string):
    '''
    Display basic information
    '''
    logger = getLogger()
    logger.info(string)

def step(label, string):
    '''
    Display labelled information
    '''
    logger = getLogger()
    logger.info('[' + label + ']' + string)

def filter_info(string):
    '''
    Display information from filtered clusters
    '''
    logger = getLogger(logType='filter')
    logger.info(string)

def getLogger(logType='general'):
    '''
    Get logger of a given loggerType for each running proccess
    
    Input:
        1. logType: type of logger. 'general' or 'filter' 
    
    Output:
        1. logger: logger object (from logging library)
    '''
    # if general logger requested
    if logType == "general":
        logName = mp.current_process().name.replace("SpawnPoolWorker-", "general_process_")
    
    # else: filter logger  
    else:
        logName = mp.current_process().name.replace("SpawnPoolWorker-", "filter_process_")    

    # get logger
    logger = logging.getLogger(logName)
    
    return logger

def setup_logger(logName, logFile, formatter, level=logging.INFO, consoleLevel=logging.WARNING):
    '''
    Create logger
    
    Input:
        1. logName: logger name
        2. logFile: output file
        3. formatter: output formatter
        4. level: level of verbosity
    
    Output:
        1. logger: logger object (from logging library)
    '''
    ### 1. Setup a logger
    logger = logging.getLogger(logName)
    # set up verbosity level
    logger.setLevel(level)
    
    ## A. Create a file handler 
    # set up handler
    handler = logging.FileHandler(logFile, 'a')
    # set up format
    handler.setFormatter(logging.Formatter(formatter))
    
    ## B. Create a console handler 
    consoleHandler = logging.StreamHandler()
    # set up verbosity level
    consoleHandler.setLevel(consoleLevel)
    # set up format 
    consoleHandler.setFormatter(logging.Formatter(formatter))
    
    ## C. Check if the handlers already exist
    newHandler, newConsoleHandler = True, True
    
    for iHandler in logger.handlers:
        
        if str(handler) == str(iHandler):
            newHandler = False
            
        if str(consoleHandler) == str(iHandler):
            newConsoleHandler = False
    
    # if they don't exist, add them to the logger
    if newHandler: logger.addHandler(handler)
    if newConsoleHandler: logger.addHandler(consoleHandler)

    return logger

def remove_logger(logger):
    '''
    Remove logger
    
    Input:
        1. logger: logger 
        
    Output:
        None
    '''
    # clean up file handlers
    for iHandler in logger.handlers:
        
        iHandler.close()
        logger.removeHandler(iHandler)
        del iHandler
        
    del logger
