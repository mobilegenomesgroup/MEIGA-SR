'''
Module 'alignment' - Contains funtions align sequences
'''

## DEPENDENCIES ##
# External
import subprocess

# Internal
from GAPI import log
from GAPI import unix
from GAPI import bamtools 
from GAPI import formats


## FUNCTIONS ##
def index_bwa(fastaPath, outDir):
    '''
    Wrapper to generate BWA index for fasta file
    '''
    err = open(outDir + '/index.err', 'w') 
    command = 'bwa index ' + fastaPath 
    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'INDEX'
        msg = 'BWA indexing failed' 
        log.step(step, msg)
    
def alignment_bwa(FASTA, reference, fileName, processes, outDir):
    '''
    Align a set of sequence into a reference with bwa mem

    Input:
        1. FASTA: Path to FASTA file with sequences to align
        2. reference: Path to the reference genome in fasta format (bwa mem index must be located in the same folder)
        3. fileName: output file will be named accordingly
        4. processes: Number of processes used by bwa mem
        5. outDir: Output directory

    Output:
        1. SAM: Path to SAM file containing input sequences alignments or 'None' if alignment failed 
    '''
    ## Align the sequences into the reference
    SAM = outDir + '/' + fileName + '.sam'
    err = open(outDir + '/align.err', 'w') 
    command = 'bwa mem -Y -t ' + str(processes) + ' ' + reference + ' ' + FASTA + ' > ' + SAM
    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'ALIGN'
        msg = 'Alignment failed' 
        log.step(step, msg)

    return SAM

def alignment_blat(FASTA, reference, args, fileName, outDir):
    '''    
    Align a set of sequence into a reference with blat

    Input:
        1. FASTA: Path to FASTA file with sequences to align
        2. reference: Path to the reference genome in fasta format (bwa mem index must be located in the same folder)
        3. args: dictionary containing blat arguments
        4. fileName: output file will be named accordingly
        5. outDir: Output directory

    Output:
        1. SAM: Path to SAM file containing input sequences alignments or 'None' if alignment failed 
    '''
    ## Align the sequences into the reference
    PSL = outDir + '/' + fileName + '.psl'
    err = open(outDir + '/align.err', 'w') 

    # Set blat arguments
    blatArgs = []

    if 'stepSize' in args.keys():
        blatArgs.append('-stepSize='+str(args['stepSize']))

    if 'tileSize' in args.keys():
        blatArgs.append('-tileSize='+str(args['tileSize']))

    command = 'blat ' + ' '.join(blatArgs) + ' -repMatch=2253 -minScore=20 -minIdentity=0 -noHead -out=psl ' + reference + ' ' + FASTA + ' ' + PSL
    status = subprocess.call(command, stderr=err, shell=True)

    if status != 0:
        step = 'ALIGN'
        msg = 'Alignment failed' 
        log.step(step, msg)

    return PSL

