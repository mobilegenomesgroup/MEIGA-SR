'''
Module 'check_dependencies' - Contains functions to check if all program dependencies are satisfied 

 A) Python modules
 B) External programs
 C) Databases

'''

## DEPENDENCIES ##
# External
import pkg_resources
import shutil
import os


## FUNCTIONS ##
def missing_python_dependencies():
    '''
    Check if all the python dependencies are satisfied
    
    Output: 
    1. isMissing: Boolean
    ''' 
    ## 0. Set variables
    isMissing = False
    
    ## 1. List required packages
    requiredPkgs = ["numpy", "scipy", "pysam", "mappy", "cigar", "pybedtools", "biopython", "scikit-learn", "pandas"]

    ## 2. List installed packages
    installedPkgs = [str(pkg).split(" ")[0] for pkg in pkg_resources.working_set]
        
    ## 3. Get missing packages
    missingPkgs = set(requiredPkgs) - set(installedPkgs)

    # if missingPkgs
    if bool(missingPkgs):
        
        isMissing = True
        
        # print error messages
        print("\n\n\n*** ERROR *** MEIGA-SR dependencies not satisfied.\nSome Python packages are missing. Install them with: ")
        for pkg in missingPkgs: print("pip3 install " + pkg) 
        print("\n\n")
        
    return isMissing

def missing_program_dependencies(): 
    '''
    Check if all program dependencies are satisfied
    Both binaries and environment variables are checked
    
    Output: 
    1. isMissing: Boolean
    '''
    ## 0. Set variables
    isMissing = False
    
    ## 1. List required programs: binaries and environment variables
    bins = ["samtools", "bedtools", "perl", "bwa", "blat"]
    envVars = ["ANNOVAR"]

    ## 2. Get missing programs
    missingBins = [bin for bin in bins if not shutil.which(bin)]
    missingVars = [var for var in envVars if not var in os.environ.keys()]

    ## 3. If any program is missing 
    if missingBins or missingVars:
        
        isMissing = True
        print("\n\n\n*** ERROR *** MEIGA-SR dependencies not satisfied")
        
    # if missing bins
    if missingBins: 
        
        print("The following binaries are missing: ")
        for bin in missingBins: print(bin)

    # if missing vars
    if missingVars: 
        
        print("The following environment variables are missing: ")
        for var in missingVars: print(var)
    
    return isMissing

def missing_db(refDir, annovarDir):
    '''
    Check if program databases are satisfied
    
    Input:
    1. refDir: Directory containing reference databases (consensus sequences, source elements...)
    2. annovarDir: Directory containing annovar reference files
    
    Output: 
    1. isMissing: Boolean
    '''
    ## 0. Set variables
    isMissing = False

    ## 1. List required databases
    refFiles = ['repeats.bed', 'retrotransposons.bed', 'polyA.bed', 'srcElements.bed', 'exons.bed']
    annovarFiles = ['build_annot.txt', 'build_annotMrna.fa']

    ## 2. Check if any is missing in the dirs provided
    missing_refFiles = [os.path.exists(refDir + '/' + file) for file in refFiles]
    missing_annovarFiles = [os.path.exists(annovarDir + '/' + file) for file in annovarFiles]

    ## 3. If any database is missing 
    if not all(missing_refFiles):
        
        isMissing = True
        
        # print error message
        print("\n\n\n*** ERROR *** MEIGA-SR database dependencies not satisfied.\nSome of the following REF files couldn't be located at", refDir)
        for file in refFiles: print(file)


    if not all(missing_annovarFiles):
        
        isMissing = True

        # print error message
        print("\n\n\n*** ERROR *** MEIGA-SR database dependencies not satisfied.\nSome of the following ANNOVAR files couldn't be located at", annovarDir)
        for file in annovarFiles: print(file)   
        
    return isMissing
