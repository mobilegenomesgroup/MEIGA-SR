#!/usr/bin/env python
#coding: utf-8

""" MEIGA-SR:
Program to detect retrotransposon integrations from pair end sequencing data

"""

""" VERSION RECORD:

1.0.1	Fully functional version with an independient GAPI + small fix 

1.0.2   Fully functional version that implements the following changues:
		1. GAPI modules included in the program
		2. Check_dependencies module updated and enabled
		3. Debug mode: --debug parameter creates a directory AAAAMMDDHHMMSS to avoid overwrite consecutive tests
		4. CONSTANTS in UPPERCASE

1.0.3	Fully functional version that implements the following changues:
		
	Major changues:
		1. imap_unordered() implemented instead of starmap() to avoid mem leaks
		2. logging library implemented instead of prints
		3. clusters that fail filters reported to specific file
		4. cleaning of unused code step 1

	Minor changues:
		1. KS test from scipy for META-RANGE filter

1.0.4	Fully functional version

	Major changues:
		1. Structure change: All steps that require annotation as single-process

1.0.5	Fully functional version

	Major changues:
		1. VCF output implementation

1.1.0	Fully functional version

	Major changues:
		0. New attributes added to META objects (ML features)
		1. ML classifier implemented
		2. Identity assignation modified
		3. PSD search deactivated
		4. Depth module added

1.1.3	Fully functional version

	Major changues:
		0. Fix bugs
		1. Non ASCI characters
		2. Paired mode in meis_vaf.py
"""



if __name__ == '__main__':
	
	VERSION = '1.1.5'
	
	
	####################
	## Import modules ##
	####################
 
	## 1. Check program dependencies are satisfied 
	################################################
	from GAPI import check_dependencies as cd
	from GAPI import unix
	from GAPI import log

	missingDependencies = cd.missing_python_dependencies() or cd.missing_program_dependencies()
	if missingDependencies: exit(1)

	# External
	import argparse
	import sys
	import os
	import multiprocessing as mp
	import configparser
	import time
	from datetime import datetime
	import logging

	# Internal
	from GAPI import bamtools
	from modules import caller

	######################
	## Get user's input ##
	######################

	def file_exists(parser, ifile, text="",index=True) :
		if not os.path.exists( ifile ) :
			parser.error("could not find %s %s !" %(text, ifile) )
		if not index : return ifile
		ext = { ".bam" : ".bai",
						".fa"  : ".fai",
						".bed" : ".tbi",
						".cram" : ".crai" }
		if ( not os.path.exists( ifile + ext[os.path.splitext(ifile)[-1]] ) ) : 
				parser.error("could not find index %s %s !" %(text, ifile) )
		return ifile
 
	## 0. Set timer
	##################
	start = time.time()

	## 1. Define parser
	######################
	parser = argparse.ArgumentParser()

	### Define subcommands 
	subparsers = parser.add_subparsers(title='subcommands')
	call = subparsers.add_parser('call', help='Call mobile element insertions from short-read data', description='Call mobile element insertions from short-read data. Two running modes: 1) SINGLE: individual sample; 2) PAIRED: tumour and matched normal sample')
	call.set_defaults(call_tds=False)
	call_tds = subparsers.add_parser('call-tds', help='Call mobile element transductions from short-read data', description='Call transductions for a set of target loci from target or whole genome sequencing data. Two running modes: 1) SINGLE: individual sample; 2) PAIRED: tumour and matched normal sample')
	call_tds.set_defaults(call_tds=True)

	### "call" mode: 
	# A. Mandatory arguments
	call.add_argument('config', help='Configuration file', type=lambda x: file_exists(call, x, "config", index=False))
	call.add_argument('bam', help='Input bam file. Will correspond to the tumour sample in the PAIRED mode', type=lambda x: file_exists(call, x, "bam"))
	# B. Optional arguments
	call.add_argument('--normalBam', default=None, dest='normalBam', help='Matched normal bam file. If provided MEIGA will run in PAIRED mode', type=lambda x: file_exists(call, x,"normalBam"))
	call.add_argument('--targetBins', default=None, dest='targetBins', help='Bed file containing target genomic bins for SV calling. Overrides --binSize and --refs in config file' , type=lambda x: file_exists(parser, x,"targetBins BED", index=False))
	call.add_argument('-o', '--outDir', default=os.getcwd(), dest='outDir', help='Output directory. Default: current working directory')
	call.add_argument('-p', '--processes', default=1, dest='processes', type=int, help='Number of processes. Default: 1')
	call.add_argument('-d', '--debug', action='store_true', dest='debug', help='Debug mode')
	call.add_argument('--predict', action='store_true', dest='predict', help='Apply ML classifier to output')

	### "call-tds" mode: 
	# A. Mandatory arguments
	call_tds.add_argument('config', help='Configuration file', type=lambda x: file_exists(call_tds, x, "config", index=False))
	call_tds.add_argument('bam', help='Input bam file. Will correspond to the tumour sample in the PAIRED mode', type=lambda x: file_exists(call_tds, x, "bam"))
	# B. Optional arguments
	call_tds.add_argument('--normalBam', default=None, dest='normalBam', help='Matched normal bam file. If provided MEIGA will run in PAIRED mode', type=lambda x: file_exists(call_tds, x,"normalBam"))
	call_tds.add_argument('--targetBins', default=None, dest='targetBins', help='Bed file containing target genomic bins for SV calling. Overrides --binSize and --refs in config file', type=lambda x: file_exists(parser, x,"targetBins BED", index=False))
	call_tds.add_argument('-o', '--outDir', default=os.getcwd(), dest='outDir', help='Output directory. Default: current working directory')
	call_tds.add_argument('-p', '--processes', default=1, dest='processes', type=int, help='Number of processes. Default: 1')
	call_tds.add_argument('-d', '--debug', action='store_true', dest='debug', help='Debug mode')

	### Set method features
	scriptName = os.path.basename(sys.argv[0])
	scriptName = os.path.splitext(scriptName)[0]

	mp.set_start_method('spawn')


	## 2. Parse user's input
	##########################
	args = parser.parse_args()
	configFile = args.config
	bam = args.bam
	normalBam = args.normalBam
	processes = args.processes
	targetBins = args.targetBins
	
	
	## 2.1. Set output dir
	##########################
	def DirFile_exists( ifile, text="") :
		if not os.path.exists( ifile ) :
			raise OSError("%s %s !" %(text, ifile) )
		return ifile

	outDir = args.outDir

	# if debug, use outDir/timeStamp as the output directory
	if args.debug:
		timeStamp = str(datetime.now().strftime("%Y%m%d%H%M%S"))
		outDir = args.outDir + '/' + timeStamp

	# create output dir
	os.makedirs(outDir, exist_ok=True)

	## 2.2. Determine running mode
	##################################
	mode = 'PAIRED' if normalBam else 'SINGLE'
	
	## 2.3. Create configuration dictionary
	###########################################
	confDict = {}
	meigaConfig = configparser.ConfigParser(inline_comment_prefixes = ('#'))
	meigaConfig.read(configFile)
	config = meigaConfig['MEIGA-SR']

	### General
	reference = DirFile_exists(config.get('reference'), text="config error, reference file not found")
	DirFile_exists(config.get('reference')+".fai", text="config error, reference index file not found")
	refDir = DirFile_exists(config.get('refDir'), text="config error, refDir directory not found")
	confDict['source'] = 'MEIGA-SR-' + VERSION
	confDict['species'] = config.get('species')
	confDict['build'] = config.get('build')
	confDict['annovarDir'] = DirFile_exists( config.get('annovarDir'), text="config error, annovarDir directory not found")
	confDict['germlineMEI'] = None if config.get('germlineMEI') == 'none' else config.get('germlineMEI')
	confDict['processes'] = processes
	confDict['debug'] = args.debug
	confDict['predict'] = args.predict

	### BAM processing
	confDict['targetBins'] = targetBins
	confDict['binSize'] = config.getint('binSize')
	confDict['filterDup'] = config.getboolean('noDuplicates')
	confDict['readFilters'] = [filt.strip() for filt in config.get('readFilters').split(',')]
	confDict['minMAPQ'] = config.getint('minMAPQ')
	confDict['minCLIPPINGlen'] = config.getint('minCLIPPINGlen')
	confDict['targetEvents'] = ['DISCORDANT', 'CLIPPING']

	### Target refs
	refs = config.get('refs')
	if refs == 'ALL': refs = bamtools.get_refs(bam) # If "ALL" specified, get all refs in bam file
	targetRefs = [ref.strip() for ref in refs.split(',')]
	confDict['targetRefs'] = targetRefs

	### Clustering
	confDict['minClusterSize'] = config.getint('minClusterSize')
	confDict['maxClusterSize'] = config.getint('maxClusterSize')
	confDict['maxBkpDist'] = config.getint('BKPdist')
	confDict['minPercRcplOverlap'] = config.getint('minPercOverlap')
	confDict['equalOrientBuffer'] = config.getint('equalOrientBuffer')
	confDict['oppositeOrientBuffer'] = config.getint('oppositeOrientBuffer')

	### Filtering thresholds
	confDict['minReads'] = config.getint('minReads')
	confDict['minNormalReads'] = config.getint('minNormalReads')
	confDict['minNbDISCORDANT'] = config.getint('minClusterSize')
	confDict['minNbCLIPPING'] = config.getint('minClusterSize')
	confDict['minReadsRegionMQ'] = config.getfloat('minReadsRegionMQ')
	confDict['maxRegionlowMQ'] = config.getfloat('maxRegionlowMQ')
	confDict['maxRegionSMS'] = config.getfloat('maxRegionSMS')

	### Transduction search
	confDict['retroTestWGS'] = config.getboolean('wgsData')
	confDict['blatClip'] = config.getboolean('blatClip')
	confDict['tdBed'] = None if config.get('tdBed') == 'none' else config.get('tdBed')
	confDict['srcFamilies'] = [family.strip() for family in config.get('srcFamilies').split(',')]

	# In debug mode the output is the specified dir + data_time
	if args.debug:
		outDir = args.outDir + "/" + str( datetime.now().strftime("%Y%m%d%H%M%S") ) 
	else:
		outDir = args.outDir

	# create  output and log dirs
	logDir = outDir + '/logs'
	confDict['outDir'] = outDir
	confDict['logDir'] = logDir

	os.makedirs(outDir, exist_ok=True)
	os.makedirs(logDir, exist_ok=True)


	## 2.5 Initialize log system
	##############################
	logName = 'main'
	logFile = logDir + '/main.log'
	logFormat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s' 
	logger = log.setup_logger(logName, logFile, logFormat, level=logging.DEBUG, consoleLevel=logging.WARNING)
        

	## 3. Display configuration to standard output
	################################################
	logger.info('***** ' + scriptName + ' ' + VERSION + ' configuration *****')
	logger.info('*** Arguments ***')
	subcommand = 'call-tds' if args.call_tds else 'call'
	logger.info('subcommand: ' + subcommand)
	logger.info('config file: ' + configFile)
	logger.info('bam: '+  bam)
	logger.info('normalBam: ' + str(normalBam))
	logger.info('outDir: ' + outDir)
	logger.info('processes: ' + str(processes) + '\n\n')
	logger.info('*** ConfigDict ***')
	for key, value in confDict.items():
		logger.info(key + ' => ' + str(value))
	logger.info('\n\n')

	## 4. Check all required DB are located in the dirs provided
	##############################################################
	missingDB = cd.missing_db(refDir, confDict['annovarDir'])
	if missingDB: exit(1)

	
	########################
	## Execute MEI caller ##
	########################
	
	# If 'call-tds' running mode selected
	if args.call_tds:
		# execute transductions caller
		meiCaller = caller.transduction_caller(mode, bam, normalBam, reference, refDir, confDict)

	else:
		# execute universal MEI caller
		meiCaller = caller.MEI_caller(mode, bam, normalBam, reference, refDir, confDict)

	meiCaller.call()


	############
	##  Exit  ##
	############

	# close log files and remove logger
	log.remove_logger(logger)

	# agregate log files
	unix.agregate_logs(confDict['logDir'])

	# print timestamp
	timeCounter = round((time.time()-start)/60, 4)
	logger.info('***** Finished! in ' + str(timeCounter) + 'minutes *****\n')




