#!/bin/sh

# A) Load environment
source ../.venv/bin/activate
export PYTHONHASHSEED=0

# B) Set input
bam=simulationRegions.sorted.bam
calls_ins=targetRegions_INS.bed
calls_rg=targetRegions_RG.bed
outDir=output
mkdir -p $outDir

# C) Launch MEIGA-SR for RT insertions
python ../scripts/MEIs_vaf.py $bam $calls_ins $outDir

# D) Launch MEIGA-SR for RT-mediated rearrangements
python ../scripts/RG_vaf.py $bam $calls_rg $outDir


