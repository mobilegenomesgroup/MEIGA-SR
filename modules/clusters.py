
'''
Module 'clusters' - Contains classes for dealing with genomic variation
'''

## DEPENDENCIES ##
# External
import sys
from operator import itemgetter
from collections import Counter

# Internal
from GAPI import log
from GAPI import bamtools 
from GAPI import clusters as clusters_GAPI
from GAPI import formats
from modules import clustering as clustering_MS


#################
##  FUNCTIONS  ##
#################

def create_cluster(events, clusterType):
    '''
    Function to create a cluster object instance

    Input:
        1. events: list of events/clusters that will compose the cluster
        2. clusterType: type of cluster (META: metacluster; INS: insertion; DEL: deletion; CLIPPING: clipping; DISCORDANT: discordant paired-end)

    Output:
        1. cluster: cluster object instance
    '''
    cluster = ''

    ## a) Create META cluster of ME and SR
    # Note: events should correspond to a list of clusters 
    if (clusterType == 'META'):
        cluster = META_cluster_MS(events)

    ## b) Create INS cluster
    elif (clusterType == 'INS'):
        cluster = clusters_GAPI.INS_cluster(events)

    ## c) Create DEL cluster
    elif (clusterType == 'DEL'):
        cluster = clusters_GAPI.DEL_cluster(events)

    ## d) Create CLIPPING cluster
    elif (clusterType == 'CLIPPING') or (clusterType == 'LEFT-CLIPPING') or (clusterType == 'RIGHT-CLIPPING'):
        cluster = clusters_GAPI.CLIPPING_cluster(events)

    ## e) Create DISCORDANT cluster
    elif 'DISCORDANT' in clusterType:
        cluster = clusters_GAPI.DISCORDANT_cluster(events)

    ## f) Create SUPPLEMENTARY cluster 
    elif 'SUPPLEMENTARY' in clusterType:
        cluster = clusters_GAPI.SUPPLEMENTARY_cluster(events)

    ## g) Create INS_VCF cluster
    elif 'INS_VCF' in clusterType:
        cluster = formats.INS_cluster(events)

    ## h) Unexpected cluster type
    else:
        log.info('Error at \'create_cluster\'. Unexpected cluster type')
        sys.exit(1)

    return cluster


def metacluster_mate_suppl(discordants, leftClippings, rightClippings, confDict):
    '''
    Group discordant and clipping clusters into metaclusters

    Input:
        1. discordants: list of discordant cluster objects
        2. leftClippings: list of left clipping cluster objects
        3. rightClippings: list of right clipping cluster objects
        5. refLengths: dictionary containing references as keys and their lengths as values

    Output:
        1. filteredMeta: list of metaclusters
    '''
    ## 1. Create list of discordant mate clusters
    mateClusters = [discordant.create_matesCluster() for discordant in discordants]
    
    ## 2. Create list of supplementary clusters
    supplClustersLeft = [clipping.supplCluster for clipping in leftClippings]
    supplClustersRight = [clipping.supplCluster for clipping in rightClippings]
    supplClusters = supplClustersLeft + supplClustersRight
    
    ## 2.1. Fill supplementary cluster orientation attribute
    [supplCluster.clippOrientation() for supplCluster in supplClusters]
    
    ## 3. Perform metaclustering
    allMeta = clustering_MS.reciprocal_overlap_clustering(mateClusters+supplClusters, 1, 1, 'META', confDict['oppositeOrientBuffer'])

    ## 4. Filter metaclusters based on read support
    filteredMeta = []

    for meta in allMeta:

        if meta.supportingReads()[0] >= confDict['minReads']:
            filteredMeta.append(meta)
    
    return filteredMeta


###############
##  CLASSES  ##
###############
 
class META_cluster_MS(clusters_GAPI.META_cluster):
    '''
    Meta cluster child class specific for mobile elements and short read data
    '''
    def __init__(self, clusters):

        clusters_GAPI.META_cluster.__init__(self, clusters)
        
        # ins type and identity
        self.ins_type = None
        self.identity = None
        self.plus_id = None
        self.minus_id = None
        
        # transduction attributes
        self.src_id = None
        self.plus_src_id = None
        self.minus_src_id = None
        self.src_end = None
        self.plus_src_end = None
        self.minus_src_end = None
        self.src_type = None
        
        # polyA
        self.pA = False
        self.plus_pA = False
        self.minus_pA = False
        
        # mei characteristics
        self.TSD = None
        self.strand = None
        self.repeatAnnot = None
        
        # filtering attrib for ML
        self.tumourPerc = None
        self.germPerc = None
        self.areaMAPQ = None
        self.areaSMS = None
        
        self.svsNormal = None
        self.regularSV = None
        self.metaRange = None
        
        self.tdSrc_isRef = False
        
        # by default, no annotated repeat in region
        self.matchRepeat = False
        # 538 was the max value found at repeats.bed
        self.miliDiv = 1000
        
        self.plus_idPerc = None
        self.minus_idPerc = None
        self.plus_count = None
        self.minus_count = None
        
        # new features related to coverage
        self.depthDiff = None
        self.upDepth = None
        self.downDepth = None
        self.normalDepth = None
        self.tumourDepth = None
        
        # features related to stacked clusters
        self.diff_plusBegs = None
        self.diff_minusBegs = None
        self.beg_excessPlus = None
        self.beg_excessMinus = None
        self.stackClipsNormal = None
        
    def define_identityAttrib(self):
        '''
        Define plus and minus cluster identities, and set polyA attributes if polyA present
        '''
        self.identity = None
        self.plus_id = None
        self.minus_id = None

        ## 1. collect events identities
        plus_identities = [event.identity.split(',') for event in self.events if event.orientation == 'PLUS']
        plus_identities = [item for sublist in plus_identities for item in sublist]
        minus_identities = [event.identity.split(',') for event in self.events if event.orientation == 'MINUS']     
        minus_identities = [item for sublist in minus_identities for item in sublist]
        
        # get total counts
        countPlus = len(plus_identities)
        countMinus = len(minus_identities)
        
        ## 2. set pA support
        if 'Simple_repeat' in plus_identities:
            plus_pA = plus_identities.count('Simple_repeat')/len(plus_identities)
            self.plus_pA, self.pA = plus_pA, True
            plus_identities = [value for value in plus_identities if value != 'Simple_repeat']
        
        if 'Simple_repeat' in minus_identities:
            minus_pA = minus_identities.count('Simple_repeat')/len(minus_identities)
            self.minus_pA, self.pA = minus_pA, True
            minus_identities = [value for value in minus_identities if value != 'Simple_repeat']

        ## set strand
        # if a pA has been detected
        if self.pA:

            # if pA detected at both sites
            if self.plus_pA and self.minus_pA:

                # choose the orientation with greater support (fraction)
                if self.plus_pA > self.minus_pA:
                    self.strand = '-'
                else:
                    self.strand = '+'

            # if pA detected at only one site
            elif self.plus_pA:
                self.strand = '-'
                
            elif self.minus_pA:
                self.strand = '+'

        # get total counts after pA support filtering
        countPlus_no_pA = len(plus_identities)
        countMinus_no_pA = len(minus_identities)
                            
        ## 3. set clusters identity
        plus_identityDict = Counter(plus_identities)
        minus_identityDict = Counter(minus_identities)
        
        # select most supported identity for plus cluster
        if plus_identityDict:
            
            # select identity with the max support
            plus_identityDict_max = max(plus_identityDict.items(), key=itemgetter(1))
            
            # if the support is > 10% of total identity counts
            if plus_identityDict_max[1]/countPlus > 0.1:
                
                plus_id = plus_identityDict_max[0].split('_')
                self.plus_id = plus_id[0]
                self.plus_idPerc = plus_identityDict_max[1]/countPlus_no_pA
                
                # if it's a TD
                if len(plus_id) == 3:
                    self.plus_src_id, self.plus_src_end = plus_id[1], plus_id[2]
            
        # if polyA ('Simple_repeat') support is more than 90%
        if not self.plus_id and countPlus > 0:
            self.plus_id = 'Simple_repeat'
            self.plus_idPerc = (countPlus - countPlus_no_pA)/countPlus
        
        # select most supported identity for minus cluster    
        if minus_identityDict:
            
            # select identity with the max support
            minus_identityDict_max = max(minus_identityDict.items(), key=itemgetter(1))
            
            # if the support is > 10% of total identity counts
            if minus_identityDict_max[1]/countMinus > 0.1:
                
                minus_id = minus_identityDict_max[0].split('_')
                self.minus_id = minus_id[0]
                self.minus_idPerc = minus_identityDict_max[1]/countMinus_no_pA
                
                # if it's a TD
                if len(minus_id) == 3:
                    self.minus_src_id, self.minus_src_end = minus_id[1], minus_id[2]
            
        # if polyA ('Simple_repeat') support is more than 90%
        if not self.minus_id and countMinus > 0:
            self.minus_id = 'Simple_repeat'
            self.minus_idPerc = (countMinus - countMinus_no_pA)/countMinus

    def addClippings(self, confDict, bam, normalBam):
        '''
        Collect supporting clippings 
        '''
        # Make custom conf. dict
        clippingConfDict = dict(confDict)
        clippingConfDict['targetEvents'] = ['CLIPPING']
        clippingConfDict['minMAPQ'] = 1
        clippingConfDict['readFilters'] = ['SMS']
        clippingConfDict['minClusterSize'] = 1

        clippingEventsDict = {}

        # Define region
        ref = self.ref
        beg = self.refLeftBkp if self.refLeftBkp is not None else self.beg
        end = self.refRightBkp if self.refRightBkp is not None else self.end

        # Collect clippings
        if not normalBam:
            clippingEventsDict = bamtools.collectSV(ref, beg, end, bam, clippingConfDict, None)
            
        else:
            clippingEventsDict = bamtools.collectSV_paired(ref, beg, end, bam, normalBam, clippingConfDict)
        
        # When cluster orientation is 'PLUS', add the biggest right clipping cluster if any:
        if self.orientation == 'PLUS':
            
            ## Get clipping clusters:
            clippingRightEventsDict = dict((key,value) for key, value in clippingEventsDict.items() if key == 'RIGHT-CLIPPING')
            CLIPPING_cluster = self.add_clippingEvents(ref, beg, end, clippingRightEventsDict, ['RIGHT-CLIPPING'], confDict)

        # When cluster orientation is LEFT, add the biggest left clipping cluster if any:
        elif self.orientation == 'MINUS':
            
            ## Get clipping clusters:
            clippingLeftEventsDict = dict((key,value) for key, value in clippingEventsDict.items() if key == 'LEFT-CLIPPING')
            CLIPPING_cluster = self.add_clippingEvents(ref, beg, end, clippingLeftEventsDict, ['LEFT-CLIPPING'], confDict)

        # If it is reciprocal
        elif self.orientation == 'RECIPROCAL':
            
            CLIPPING_cluster = self.add_clippingEvents(ref, beg, end, clippingEventsDict, ['RIGHT-CLIPPING', 'LEFT-CLIPPING'], confDict)

        return CLIPPING_cluster        

