## DEPENDENCIES ##
# External
import pybedtools
import pysam
from collections import Counter
from Bio.Seq import Seq

# Internal
from GAPI import formats
from modules import filters


## FUNCTIONS CALL ##
def write_MEI_calls(metaclusters, outDir, bam, confDict, PASS = True):
    '''
    Write metacluster calls into file

    Input:
        1. metaclusters: list of metaclusters 
        2. outDir: Output directory
        3. PASS: Boolean
        
    Output: tsv file containing all MEI calls detected by MEIGA-SR
    '''
    ## 1. Write header
    outFilePath = outDir + '/meiga_sr_calls.tsv' if PASS else outDir + '/meiga_sr_calls_filteredOut.tsv'
    outFile = open(outFilePath, 'w')
    row = "#ref \t beg \t end \t orientation \t pA \t identity \t ins_type \t src_id \t src_type \t src_end \t annot_region \t annot_gene \t annot_repeats \t repeats_subfamilies \t repeats_distances \t plus_pA \t minus_pA \t plus_id \t minus_id \t strand \t nbReads \t nbTumour \t nbNormal \t nbDISCORDANT \t nbCLIPPING \t readIds \t normal_readIds \t germPerc \t tumourPerc \t areaMAPQ \t areaSMS \t svsNormal \t metaRange \t tdSrc_isRef \t matchRepeat \t miliDiv \t plus_idPerc \t minus_idPerc \t plus_count \t minus_count \t depthDiff \t upDepth \t downDepth \t tumourDepth \t normalDepth \t diff_plusBegs \t diff_minusBegs \t beg_excessPlus \t beg_excessMinus \t stackClipsNormal\n"

    outFile.write(row)

    ## 2. Generate list containing transduction calls
    call = None 
    calls = []

    # For each cluster
    for cluster in metaclusters:

        readIds = ','.join(cluster.supportingReads()[3])
        normalReads = cluster.supportingReads()[5]
        normal_readIds = ','.join(normalReads) if normalReads else None
                
        # if bkp has being defined, use it as call coordinates
        beg = cluster.bkpA
        end = cluster.bkpB
        region, gene = cluster.geneAnnot if hasattr(cluster, 'geneAnnot') else ("None", "None")
        
        ## Insertion region annotation
        repeats = cluster.repeatAnnot if hasattr(cluster, 'repeatAnnot') else []        
        families = ','.join([repeat['family'] for repeat in repeats]) if repeats else None 
        subfamilies = ','.join([repeat['subfamily'] for repeat in repeats]) if repeats else None   
        distances = ','.join([str(repeat['distance']) for repeat in repeats]) if repeats else None   
        
        failedFilters = cluster.failedFilters if hasattr(cluster, 'failedFilters') else None
        
        ## ML features
        # fill self.tdSrc_isRef
        filters.check_tdSrc_isRef(cluster)
        # fill self.matchRepeat and self.matchSubfam
        minDist = 150
        filters.check_matchRepeats(cluster, minDist)
        cluster.plus_count = len(set([event.readName for event in cluster.events if event.orientation == 'PLUS']))
        cluster.minus_count =len(set([event.readName for event in cluster.events if event.orientation == 'MINUS']))
        
        # call = [cluster.ref, str(beg), str(end), str(cluster.orientation), str(cluster.pA), str(cluster.identity), str(cluster.ins_type), str(cluster.src_id), str(cluster.src_type), str(cluster.src_end), str(region), str(gene), str(families), str(subfamilies), str(distances), str(cluster.plus_pA), str(cluster.minus_pA), str(cluster.plus_id), str(cluster.minus_id), str(cluster.strand), str(cluster.supportingReads()[0]), str(cluster.supportingReads()[1]), str(cluster.supportingReads()[2]), str(cluster.nbDISCORDANT()), str(cluster.nbCLIPPINGS()), readIds, str(normal_readIds)]
        call = [cluster.ref, str(beg), str(end), str(cluster.orientation), str(cluster.pA), 
                str(cluster.identity), str(cluster.ins_type), str(cluster.src_id), str(cluster.src_type), 
                str(cluster.src_end), str(region), str(gene), str(families), str(subfamilies), str(distances), 
                str(cluster.plus_pA), str(cluster.minus_pA), str(cluster.plus_id), str(cluster.minus_id), 
                str(cluster.strand), str(cluster.supportingReads()[0]), str(cluster.supportingReads()[1]), 
                str(cluster.supportingReads()[2]), str(cluster.nbDISCORDANT()), str(cluster.nbCLIPPINGS()), 
                readIds, str(normal_readIds), str(cluster.germPerc), str(cluster.tumourPerc), str(cluster.areaMAPQ), str(cluster.areaSMS), 
                str(cluster.svsNormal), str(cluster.metaRange), str(cluster.tdSrc_isRef), str(cluster.matchRepeat), str(cluster.miliDiv),
                str(cluster.plus_idPerc), str(cluster.minus_idPerc), str(cluster.plus_count), str(cluster.minus_count),
                str(cluster.depthDiff), str(cluster.upDepth), str(cluster.downDepth), str(cluster.tumourDepth), str(cluster.normalDepth), 
                str(cluster.diff_plusBegs), str(cluster.diff_minusBegs), str(cluster.beg_excessPlus), str(cluster.beg_excessMinus), 
                str(cluster.stackClipsNormal)]
        
        calls.append(call)
            
    ## 3. Sort calls first by chromosome and then by start position
    calls.sort(key=lambda x: (x[0], int(x[1])))

    ## 4. Write calls into output file
    for MEI in calls:
        row = "\t".join(MEI) + "\n"
        outFile.write(row)
    
    outFile.close()
    

## FUNCTIONS CALL-TDS ##
def write_transduction_calls(clustersPerSrc, outDir, germlineDb):
    '''
    Compute transduction counts per source element and write into file

    Input:
        1. clustersPerSrc: Dictionary containing one key per source element and the list of identified transduction clusters 
        2. outDir: Output directory
        3. germlineDb: germline MEI bed file 
        
    Output: tsv file containing transduction counts per source element ordered by chromosome and then by start position
    '''
    ## 1. Write header 
    outFilePath = outDir + '/transduction_calls.tsv'
    outFile = open(outFilePath, 'w')
    row = "#ref \t beg \t end \t srcId \t identity \t ins_type \t src_id \t src_type \t src_end \t orientation \t nbReads \t nbReadsNormal \t nbDiscordant \t nbClipping \t readIds \t readIds_normal \n"
    outFile.write(row)

    ## 2. Generate list containing transduction calls
    call = None 
    calls = []

    # For each source element
    for srcId, clusters in clustersPerSrc.items(): 

        # For each cluster
        for cluster in clusters:
            
            # read ids
            readIds = ','.join(cluster.supportingReads()[3])
            
            tumourReads = cluster.supportingReads()[4]
            tumour_readIds = ','.join(tumourReads) if tumourReads else readIds
            
            normalReads = cluster.supportingReads()[5]
            normal_readIds = ','.join(normalReads) if normalReads else None
            
            # nb_reads
            nb_tumour = str(cluster.supportingReads()[1]) if cluster.supportingReads()[1] else str(cluster.supportingReads()[0])
            nb_normal = str(cluster.supportingReads()[2])
            
            # if bkp has being defined, use it as call coordinates
            beg = cluster.bkpA
            end = cluster.bkpB
            
            # gene annotation
            region, gene = cluster.geneAnnot if hasattr(cluster, 'geneAnnot') else ("None", "None")
            
            call = [cluster.ref, str(beg), str(end), srcId, str(cluster.identity), str(cluster.ins_type), str(cluster.src_id), str(cluster.src_type), str(cluster.src_end), str(cluster.orientation), nb_tumour, nb_normal, str(cluster.nbDISCORDANT()), str(cluster.nbSUPPLEMENTARY()), str(region), str(gene), str(tumour_readIds), str(normal_readIds)]
            calls.append(call)

    ## 3. Sort transduction calls first by chromosome and then by start position
    calls.sort(key=lambda x: (x[0], int(x[1])))

    ## 4. Write transduction calls into output file
    for transduction in calls:
        row = "\t".join(transduction) + "\n"
        outFile.write(row)
    
    outFile.close()
    
    # if there is calls
    if call is not None:
    
        outFile = pybedtools.BedTool(outFilePath)

        ## 5. Collapse calls when pointing to the same MEI. It happens when source elements are too close
        colList = list(range(4, len(call)+1))
        colFormat = ['distinct'] * (len(call) - 3)
        outBed = outFile.merge(c=colList, o=colFormat, d=100, header=True)
        
        ## 6. Filter germline variants if orphan germline DB provided
        if germlineDb:
            
            dbBed = pybedtools.BedTool(germlineDb)
            outBed = outBed.intersect(b=dbBed, v=True, header=True)
        
        outBed.saveas(outFilePath)
    
    ## 7. Write final transduction counts to file
    write_transduction_counts(outFilePath, outDir)


def write_transduction_counts(outBed, outDir):
    '''
    Compute transduction counts per source element and write into file

    Input:
        1. outBed: RetroTest output bed file 
        2. outDir: Output directory
        
    Output: tsv file containing orphan counts per source element
    '''
    ## 1. Open output bed 
    BED = formats.BED()
    BED.read(outBed, 'List', None)
    
    ## 2. Select orphan calls
    src_ids = [line.optional['srcId'] for line in BED.lines if line.optional['ins_type'] == "TD2"]
    
    ## 3. Count orphans per source id
    srcCounter = Counter(src_ids)

    ## 4. Write counts to output file
    outFilePath = outDir + '/orphan_counts.tsv'
    outFile = open(outFilePath, 'w')
    row = "#srcId \t nbTransductions \n"
    outFile.write(row)
    
    for srcId, nbTd in srcCounter.items():
        row = "\t".join([srcId, str(nbTd), "\n"])
        outFile.write(row)
            
    outFile.close()


def write_failedCluster(cluster, failedFilters):
    '''
    Create a list with cluster main attributes to be output when 
    it fails filtering.
    
    Input: 
    1. cluster: cluster object
    2. failedFilters: list with filters names that have been failed by cluster
    
    Output:
    1. row: list of strings with the following fields:
    #ref, beg, end, clusterType, failedFilters, orientation, identity, nb_tumour, nb_normal, ttumour_readIds, normal_readIds
    '''
    ## get idendity and orientation if attribute exists
    orientation = str(cluster.orientation) if hasattr(cluster, 'orientation') else 'None'
    identity = str(cluster.identity) if hasattr(cluster, 'identity') else 'None'
    
    ## get cluster events
    nbTotal, nbTumour, nbNormal, reads, readsTumour, readsNormal = cluster.supportingReads()
    
    # A. read ids
    tumour_readIds = ','.join(readsTumour) if readsTumour else ','.join(reads)
    normal_readIds = ','.join(readsNormal) if readsNormal else 'None'
    
    # B. nb_reads
    nb_tumour = str(nbTumour) if nbTumour else str(nbTotal)
    nb_normal = str(nbNormal)
    
    ## Get failed filters
    failedFilters2str = ','.join(failedFilters)
    
    call = [cluster.ref, str(cluster.beg), str(cluster.end), cluster.clusterType, failedFilters2str, orientation, identity, nb_tumour, nb_normal, tumour_readIds, normal_readIds]
    row = '\t'.join(call)
    
    return row

def write_MEI_calls_VCF(metaclusters, outName, outDir, reference, refLengths, confDict):
    '''
    Write MEIGA-SR calls in a VCF file

    Input:
        1. metaclusters: list of metaclusters
        2. outName: output file name
        3. outDir: output directory
        4. reference: reference fasta
        5. refLengths: dict containing ref ids as keys and their lenght as values
        6. confDict: configuration dictionary

    Output: VCF file containing identified metaclusters
    '''
    
    ## 0. Collect info from confDict
    source = confDict['source'] # MEIGA-SR version
    species = confDict['species'] # species
    build = confDict['build'] # reference genome build
    seqBuffer = 10 # seq buffer for REFSEQ

    ## 1. Initialize VCF
    #####################
    VCF = formats.VCF()

    ## 2. Create VCF header
    ########################
    # Define info fields
    ## TO DO: Add 'COSMIC': ['0', 'Flag', 'Reported as cancer driver in COSMIC cancer gene census database']
    info = {'BKPB': ['1', 'Integer', 'Second breakpoint position'],
            'STRAND': ['1', 'String', 'Insertion DNA strand. + | -'],
            'TSD': ['1', 'String', 'True if target site duplication'],
            'TSD_SEQ': ['1', 'String', 'Target site duplication sequence'],
            'TSD_LEN': ['1', 'String', 'Target site duplication sequence length'],
            'SVTYPE': ['1', 'String', 'Type of structural variant. TD0: solo | TD1: partnered-transduction | TD2: orphan-transduction | PSD: processed-pseudogen | RG: rearrangement'],
            'IDENT': ['1', 'String', 'Mobile element identity. L1 | Alu | SVA | pA | PSD'],
            'PLUS_ID': ['1', 'String', 'Plus cluster identity'],
            'MINUS_ID': ['1', 'String', 'Minus cluster identity'],
            'SRCID': ['1', 'String', 'Source element cytoband identifier. Only for gemline source elements'],
            'SRCTYPE': ['1', 'String', 'Source element type. GERMLINE or SOMATIC'],
            'SRCEND': ['1', 'String', 'Transduction end. Only for gemline source elements'],
            'REGION': ['1', 'String', 'Genomic region. Exonic | splicing | ncRNA | UTR5 | UTR3 | intronic | upstream | downstream | intergenic'],
            'GENE': ['1', 'String', 'Gene symbol'],
            'REP': ['1', 'String', 'Repetitive element overlapping the insertion breakpoint'],
            'REP_SUBFAM': ['1', 'String', 'Repeat subfamily'],
            'REP_DIST': ['1', 'String', 'Repeat distance from breakpoint'],
            'REFSEQ': ['1', 'String', 'Reference sequence around breakpoint'],
            'ORIENT': ['1', 'String', 'Metacluster orientation. PLUS | MINUS | RECIPROCAL'],
            'POLYA': ['1', 'String', 'True if pA tail has been identified'],
            'PLUS_PA': ['1', 'String', 'True if plus cluster has pA tail'],
            'MINUS_PA': ['1', 'String', 'True if minus cluster has pA tail'],
            'NBREADS': ['1', 'Integer', 'Total nb of reads supporting structural variant'],
            'NBTUMOUR': ['1', 'Integer', 'Nb of reads supporting structural variant in tumour'],
            'NBNORMAL': ['1', 'Integer', 'Nb of reads supporting structural variant in matched normal'],
            'NBDISC': ['1', 'Integer', 'Nb of discordant reads'],
            'NBCLIP': ['1', 'Integer', 'Nb of clipping reads'],
            'NBPSEUDODISC': ['1', 'Integer', 'Nb of pseudo-discordant reads'],
            'TR': ['1', 'String', 'Tumour read identifiers'],
            'NR': ['1', 'String', 'Matched normal read identifiers']}
  
    # Create header
    VCF.create_header(source, build, species, refLengths, info, {}, None)

    ## 3. Add calls to the VCF
    ###########################
    # 3.1 Create dictionary with reference sequences
    dictRef = {}
    with pysam.FastxFile(reference) as fh:
        
        for entry in fh:
            dictRef[entry.name] = entry.sequence

    # 3.2 Iterate over metaclusters to retrieve features
    for cluster in metaclusters:
        
        ## Define precise metacluster coordinates
        beg = cluster.bkpA
        end = cluster.bkpB
        
        ## Retrieve reads forming the cluster
        readIds = ','.join(cluster.supportingReads()[3])
        normalReads = cluster.supportingReads()[5]
        normal_readIds = ','.join(normalReads) if normalReads else None
        
        ## Retrieve annotation attributes
        region, gene = cluster.geneAnnot if hasattr(cluster, 'geneAnnot') else ("None", "None")
        repeats = cluster.repeatAnnot if hasattr(cluster, 'repeatAnnot') else []        
        families = ','.join([repeat['family'] for repeat in repeats]) if repeats else None 
        subfamilies = ','.join([repeat['subfamily'] for repeat in repeats]) if repeats else None   
        distances = ','.join([str(repeat['distance']) for repeat in repeats]) if repeats else None   
        
        ## Characterize TSD
        TSD_seq = None
        if cluster.TSD and end: 
            TSD_seq = dictRef[cluster.ref][beg:end]

        TSD_len = len(TSD_seq) if TSD_seq else None

        plus_bkp = cluster.plus_bkp
        minus_bkp = cluster.minus_bkp
        if (cluster.strand == '-' and plus_bkp):
            REF_seq = dictRef[cluster.ref][plus_bkp-seqBuffer:plus_bkp+seqBuffer]
            seq = Seq(REF_seq)
            REF_seq = seq.reverse_complement()
        elif (cluster.strand == '+' and minus_bkp):
            REF_seq = dictRef[cluster.ref][minus_bkp-seqBuffer:minus_bkp+seqBuffer]
        else:
            REF_seq = None

        ## Collect filter information
        # failedFilters = cluster.failedFilters if hasattr(cluster, 'failedFilters') else None
        
        ## Collect insertion basic features
        CHROM = cluster.ref
        POS = beg
        ID = '.'
        REF = str(dictRef[cluster.ref][POS])
        ALT = '<MEI>'
        QUAL = '.'
        FILTER = 'PASS' if not cluster.failedFilters else ','.join(cluster.failedFilters)
        
        ## Collect extra insertion features to include at info field
        INFO = {}
        INFO['BKPB'] = str(end)
        INFO['STRAND'] = str(cluster.strand)
        INFO['TSD'] = str(cluster.TSD)
        INFO['TSD_SEQ'] = str(TSD_seq)
        INFO['TSD_LEN'] = str(TSD_len)
        INFO['SVTYPE'] = str(cluster.ins_type)
        INFO['IDENT'] = str(cluster.identity)
        INFO['PLUS_ID'] = str(cluster.plus_id)
        INFO['MINUS_ID'] = str(cluster.minus_id)
        INFO['SRCID'] =  str(cluster.src_id)
        INFO['SRCTYPE'] = str(cluster.src_type)
        INFO['SRCEND'] = str(cluster.src_end)        
        INFO['REGION'] = str(region).replace(';',",")
        INFO['GENE'] = str(gene).replace(';',",")
        INFO['REP'] = str(families)
        INFO['REP_SUBFAM'] = str(subfamilies)
        INFO['REP_DIST'] = str(distances)        
        INFO['REFSEQ'] = str(REF_seq) 
        INFO['ORIENT'] = str(cluster.orientation)
        INFO['POLYA'] = str(cluster.pA)
        INFO['PLUS_PA'] = str(cluster.plus_pA)
        INFO['MINUS_PA'] = str(cluster.minus_pA)
        INFO['NBREADS'] = str(cluster.supportingReads()[0])
        INFO['NBTUMOUR'] = str(cluster.supportingReads()[1])
        INFO['NBNORMAL'] = str(cluster.supportingReads()[2])
        INFO['NBDISC'] = str(cluster.nbDISCORDANT())
        INFO['NBCLIP'] = str(cluster.nbCLIPPINGS())
        INFO['NBPSEUDODISC'] = str(cluster.nbPSEUDO_DISCORDANT())
        INFO['TR'] = str(readIds)
        INFO['NR'] = str(normal_readIds)

        ## Create VCF variant object
        fields = [CHROM, POS, ID, REF, ALT, QUAL, FILTER, INFO, {}]

        ## Add variant to the VCF
        INS = formats.VCF_variant(fields)
        VCF.add(INS)
        
    ## 4. Sort VCF
    ###############
    VCF.sort()

    ## 5. Write VCF in disk
    ########################
    infoIds = ['BKPB', 'STRAND', 'TSD', 'TSD_SEQ', 'TSD_LEN', 'SVTYPE', 'IDENT', 'PLUS_ID', 'MINUS_ID', 'SRCID', 'SRCTYPE', 'SRCEND', \
               'REGION', 'GENE', 'REP', 'REP_SUBFAM', 'REP_DIST', 'REFSEQ', 'ORIENT', 'POLYA', \
               'PLUS_PA', 'MINUS_PA', 'NBREADS', 'NBTUMOUR', 'NBNORMAL', 'NBDISC', 'NBCLIP', 'NBPSEUDODISC', 'TR', 'NR']

    VCF.write(infoIds, [], outName, outDir)

