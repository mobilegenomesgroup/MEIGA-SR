'''
Module 'retrotransposons' - Contains functions for the identification and characterization of retrotransposon sequences
'''

## DEPENDENCIES ##
# External
import operator

# Internal
from GAPI import unix
from GAPI import sequences
from GAPI import bamtools
from GAPI import bkp
from GAPI import events
from GAPI import annotation
from GAPI import structures

## FUNCTIONS CALL ##
def determine_discordant_identity_MEIs(discordants, polyABinDb, repeatsBinDb, transducedBinDb, exonsBinDb, readSize):
    '''
    Determine discortant read pair identity based on the mapping position of anchor´s mate

    Input:
        1. discordants: list containing input discordant read pair events
        2. polyABinDb: dictionary containing annotated polyA tails organized per chromosome (keys) into genomic bins (values)
        2. repeatsBinDb: dictionary containing annotated retrotransposons organized per chromosome (keys) into genomic bins (values)
        3. transducedBinDb: dictionary containing source element transduced regions (keys) into genomic bins (values)
        4. exonsBinDb: dictionary containing exons organized per chromosome (keys) into genomic bins (values)
        5. readSize: read size

    Output:
        1. discordantsIdentity: dictionary containing lists of discordant read pairs organized taking into account their orientation and if the mate aligns in an annotated retrotransposon 
                                This info is encoded in the dictionary keys as follows. Keys composed by 3 elements separated by '_':
                                
                                    - Orientation: read orientation (PLUS or MINUS)
                                    - Event type: DISCORDANT   
                                    - Type: identity type. It can be retrotransposon family (L1, Alu, ...), source element (22q, 5p, ...), viral strain (HPV, ...)
    '''
    
    ## 0. Assess if discordant read pairs support poly tail if pA repeats database provided
    if polyABinDb is not None:
        discordantsPolyA = annotation.intersect_mate_annotation(discordants, polyABinDb, ['family'], readSize)

        ## Separate discordants matching from those not matching source elements
        discordants = []

        if 'PLUS_DISCORDANT_None' in discordantsPolyA:
            discordants += discordantsPolyA['PLUS_DISCORDANT_None']
            discordantsPolyA.pop('PLUS_DISCORDANT_None', None)

        if 'MINUS_DISCORDANT_None' in discordantsPolyA:
            discordants += discordantsPolyA['MINUS_DISCORDANT_None']
            discordantsPolyA.pop('MINUS_DISCORDANT_None', None)
    else:

        discordantsPolyA = {}
    
    ## 1. Assess if discordant read pairs support transduction insertion if transduction database provided
    if transducedBinDb is not None:   
        discordantsTd = annotation.intersect_mate_annotation(discordants, transducedBinDb, ['family', 'cytobandId', 'srcEnd'], readSize)

        ## Separate discordants matching from those not matching source elements
        discordants = []
                
        if 'PLUS_DISCORDANT_None' in discordantsTd:
            discordants += discordantsTd['PLUS_DISCORDANT_None']
            discordantsTd.pop('PLUS_DISCORDANT_None', None)

        if 'MINUS_DISCORDANT_None' in discordantsTd:
            discordants += discordantsTd['MINUS_DISCORDANT_None']
            discordantsTd.pop('MINUS_DISCORDANT_None', None)
    else:

        discordantsTd = {}
        
    ## 2. Assess if discordant read pairs support pseudogene insertion if exons database provided
    if exonsBinDb is not None:
        discordantsExons = annotation.intersect_mate_annotation(discordants, exonsBinDb, ['geneName'], readSize)

        ## Separate discordants matching from those not matching source elements
        discordants = []
        
        if 'PLUS_DISCORDANT_None' in discordantsExons:
            discordants += discordantsExons['PLUS_DISCORDANT_None']
            discordantsExons.pop('PLUS_DISCORDANT_None', None)

        if 'MINUS_DISCORDANT_None' in discordantsExons:
            discordants += discordantsExons['MINUS_DISCORDANT_None']
            discordantsExons.pop('MINUS_DISCORDANT_None', None)
    else:

        discordantsExons = {}

    ## 3. Assess if discordant read pairs support retrotransposons insertion if repeats database provided
    if repeatsBinDb is not None:
        discordantsRt = annotation.intersect_mate_annotation(discordants, repeatsBinDb, ['family'], readSize)
        
        if 'PLUS_DISCORDANT_None' in discordantsRt:
            discordantsRt.pop('PLUS_DISCORDANT_None', None)

        if 'MINUS_DISCORDANT_None' in discordantsRt:
            discordantsRt.pop('MINUS_DISCORDANT_None', None)

    else:
        discordantsRt = {}

    ## 3. Merge discordant read pairs supporting RT and transduction insertions if transduction database provided    
    #discordantsIdentity = structures.merge_dictionaries([discordantsPolyA, discordantsTd, discordantsRt, discordantsExons])
    discordantsIdentity = structures.merge_dictionaries([discordantsTd, discordantsRt, discordantsExons])
    
    return discordantsIdentity, discordantsPolyA


## FUNCTIONS CALL-TDS ##
def identity_metaclusters(metaclusters, bam, normalBam, outDir, srcId):
    '''
    Determine metaclusters identity. If there is only a cluster and it contains a polyA tail, 
    it will be clasified as a partnered event. Else, it will be an orphan transduction. 
    
    Partnered:
    --------->
             ----AAAAAA>
       --------->
           ------AAAA>

    Orphan:
    --------->
             ----ACGTCA>
       --------->
           ------ACG>
    
    Input:
    1. metaclusters: List of metaclusters
    2. bam: Bam file
    3. normalBam: matched normal bam file
    4. outDir: output directory
    5. srcId: metacluster source id

    Output:
    Fill metacluster identity attribute with 'partnered' or 'orphan'    
    '''
    
    # set new confDict parameters to search for clippings
    newconfDict = {}
    newconfDict['targetEvents'] = ['CLIPPING']
    newconfDict['minMAPQ'] = 30
    newconfDict['minCLIPPINGlen'] = 8
    newconfDict['overhang'] = 0
    newconfDict['filterDup'] = True
    newconfDict['readFilters'] = ['mateUnmap', 'insertSize', 'SMS']

    # for each metacluster
    for metacluster in metaclusters:
        
        # set src id
        metacluster.src_id = srcId
        
        # if there is no reciprocal clusters
        if metacluster.orientation != 'RECIPROCAL':

            ## 1. Collect clippings in region
            # a) Paired sample mode (tumour & matched normal)
            buffer = 100
            if normalBam:
                eventsDict = bamtools.collectSV_paired(metacluster.ref, metacluster.bkpA-buffer, metacluster.bkpB+buffer, bam, normalBam, newconfDict, supplementary = False)
            # b) Single sample mode
            else:
                eventsDict = bamtools.collectSV(metacluster.ref, metacluster.bkpA-buffer, metacluster.bkpB+buffer, bam, newconfDict, None, supplementary = False)

            ## 2. Create clipping consensus
            # create bkp dir
            bkpDir = outDir + '/BKP'
            unix.mkdir(bkpDir)
            
            # initialize variable 
            clipConsensus = None
            
            # if cluster orientation is plus
            if metacluster.orientation == 'PLUS':
                
                # if there is only a clipping event
                if len(eventsDict['RIGHT-CLIPPING']) == 1:
                    clipConsensus = eventsDict['RIGHT-CLIPPING'][0].clipped_seq()
                
                # if there is more than a clipping event
                elif len(eventsDict['RIGHT-CLIPPING']) > 1:
                    clipConsensusPath, clipConsensus = bkp.makeConsSeqs(eventsDict['RIGHT-CLIPPING'], 'INT', bkpDir)
            
            # if cluster orientation is minus
            elif metacluster.orientation == 'MINUS':
                
                # if there is only a clipping event
                if len(eventsDict['LEFT-CLIPPING']) == 1:
                    clipConsensus = eventsDict['LEFT-CLIPPING'][0].clipped_seq()
                
                # if there is more than a clipping event
                elif len(eventsDict['LEFT-CLIPPING']) > 1:
                    clipConsensusPath, clipConsensus = bkp.makeConsSeqs(eventsDict['LEFT-CLIPPING'], 'INT', bkpDir)
            
            ## 3. polyA search if there is a consensus
            if clipConsensus:
                
                # set metacluster identity to partnered if there is polyA/polyT tail in consensus seq
                if has_polyA_illumina(clipConsensus): metacluster.identity = 'partnered'
        
        # set metacluster identity to orphan if metacluster not partnered
        if metacluster.identity != 'partnered': metacluster.identity = 'orphan'


def identity_metaclusters_wgs(metaclusters, bam, normalBam, outDir, confDict, annotations, srcId):
    '''
    Determine metaclusters identity using discordants around the insertion
    
    Input:
    1. metaclusters: list of metaclusters
    2. bam
    3. normalBam
    4. outDir
    5. confDict: original config dictionary
    6. annotations: nested dictionary of ['REPEATS', 'TRANSDUCTIONS'] (first keys) containing 
                    annotated repeats organized per chromosome (second keys) into genomic bins (values)
    7. srcId: metacluster source id 

    Output: metacluster.identity attribute is filled
    '''
    
    # create newconfDict to collect discordants around insertion
    newconfDict = confDict
    newconfDict['targetEvents'] = ['DISCORDANT', 'CLIPPING']
    newconfDict['minMAPQ'] = 20
    newconfDict['filterDup'] = True
    newconfDict['readFilters'] = ['mateUnmap', 'insertSize', 'SMS']
    
    # for each metacluster
    for metacluster in metaclusters:
        
        # set src id
        metacluster.src_id = srcId
        
        # collect discordants in region
        buffer = confDict['readSize'] * 2
        # a) Paired sample mode (tumour & matched normal)
        if normalBam:
            eventsDict = bamtools.collectSV_paired(metacluster.ref, metacluster.bkpA-buffer, metacluster.bkpB+buffer, bam, normalBam, newconfDict, supplementary = False)

        # b) Single sample mode
        else:
            eventsDict = bamtools.collectSV(metacluster.ref, metacluster.bkpA-buffer, metacluster.bkpB+buffer, bam, newconfDict, None, supplementary = False)

        # determine identity if there is discordants
        if 'DISCORDANT' in eventsDict.keys():
            
            ## 1. Transform clippings in discordants
            clippings = []
            if 'LEFT-CLIPPING' in eventsDict.keys(): clippings += eventsDict['LEFT-CLIPPING']
            if 'RIGHT-CLIPPING' in eventsDict.keys(): clippings += eventsDict['RIGHT-CLIPPING']
                
            discordants_SA = events.SA_as_DISCORDANTS(clippings, confDict)
            discordants = eventsDict['DISCORDANT'] + discordants_SA
            
            ## 2. Determine discordant identity
            discordantsIdentity, discordantsPolyA = determine_discordant_identity_MEIs(discordants, annotations['REPEATS-POLYA'], annotations['RETROTRANSPOSONS'], annotations['TRANSDUCTIONS'], annotations['EXONS'], 20)
            discordantEventsIdent = structures.merge_dictionaries([discordantsIdentity, discordantsPolyA])
            
            # Add pA support 
            discordantsIdentDict = add_polyA_discordantsIdentDict(discordantEventsIdent)
            
            ## 3. Determine metacluster identity
            determine_MEI_type_discordants(metacluster, discordantsIdentDict)
            
            ## 4. Add events that contributed to identity determination
            listEvents = [event for key,events in discordantsIdentDict.items() if metacluster.identity in key for event in events]
            metacluster.addEvents(listEvents)
            
            
            
## FUNCTIONS FROM GAPI ##
def has_polyA_illumina(targetSeq):
    '''
    Search for polyA/polyT tails in consensus sequence of Illumina clipping events
    
    Input:
    1. targetSeq: consensus sequence of Illumina clipping events
    
    Output:
    1. has_polyA: True or False
    '''
    
    ## 0. Set up monomer searching parameters ##
    windowSize = 8
    maxWindowDist = 2
    minMonomerSize = 8
    minPurity = 95
    maxDist2Ends = 1 
    
    monomerTails = []
    
    ## 1. Search for polyA/polyT at the sequence ends ##
    targetMonomers = ['T', 'A']
        
    for targetMonomer in targetMonomers:
        
        monomers = sequences.find_monomers(targetSeq, targetMonomer, windowSize, maxWindowDist, minMonomerSize, minPurity)
        filtMonomers = sequences.filter_internal_monomers(monomers, targetSeq, maxDist2Ends, minMonomerSize)
        monomerTails += filtMonomers if filtMonomers is not None else monomerTails
    
    while [] in monomerTails: monomerTails.remove([])    
    has_polyA = True if monomerTails != [] else False
    
    return has_polyA

      
def add_polyA_discordantsIdentDict(discordantEventsIdent):
    '''
    Add polyA support to other clusters with the same orientation
    
    Input: 
    1. discordantEventsIdent: Dictionary with keys following this pattern 'PLUS_DISCORDANT_L1', 'MINUS_DISCORDANT_Simple_repeat', 'PLUS_DISCORDANT_22q12.1'
    
    Output: 
    1. discordantEventsIdent: The same dictionary but events supporting polyA are merged into other identities with the same orientation
    '''
    
    for orientation in ['PLUS', 'MINUS']:
        
        pA_key = orientation + '_DISCORDANT_Simple_repeat'
        
        # if there is polyA key in dict keys
        if pA_key in discordantEventsIdent.keys():
            
            # for all identity keys
            for identity in discordantEventsIdent.keys():
                
                # select keys with same orientation but discard polyA key
                if identity != pA_key and orientation in identity:
                    
                    # add polyA events to other identity keys with same orientation
                    discordantEventsIdent[identity] = discordantEventsIdent[identity] + discordantEventsIdent[pA_key]
                        
    return(discordantEventsIdent)
    

def determine_MEI_type_discordants(metacluster, discordantsIdentDict):
    '''
    Input:
    1. metacluster: metacluster object
    2. discordantsIdentDict: dict of dicordants ordered by orientation and mate identity (keys)
    
    Output: metacluster.identity attribute is filled
    '''
    # 0. Reinitialize metacluster identity attributes
    metacluster.plus_id, metacluster.minus_id = None, None
    metacluster.plus_src_id, metacluster.minus_src_id = None, None
    metacluster.plus_src_end, metacluster.minus_src_end = None, None
    
    # split discordantsIdentDict by cluster orientation
    plus_identityDict = {key: len(value) for key, value in discordantsIdentDict.items() if 'PLUS' in key}
    minus_identityDict = {key: len(value) for key, value in discordantsIdentDict.items() if 'MINUS' in key}

    # select most supported identity of plus cluster
    if plus_identityDict:
        plus_identity = max(plus_identityDict.items(), key=operator.itemgetter(1))[0].split('_', 2)[2]
        
        plus_id = plus_identity.split('_')
        metacluster.plus_id = plus_id[0]
                    
        # if it's a TD
        if len(plus_id) == 3:
            metacluster.plus_src_id, metacluster.plus_src_end = plus_id[1], plus_id[2]
        
    # select most supported identity of minus cluster   
    if minus_identityDict:
        minus_identity = max(minus_identityDict.items(), key=operator.itemgetter(1))[0].split('_', 2)[2]
    
        minus_id = minus_identity.split('_')
        metacluster.minus_id = minus_id[0]
                    
        # if it's a TD
        if len(minus_id) == 3:
            metacluster.minus_src_id, metacluster.minus_src_end = minus_id[1], minus_id[2]

    ## 1. Set identities
    identities = [metacluster.plus_id, metacluster.minus_id]
    src_ids = [metacluster.plus_src_id, metacluster.minus_src_id]
    src_ends = [metacluster.plus_src_end, metacluster.minus_src_end]
    
    while None in identities: identities.remove(None)
    while None in src_ids: src_ids.remove(None)
    while None in src_ends: src_ends.remove(None)
    
    ## If both clusters have the identity and it's the same
    if len(identities) == 2 and len(set(identities)) == 1:
        
        # if no cluster pointing to a TD --> solo and PSD insertions
        if not src_ids:
        
            if metacluster.plus_id == 'L1':
                metacluster.identity = 'L1'
                metacluster.ins_type = 'TD0'
            
            elif metacluster.plus_id == 'Alu':
                metacluster.identity = 'Alu'
                metacluster.ins_type = 'TD0'
                
            elif metacluster.plus_id == 'SVA':
                metacluster.identity = 'SVA'
                metacluster.ins_type = 'TD0'
                
            elif metacluster.plus_id == 'Simple_repeat':
                metacluster.identity = 'pA'
                metacluster.ins_type = 'TD0'
            
            elif metacluster.plus_id:
                metacluster.identity = metacluster.plus_id
                metacluster.ins_type = 'PSD'
        
        # there is a source identifier and a src end --> TD
        elif len(set(src_ids)) == 1 and len(set(src_ends)) == 1:
            
            # partnered
            if len(src_ids) == 1:
                metacluster.identity = metacluster.plus_id
                metacluster.ins_type = 'TD1'
                # BUG
                # metacluster.src_id = metacluster.plus_src_id
                # metacluster.src_end = metacluster.plus_src_end
                metacluster.src_id = '_'.join(list(set(src_ids)))
                metacluster.src_end = '_'.join(list(set(src_ends)))
                metacluster.src_type = 'GERMLINE' 
            
            # orphan
            elif len(src_ids) == 2:
                metacluster.identity = metacluster.plus_id
                metacluster.ins_type = 'TD2'
                # BUG
                # metacluster.src_id = metacluster.plus_src_id
                # metacluster.src_end = metacluster.plus_src_end
                metacluster.src_id = '_'.join(list(set(src_ids)))
                metacluster.src_end = '_'.join(list(set(src_ends)))
                metacluster.src_type = 'GERMLINE' 
        
        # not canonical
        else:
            metacluster.identity = metacluster.plus_id
            metacluster.ins_type = 'UNRESOLVED-TD'
            if src_ids: metacluster.src_id = '_'.join(src_ids)
            if src_ends: metacluster.src_end = '_'.join(src_ends)
    
    # if not both clusters have identity or it is different
    else:
        
        # if only one cluster has identity
        if len(identities) == 1:
            metacluster.identity = identities[0]
            metacluster.ins_type = 'RG'
        
        # if both clusters have identity, but are different
        elif len(identities) == 2:
            
            # if one of the clusters is pA support
            if 'Simple_repeat' in identities:
                identities.remove('Simple_repeat')
                metacluster.identity = identities[0]
                
                if metacluster.identity in ['L1', 'Alu', 'SVA']:
                    metacluster.ins_type = 'TD2' if src_ids else 'TD0-TD1'
                else:
                    metacluster.ins_type = 'PSD'
                        
            else:
                metacluster.identity = '_'.join(identities)
                metacluster.ins_type = 'UNDEFINED'
        
        # set src attributes if TD
        if src_ids: metacluster.src_id = '_'.join(list(set(src_ids)))
        if src_ends: metacluster.src_end = '_'.join(list(set(src_ends)))
        if metacluster.src_id: metacluster.src_type = 'GERMLINE'


def metaclusters_MEI_type(metaclusters):
    '''
    Determine metaclusters identity
    
    Input:
    1. metaclusters: list of metaclusters
    
    Output: metacluster.identity attribute is filled
    '''
    for metacluster in metaclusters:
        
        metacluster.define_identityAttrib()
        
        ## 1. Set identity
        identities = [metacluster.plus_id, metacluster.minus_id]
        src_ids = [metacluster.plus_src_id, metacluster.minus_src_id]
        src_ends = [metacluster.plus_src_end, metacluster.minus_src_end]
        
        while None in identities: identities.remove(None)
        while None in src_ids: src_ids.remove(None)
        while None in src_ends: src_ends.remove(None)
        
        ## If both clusters have the identity and it's the same
        if len(identities) == 2 and len(set(identities)) == 1:
            
            # if no cluster pointing to a TD --> solo and PSD insertions
            if not src_ids:
            
                if metacluster.plus_id == 'L1':
                    metacluster.identity = 'L1'
                    metacluster.ins_type = 'TD0'
                
                elif metacluster.plus_id == 'Alu':
                    metacluster.identity = 'Alu'
                    metacluster.ins_type = 'TD0'
                    
                elif metacluster.plus_id == 'SVA':
                    metacluster.identity = 'SVA'
                    metacluster.ins_type = 'TD0'
                    
                elif metacluster.plus_id == 'Simple_repeat':
                    metacluster.identity = 'pA'
                    metacluster.ins_type = 'TD0'
                
                elif metacluster.plus_id:
                    metacluster.identity = identities[0]
                    metacluster.ins_type = 'PSD'
            
            # there is a source identifier and a src end --> TD
            elif len(set(src_ids)) == 1 and len(set(src_ends)) == 1:
                
                # partnered
                if len(src_ids) == 1:
                    metacluster.identity = identities[0]
                    metacluster.ins_type = 'TD1'
                    metacluster.src_id = src_ids[0]
                    metacluster.src_end = src_ends[0]
                    metacluster.src_type = 'GERMLINE' 
                
                # orphan
                elif len(src_ids) == 2:
                    metacluster.identity = identities[0]
                    metacluster.ins_type = 'TD2'
                    metacluster.src_id = src_ids[0]
                    metacluster.src_end = src_ends[0]
                    metacluster.src_type = 'GERMLINE' 
            
            # not canonical
            else:
                metacluster.identity = identities[0]
                metacluster.ins_type = 'UNRESOLVED-TD'
                if src_ids: metacluster.src_id = '_'.join(src_ids)
                if src_ends: metacluster.src_end = '_'.join(src_ends)
        
        # if not both clusters have identity or it is different
        else:
            
            # if only one cluster has identity
            if len(identities) == 1:
                metacluster.identity = identities[0]
                metacluster.ins_type = 'RG'
            
            # if both clusters have identity, but are different
            elif len(identities) == 2:
                
                # if one of the clusters is pA support
                if 'Simple_repeat' in identities:
                    identities.remove('Simple_repeat')
                    metacluster.identity = identities[0]
                    
                    if metacluster.identity in ['L1', 'Alu', 'SVA']:
                        metacluster.ins_type = 'TD2' if src_ids else 'TD0-TD1'
                    else:
                        metacluster.ins_type = 'PSD'
                            
                else:
                    metacluster.plus_count = len(set([event.readName for event in metacluster.events if (event.orientation == 'PLUS' and event.sample != 'NORMAL')]))
                    metacluster.minus_count = len(set([event.readName for event in metacluster.events if (event.orientation == 'MINUS' and event.sample != 'NORMAL')]))
                    
                    # estimate the number of effective reads supporting each identity
                    effectivePlus = metacluster.plus_count * metacluster.plus_idPerc
                    effectiveMinus = metacluster.minus_count * metacluster.minus_idPerc
                    
                    if effectivePlus > effectiveMinus:
                        metacluster.identity = metacluster.plus_id
                        metacluster.ins_type = 'UNDEFINED'
                        
                    elif effectivePlus < effectiveMinus:
                        metacluster.identity = metacluster.minus_id
                        metacluster.ins_type = 'UNDEFINED'
                        
                    else:
                        identities.sort()
                        metacluster.identity = '_'.join(identities)
                        metacluster.ins_type = 'UNDEFINED'
            
            # set src attributes if TD
            if src_ids: metacluster.src_id = '_'.join(list(set(src_ids)))
            if src_ends: metacluster.src_end = '_'.join(list(set(src_ends)))
            if metacluster.src_id: metacluster.src_type = 'GERMLINE'

