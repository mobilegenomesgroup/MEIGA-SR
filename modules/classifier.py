'''
Module 'classifier' - Contains classes and functions for implementing a machine learning classifier
'''

## DEPENDENCIES ##
# External
import pandas as pd
from joblib import load
import os
import sys

# Internal


## FUNCTIONS ##
def output_predictions(outDir):
    '''
    Apply machine learning classifier for filtering meiga output
    
    Input:
        1 . outDir: output directory
    
    Output:
        outDir + '/meiga_sr_calls.filter.pred.tsv': File with filtered calls according to ML model predictions
    '''
    ## Load data
    inFile = outDir + '/meiga_sr_calls.tsv'
    df = pd.read_csv(inFile, sep='\t')
    df.rename(columns=lambda x: x.strip(), inplace=True)

    ## Filter data
    # a. Select only L1 and pA isenrtions
    df = df[df["identity"].isin(["L1", "pA"])]
    # b. Select only MEIs
    df = df[(df["ins_type"] != "RG") & (df["ins_type"] != "PSD")]
    # c. write to output file
    df.to_csv(outDir + '/meiga_sr_calls.filter.tsv', sep='\t', index=False)

    ## if there is events 
    if len(df.index) > 0:
        
        ## Subset relevant features
        df_subset = df[["germPerc", "tumourPerc", "areaMAPQ", "areaSMS", 
                        "svsNormal", "metaRange", "plus_idPerc", "minus_idPerc", 
                        "nbReads", "nbTumour", "nbNormal", "nbDISCORDANT", "nbCLIPPING", 
                        "miliDiv", "plus_count", "minus_count", "pA", "plus_pA", 
                        "minus_pA", "matchRepeat", "identity", "ins_type", "src_end", 
                        "plus_id", "minus_id", "depthDiff", "upDepth", "downDepth", 
                        "normalDepth"]].copy()
        df_subset["src_end"].replace("None", "NA", inplace=True)

        ## Load trained model
        model = load(os.path.join(sys.path[0], 'modules/meiga.joblib'))

        ## Classify events
        y_pred = model.predict(df_subset)
        y_prob = model.predict_proba(df_subset)[:,1]

        ## Write predictions to output file
        # a. Join with full calls
        df["pred"] = y_pred
        df["prob"] = y_prob
        # b. Write output file
        outFile = outDir + '/meiga_sr_calls.filter.pred.tsv'
        df.to_csv(outFile, sep='\t', index=False)
    