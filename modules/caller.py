'''
Module 'callers' - Contains classes and functions for calling variants from next generation sequencing data
'''

## DEPENDENCIES ##
# External
import multiprocessing as mp
import logging
import pybedtools

# GAPI
from GAPI import log
from GAPI import unix
from GAPI import databases
from GAPI import formats
from GAPI import bamtools
from GAPI import structures
from GAPI import events
from GAPI import clusters
from GAPI import annotation
from GAPI import bkp
from GAPI import gRanges
from GAPI import depth

# Internal
from modules import output
from modules import bkp
from modules import retrotransposons as retrotransposons_MS
from modules import clustering as clustering_MS
from modules import clusters as clusters_MS
from modules import filters
from modules import classifier


## FUNCTIONS ##

## CLASSES ##
class MEI_caller():
    '''
    Mobile Element Insertion (MEI) caller for short paired-end sequencing data
    '''
    def __init__(self, mode, bam, normalBam, reference, refDir, confDict):

        self.mode = mode
        self.bam = bam
        self.normalBam = normalBam
        self.reference = reference
        self.refDir = refDir
        self.confDict = confDict
        self.outDir = confDict['outDir']
        self.logDir = confDict['logDir']
        self.repeatsBinDb = None

        ## Compute reference lengths
        self.refLengths = bamtools.get_ref_lengths(self.bam)

    def call(self):
        '''
        Search for mobile element insertions (MEI) genome wide or in a set of target genomic regions
        '''
        
        ### 1. Infer read size
        self.confDict['readSize'] = bamtools.infer_read_size(self.bam)
        
        
        ### 2. Set transduction regions
        # Select tdBed
        tdBed = self.refDir + '/tdRegions.bed' if self.confDict['tdBed'] == None else self.confDict['tdBed']
        
        # Organize bins into dictionary
        bins = bamtools.binning(tdBed, None, None, None)
        self.confDict['rangesDict'] = gRanges.rangeList2dict(bins)
        
        
        ### 3. Create SV clusters 
        msg = '1. Create SV clusters'
        log.header(msg)
        metaclusters = self.make_clusters()
        
        
        ### 4. Write output
        # apply ML if predict argument activated 
        if self.confDict['predict']:
            output.write_MEI_calls(metaclusters, self.outDir, self.bam, self.confDict)
            classifier.output_predictions(self.outDir)
            
        # if not predict, but debug mode activated, write output to tsv    
        elif self.confDict['debug']:
            output.write_MEI_calls(metaclusters, self.outDir, self.bam, self.confDict)
        
        # write output to VCF
        outName = 'meiga_sr_calls'
        output.write_MEI_calls_VCF(metaclusters, outName, self.outDir, self.reference, self.refLengths, self.confDict)
        
        
    def make_clusters(self):
        '''
        Search for structural variant (SV) clusters 

        Output:
            1. metaclustersPass: dictionary containing one key per SV type and the list of metaclusters identified of each given SV type
        '''
        
        ### 1. Define genomic bins to search for SV ###
        msg = '1. Define genomic bins'
        log.subHeader(msg)
        
        # target bins
        if self.confDict['targetBins']:
            
            # merge overlapping windows
            targetBins = self.confDict['targetBins']
            outFile = pybedtools.BedTool(targetBins)
            outBed = outFile.merge(d=100, header=False)
            outBed.saveas(targetBins)
        
        bins = bamtools.binning(self.confDict['targetBins'], self.bam, self.confDict['binSize'], self.confDict['targetRefs'])
        
        
        ### 2. Search for SV clusters in each bin ###
        msg = '2. Search for SV clusters'
        log.subHeader(msg)
        # unix.mkdir(self.outDir + '/CLUSTER/')

        # Genomic bins will be distributed into X processes
        pool = mp.Pool(processes=self.confDict['processes'])
        discordants = pool.map(self.make_clusters_bin, bins)
        pool.close()
        pool.join()
        
        
        ### 3. Load annotations ### 
        msg = '3. Load annotations'
        log.subHeader(msg)
        annotDir = self.outDir + '/ANNOT/'
        # annotations = annotation.load_annotations(['REPEATS-POLYA', 'RETROTRANSPOSONS', 'TRANSDUCTIONS', 'EXONS'], self.refLengths, self.refDir, self.confDict['germlineMEI'], self.confDict['processes'], annotDir, self.confDict['tdBed'])
        ## TMP: PSD search deactivated
        annotations = annotation.load_annotations(['REPEATS-POLYA', 'RETROTRANSPOSONS', 'TRANSDUCTIONS'], self.refLengths, self.refDir, self.confDict['germlineMEI'], self.confDict['processes'], annotDir, self.confDict['tdBed'])
        unix.rm([annotDir])
        
        
        ### 4. Determine discordant mates identity ###
        msg = '4. Determine discordant mates identity'
        log.subHeader(msg)
        ## TMP: PSD search deactivated
        # identities = [retrotransposons_MS.determine_discordant_identity_MEIs(discordantList, annotations['REPEATS-POLYA'], annotations['RETROTRANSPOSONS'], annotations['TRANSDUCTIONS'], annotations['EXONS'], 20) for discordantList in discordants]
        identities = [retrotransposons_MS.determine_discordant_identity_MEIs(discordantList, annotations['REPEATS-POLYA'], annotations['RETROTRANSPOSONS'], annotations['TRANSDUCTIONS'], None, 20) for discordantList in discordants]
        del annotations
        
        
        ### 5. Clusterize and filter MEI candidates ###
        msg = '5. Clusterize and filter MEI candidates'
        log.subHeader(msg)
        
        # Identity dictionaries for each bin will be distributed into X processes
        pool = mp.Pool(processes=self.confDict['processes'])
        metaclusters = pool.map(self.make_clusters_bin2, identities)
        pool.close()
        pool.join()
        
        metaclusters = [item for sublist in metaclusters for item in sublist]
        
        
        ### 6. Load annotations ###
        msg = '6. Load annotations'
        log.subHeader(msg)
        annotDir = self.outDir + '/ANNOT/'
        annotations = annotation.load_annotations(['RETROTRANSPOSONS', 'GERMLINE-MEI'], self.refLengths, self.refDir, self.confDict['germlineMEI'], self.confDict['processes'], annotDir, self.confDict['tdBed'])
        unix.rm([annotDir])
        
        
        ### 7. Annotate and filter created clusters ###
        msg = '7. Annotate and filter created clusters'
        log.subHeader(msg)
        
        # A. Filter known germline MEI #
        if self.confDict['germlineMEI'] and metaclusters:
            filters2Apply = ['GERMLINE-MEI']
            metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META', germlineDB = annotations['GERMLINE-MEI'])
            
        # B. Annotate metaclusters #
        steps = ['RETROTRANSPOSONS']
        if self.confDict['annovarDir'] is not None: steps.append('GENE')
        annotation.annotate(metaclusters, steps, annotations, self.confDict['annovarDir'], annotDir, self.confDict['readSize'])
        del annotations
        
        # C. Filter by repeat annotation #
        filters2Apply = ['ANNOTATION']
        metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        
        ### 8. Clean up ###
        unix.rm([annotDir])
        unix.rm([self.outDir + '/CLUSTER/'])
               
        return metaclusters
    
    def make_clusters_bin(self, binA):
        '''
        Search for structural variant (SV) clusters in a genomic bin/window
        '''
        ref, beg, end = binA
        
        ## 0. Initialize log system ##
        logName = mp.current_process().name.replace('SpawnPoolWorker-', 'general_process_')
        logFile = self.logDir + '/' + logName + '.log'
        logFormat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s' 
        generalLogger = log.setup_logger(logName, logFile, logFormat, level=logging.DEBUG, consoleLevel=logging.WARNING)

        ## 1. Set bin id and create bin directory ##
        binId = '_'.join([str(ref), str(beg), str(end)])
        msg = 'SV calling in bin: ' + binId
        log.subHeader(msg)
        
        ## 1. Search for SV candidate events in the bam file/s ##      
        # a) Single sample mode
        if self.mode == "SINGLE":
            eventsDict = bamtools.collectSV(ref, beg, end, self.bam, self.confDict, None)

        # b) Paired sample mode (tumour & matched normal)
        else:
            eventsDict = bamtools.collectSV_paired(ref, beg, end, self.bam, self.normalBam, self.confDict)

        SV_types = sorted(eventsDict.keys())
        counts = [str(len(eventsDict[SV_type])) for SV_type in SV_types]
        
        step = 'COLLECT'
        msg = 'Number of SV events in bin (' + ','.join(['binId'] + SV_types) + '): ' + '\t'.join([binId] + counts)
        log.step(step, msg)
        
        ## 2. Transform clippings in pseudo-discordants
        step = 'SA AS DISCORDANTS'
        msg = 'Store supp alignments as discordant pairs'
        log.step(step, msg)
        
        clippings = [event for key in eventsDict.keys() if 'CLIPPING' in key for event in eventsDict[key]]
        discordants_SA = events.SA_as_DISCORDANTS(clippings, self.confDict)
        discordants = eventsDict['DISCORDANT'] + discordants_SA
        
        return discordants
    
    def make_clusters_bin2(self, identityDicts):

        discordantsIdentity, discordantsPolyA = identityDicts
        
        ## 1. Group events into clusters ##
        step = 'CLUSTERING'
        msg = 'Group events into clusters by identity' 
        log.step(step, msg)

        discordantClusters = clustering_MS.cluster_DISCORDANTS_byIdentity(discordantsIdentity, discordantsPolyA, self.confDict)
                
        msg = 'Number of created clusters: ' + str(len(discordantClusters))
        log.step(step, msg)
        
        ## 2. Cluster filtering ##
        step = 'FILTER-DISCORDANT'
        msg = 'Discordant cluster filtering'
        log.step(step, msg)
        
        filters2Apply = ['MIN-NBREADS', 'READ-DUP', 'CLUSTER-RANGE']
        if self.confDict['filterDup']:
            filters2Apply.remove('READ-DUP')
        filteredDiscordants = filters.filter_clusters(discordantClusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'DISCORDANT')
        
        ## 3. Create metaclusters ##
        step = 'META-CLUSTERING'
        msg = 'Group discordant mates and supplementary clusters into metaclusters'
        log.step(step, msg)
        
        if discordantClusters == []:
            return {}
        else:
            metaclusters = clustering_MS.create_metaclusters(filteredDiscordants, self.confDict['oppositeOrientBuffer'])
        
        ## 4. Ligthen up metaclusters ##
        clusters.lighten_up_metaclusters(metaclusters)
        
        ## 5. Filter metaclusters ##
        step = 'FILTER-METACLUSTERS'
        msg = 'Filter metaclusters'
        log.step(step, msg)
        
        # first round
        # filters2Apply = ['MIN-NBREADS', 'MAX-NBREADS', 'PSEUDO-DUP']
        filters2Apply = ['MIN-NBREADS', 'MAX-NBREADS']
        metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')

        # second round
        if self.mode == 'PAIRED':
            filters2Apply = ['GERMLINE-PERC']
            metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        # third round
        filters2Apply = ['AREAMAPQ', 'AREASMS']
        metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')

        ## 6. Determine metaclusters identity ##
        step = 'DEFINE-IDENTITY'
        msg = 'Define metaclusters identity'
        log.step(step, msg)
        retrotransposons_MS.metaclusters_MEI_type(metaclusters)
        
        # filtering
        filters2Apply = ['IDENTITY', 'REGULAR_SV']
        metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        ## 7. Determine metaclusters precise coordinates ##
        step = 'DETERMINE-BKP'
        msg = 'Determine metaclusters breakpoints'
        log.step(step, msg)
        # bkp.infer_bkp(metaclusters, self.confDict, self.bam, self.normalBam)
        [bkp.determine_bkp(metacluster, self.bam) for metacluster in metaclusters]
        
        # filtering
        if self.mode == 'PAIRED':
            filters2Apply = ['SVs-NORMAL']
            metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        # filtering
        filters2Apply = ['META-RANGE', 'GERMLINE-PERC', 'SRC-REGION']
        if self.mode == 'SINGLE':
            filters2Apply.remove('GERMLINE-PERC')
        metaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        
        ## 8. Determine coverage at metaclusters breakpoints ##
        step = 'DETERMINE-COV'
        msg = 'Determine coverage at metaclusters breakpoints'
        log.step(step, msg)
        
        # a. For tumour
        tumour_depthObj = depth.read_depth_caller(self.bam, 'TUMOUR', self.confDict)
        tumour_depthObj.bkp_depth(metaclusters)
        
        # b. For normal
        if self.mode == 'PAIRED':
            normal_depthObj = depth.read_depth_caller(self.normalBam, 'NORMAL', self.confDict)
            normal_depthObj.normal_depth(metaclusters)        
        
        if metaclusters == []: metaclusters = {}

        return metaclusters



class transduction_caller():
    '''
    Tranduction caller for Illumina sureselect data targetering source element´s downstream regions
    '''
    def __init__(self, mode, bam, normalBam, reference, refDir, confDict):

        self.mode = mode
        self.bam = bam
        self.normalBam = normalBam
        self.reference = reference
        self.refDir = refDir
        self.confDict = confDict
        self.outDir = confDict['outDir']
        self.logDir = confDict['logDir']
        self.repeatsBinDb = None

        ## Compute reference lengths
        self.refLengths = bamtools.get_ref_lengths(self.bam)

    def call(self):
        '''
        Search for structural variants (SV) in a set of target genomic regions
        '''

        ### 1. Infer read size
        self.confDict['readSize'] = bamtools.infer_read_size(self.bam)
        
        ### 2. Get bed file containing source elements transduced intervals
        transducedPath = self.refDir + '/tdRegions.bed' if self.confDict['tdBed'] == None else self.confDict['tdBed']
        
        ### 3. Load annotations if running method on wgs data
        if self.confDict['retroTestWGS']:
            annotDir = self.outDir + '/ANNOT/'
            self.annotations = annotation.load_annotations(['REPEATS-POLYA', 'RETROTRANSPOSONS', 'TRANSDUCTIONS'], self.refLengths, self.refDir, self.confDict['germlineMEI'], self.confDict['processes'], annotDir, self.confDict['tdBed'])
            unix.rm([annotDir])
            
        ### 3. Define genomic bins to search for SV (will correspond to transduced areas)
        bins = bamtools.binning(transducedPath, None, None, None)

        ## Organize bins into a dictionary
        self.confDict['rangesDict'] = gRanges.rangeList2dict(bins)

        ### 4. Associate to each bin the src identifier
        BED = formats.BED()
        BED.read(transducedPath, 'List', None)   
        
        for index, coords in enumerate(bins):
            coords.append(BED.lines[index].optional['cytobandId'])
            coords.append(BED.lines[index].optional['family'])
            coords.append(BED.lines[index].optional['srcEnd'])
        
        ### 5. Search for SV clusters in each bin 
        # Genomic bins will be distributed into X processes
        pool = mp.Pool(processes=self.confDict['processes'])
        clusterPerSrc = pool.starmap(self.make_clusters_bin, bins)
        pool.close()
        pool.join()
        
        # Convert into dictionary
        clusterPerSrcDict = {regionId:clusters for regionId, clusters in clusterPerSrc}

        ### 6. Annotate metaclusters
        # Create output directory
        annotDir = self.outDir + '/ANNOT/'
        unix.mkdir(annotDir)
        if self.confDict['annovarDir'] is not None:
            metaclusters = [cluster for regionId, clusters in clusterPerSrc for cluster in clusters]
            annotation.annotate(metaclusters, ['GENE'], None, self.confDict['annovarDir'], annotDir)
        
        ### 7. Write calls to file
        output.write_transduction_calls(clusterPerSrcDict, self.outDir, self.confDict['germlineMEI'])
        
        ### 8. Clean up
        unix.rm([tdDir])
        unix.rm([annotDir])
        unix.rm([self.outDir + '/SUPPLEMENTARY'])
        unix.rm([self.outDir + '/BKP'])
            

    def make_clusters_bin(self, ref, beg, end, srcId, family, srcEnd):
        '''
        Search for structural variant (SV) clusters in a genomic bin/window
        '''
        binId = '_'.join([str(ref), str(beg), str(end)])

        ## 0. Initialize log system ##
        logName = mp.current_process().name.replace('SpawnPoolWorker-', 'general_process_')
        logFile = self.logDir + '/' + logName + '.log'
        logFormat = '%(asctime)s - %(name)s - %(levelname)s - %(message)s' 
        generalLogger = log.setup_logger(logName, logFile, logFormat, level=logging.DEBUG, consoleLevel=logging.WARNING)
        
        ## 1. Set bin id and create bin directory ##
        binId = '_'.join([str(ref), str(beg), str(end)])
        msg = 'SV calling in bin: ' + binId
        log.subHeader(msg)

        ## 1. Search for discordant and clipped read events in the bam file/s ##
        # a) Single sample mode
        if self.mode == "SINGLE":
            eventsDict = bamtools.collectSV(ref, beg, end, self.bam, self.confDict, None)

        # b) Paired sample mode (tumour & matched normal)
        else:
            eventsDict = bamtools.collectSV_paired(ref, beg, end, self.bam, self.normalBam, self.confDict)

        step = 'COLLECT'
        SV_types = sorted(eventsDict.keys())
        counts = [str(len(eventsDict[SV_type])) for SV_type in SV_types]
        msg = 'Number of SV events in bin (' + ','.join(['binId'] + SV_types) + '): ' + '\t'.join([binId] + counts)
        log.step(step, msg)

        ## If search for supplementary alignments selected:
        if self.confDict['blatClip']:
            
            ## 2. Search for supplementary alignments by realigning the clipped sequences
            step = 'SEARCH4SUPPL'
            msg = 'Search for supplementary alignments by realigning the clipped sequences'
            log.step(step, msg)

            ## Create output directory 
            supplDir = self.outDir + '/SUPPLEMENTARY/' + srcId + family + srcEnd
            unix.mkdir(supplDir)
            
            ## Left-clippings
            events.search4supplementary(eventsDict['LEFT-CLIPPING'], self.reference, srcId, supplDir)
                
            ## Rigth-clippings
            events.search4supplementary(eventsDict['RIGHT-CLIPPING'], self.reference, srcId, supplDir)

            ## Remove output directory
            unix.rm([supplDir])

        ## 3. Discordant and clipping clustering ##
        ## 3.1 Organize discordant and clipping events into genomic bins prior clustering ##
        step = 'BINNING'
        msg = 'Organize discordant and clipping events into genomic bins prior clustering'
        log.step(step, msg)
        
        ## Create bin database with discordants
        discordantsDict = {}
        discordantsDict['DISCORDANT'] = eventsDict['DISCORDANT']

        binSizes = [1000, 10000, 100000, 1000000]  
        discordantsBinDb = structures.create_bin_database_interval(ref, beg, end, discordantsDict, binSizes)

        ## Create bin database with clippings 
        clippingsDict = {}
        clippingsDict['LEFT-CLIPPING'] = eventsDict['LEFT-CLIPPING']
        clippingsDict['RIGHT-CLIPPING'] = eventsDict['RIGHT-CLIPPING']

        binSizes = [self.confDict['maxBkpDist'], 100, 1000, 10000, 100000, 1000000]
        clippingsBinDb = structures.create_bin_database_interval(ref, beg, end, clippingsDict, binSizes)
                
        ## 3.2 Group discordant and clipping events into clusters ##
        step = 'CLUSTERING'
        msg = 'Group discordant and clipping events into clusters'
        log.step(step, msg)
 
        ## Discordant clustering
        discordantClustersBinDb = clusters.create_clusters(discordantsBinDb, self.confDict)

        ## Clipping clustering
        clippingClustersBinDb = clusters.create_clusters(clippingsBinDb, self.confDict)

        ## 3.3 Group discordant read pairs based on mate position ##
        step = 'GROUP-BY-MATE'
        msg = 'Group discordant read pairs based on mate position'
        log.step(step, msg)

        ## Make groups
        discordants = clusters.cluster_by_matePos(discordantClustersBinDb.collect(['DISCORDANT']), self.refLengths, self.confDict['minClusterSize'])

        ## 3.4 Group clipping events based on suppl alignment position ##
        step = 'GROUP-BY-SUPPL'
        msg = 'Group clipping events based on suppl alignment position'
        log.step(step, msg)

        ## Left clipping
        leftClippingClusters = clusters.cluster_by_supplPos(clippingClustersBinDb.collect(['LEFT-CLIPPING']), self.refLengths, self.confDict['minClusterSize'], 'LEFT-CLIPPING')

        ## Right clipping
        rightClippingClusters = clusters.cluster_by_supplPos(clippingClustersBinDb.collect(['RIGHT-CLIPPING']), self.refLengths, self.confDict['minClusterSize'], 'RIGHT-CLIPPING')

        ## 4. Cluster filtering ##
        ## 4.1 Discordant cluster filtering ##
        step = 'FILTER-DISCORDANT'
        msg = 'Discordant cluster filtering'
        log.step(step, msg)

        filters2Apply = ['MIN-NBREADS', 'MATE-REF', 'MATE-SRC', 'MATE-MAPQ', 'UNSPECIFIC', 'READ-DUP', 'CLUSTER-RANGE']
        
        if self.confDict['retroTestWGS']:
            filters2Apply.remove('UNSPECIFIC')
        
        filteredDiscordants = filters.filter_clusters(discordants, filters2Apply, self.bam, self.normalBam, self.confDict, 'DISCORDANT')

        ## 4.2 Clipping cluster filtering ##
        step = 'FILTER-CLIPPING'
        msg = 'Clipping cluster filtering'
        log.step(step, msg)
        
        filters2Apply = ['MIN-NBREADS', 'SUPPL-REF', 'SUPPL-SRC', 'SUPPL-MAPQ', 'UNSPECIFIC', 'READ-DUP', 'CLUSTER-RANGE']
        filteredLeftClippings = filters.filter_clusters(leftClippingClusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'CLIPPING')
        filteredRightClippings = filters.filter_clusters(rightClippingClusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'CLIPPING')
                    
        ## 5. Create metaclusters ##
        step = 'META-CLUSTERING'
        msg = 'Group discordant mates and suplementary clusters into metaclusters'
        log.step(step, msg)
        metaclusters = clusters_MS.metacluster_mate_suppl(filteredDiscordants, filteredLeftClippings, filteredRightClippings, self.confDict)
        
        ## 6. Determine metaclusters precise coordinates ##
        step = 'DETERMINE-BKP'
        msg = 'Determine metaclusters breakpoints'
        log.step(step, msg)
        bkp.infer_bkp_tds(metaclusters, self.bam, self.confDict['readSize'])
        
        ## 7. Determine metaclusters identity ##
        step = 'DEFINE-TD-TYPE'
        msg = 'Define metaclusters transduction type'
        log.step(step, msg)

        if self.confDict['retroTestWGS']:
            retrotransposons_MS.identity_metaclusters_wgs(metaclusters, self.bam, self.normalBam, self.outDir, self.confDict, self.annotations, srcId)
        else:
            retrotransposons_MS.identity_metaclusters(metaclusters, self.bam, self.normalBam, self.outDir, srcId)
        
        ## 6. Determine metaclusters precise coordinates ##
        step = 'DETERMINE-BKP'
        msg = 'Determine metaclusters breakpoints'
        log.step(step, msg)
        bkp.infer_bkp_tds(metaclusters, self.bam, self.confDict['readSize'])
        
        ## 8. Filter metaclusters ##
        step = 'FILTER-METACLUSTERS'
        msg = 'Filter metaclusters'
        log.step(step, msg)
        
        filters2Apply = ['MIN-NBREADS', 'AREAMAPQ', 'AREASMS', 'IDENTITY', 'GERMLINE', 'GERMLINE-PERC', 'srcREF-TDs-ref6']
        
        if self.mode == 'SINGLE':
            filters2Apply.remove('GERMLINE')
            filters2Apply.remove('GERMLINE-PERC')

        filteredMetaclusters = filters.filter_clusters(metaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')
        
        # if running in wg mode, filter by annotation at insertion point
        if self.confDict['retroTestWGS']:
            
            ## 10. Annotate metaclusters ##
            step = 'ANNOTATE-BKP'
            msg = 'Annotate metaclusters'
            log.step(step, msg)
            annotation.repeats_annotation(filteredMetaclusters, self.annotations['RETROTRANSPOSONS'], self.confDict['readSize'])    
            
            ## 11. Last filtering step ##
            step = 'LAST-FILTERING'
            msg = 'Last filtering step '
            log.step(step, msg)
            
            filters2Apply = ['ANNOTATION']
            filteredMetaclusters = filters.filter_clusters(filteredMetaclusters, filters2Apply, self.bam, self.normalBam, self.confDict, 'META')

        regionId = srcId + '_' + family + '_' + srcEnd
        return [regionId, filteredMetaclusters]
