'''
Module to solve the bkp 
'''

## DEPENDENCIES ##
# External
from collections import Counter
import pysam
import numpy as np

# Internal
from GAPI import bkp
from GAPI import clusters


## FUNCTIONS CALL ##
def infer_bkp(metaclusters, confDict, bam, normalBam):
    '''
    Determine precise breakpoints
    
    Input: 
    1. metaclusters: List of metaclusters
    
    Output:
    There is no output, but refLeftBkp and refRightBkp attributes are filled
    '''
        
    for metacluster in metaclusters:
        
        ## A) Create subclusters
        subclusters = metacluster.create_subclusters()
        discordantKeys = set(['MINUS-DISCORDANT', 'PLUS-DISCORDANT']).intersection(subclusters.keys())
        
        ## B) Narrow metacluster coordinates
        # if it is a RECIPROCAL metacluster:
        if discordantKeys == {'MINUS-DISCORDANT', 'PLUS-DISCORDANT'}:
            
            begs = [event.beg for event in subclusters['MINUS-DISCORDANT'].events]
            beg = Counter(begs).most_common()[0][0]
            
            ends = [event.end for event in subclusters['PLUS-DISCORDANT'].events]
            end = Counter(ends).most_common()[0][0]
            
            if beg < end:
                metacluster.bkpA, metacluster.bkpB = beg, end
            else:
                metacluster.bkpA, metacluster.bkpB = end, beg
                
        # elif there is a MINUS-DISCORDANT cluster:
        elif discordantKeys == {'MINUS-DISCORDANT'}:
            
            begs = [event.beg for event in subclusters['MINUS-DISCORDANT'].events]
            beg = Counter(begs).most_common()[0][0]
            metacluster.bkpA, metacluster.bkpB = beg, beg
        
        # elif there is a PLUS-DISCORDANT cluster:
        elif discordantKeys == {'PLUS-DISCORDANT'}:
            
            ends = [event.end for event in subclusters['PLUS-DISCORDANT'].events]
            end = Counter(ends).most_common()[0][0]
            metacluster.bkpA, metacluster.bkpB = end, end
        
        ## C) Collect CLIPPINGS 
        metacluster.addClippings(confDict, bam, normalBam)
    
    ## D) Refine bkp using clipping info
    bkp.bkp_fromClusters(metaclusters)


def determine_bkp(metacluster, bam, buffer = 5):
    '''
    Search for bkp in a given region
    Input:
        1. metacluster
        2. bam
        3. buffer
    Output:
        No output, but the following attrib are filled:
        1. plus_bkp
        2. minus_bkp
    '''
    # Initialize counts
    left_clipping = []
    right_clipping = []
    plus_bkp = None
    minus_bkp = None
    
    # Get coordinates
    ref = metacluster.ref
    # estimate median from discordants coordinated
    plusBkps = np.array([event.beg for event in metacluster.events if event.orientation == "PLUS" and event.type == "DISCORDANT"])
    beg = np.median(plusBkps) if plusBkps.any() else metacluster.beg
    minusBkps = np.array([event.end for event in metacluster.events if event.orientation == "MINUS" and event.type == "DISCORDANT"])
    end = np.median(minusBkps) if minusBkps.any() else metacluster.end
    
    ## Open BAM file for reading in region
    bamFile = pysam.AlignmentFile(bam, "rb")
    if beg < end:
        iterator = bamFile.fetch(ref, beg-buffer, end+buffer)
    else:
        iterator = bamFile.fetch(ref, end-buffer, beg+buffer)

    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue    
    
        ## Determine clipping
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation = alignmentObj.cigartuples[-1][0]

        if (firstOperation == 4) or (firstOperation == 5):
            left_clipping.append(alignmentObj.reference_start)

        elif (lastOperation == 4) or (lastOperation == 5):
            right_clipping.append(alignmentObj.reference_end)
    
    if right_clipping:        
        plus_bkp = Counter(right_clipping).most_common()[0][0]
    
    if left_clipping:
        minus_bkp = Counter(left_clipping).most_common()[0][0]
    
    metacluster.plus_bkp = plus_bkp
    metacluster.minus_bkp = minus_bkp
    metacluster.TSD = False
    
    if plus_bkp and minus_bkp:
        
        if plus_bkp <= minus_bkp:
            metacluster.bkpA = plus_bkp
            metacluster.bkpB = minus_bkp
            
        else:
            metacluster.TSD = True
            metacluster.bkpA = minus_bkp
            metacluster.bkpB = plus_bkp
        
    # if only a bkp identified
    elif plus_bkp:
        metacluster.bkpA = plus_bkp
        metacluster.bkpB = None
        
    elif minus_bkp:
        metacluster.bkpA = minus_bkp
        metacluster.bkpB = None
           
    # if no bkp identified    
    else:
        metacluster.bkpA = None
        metacluster.bkpB = None


## FUNCTIONS CALL-TDS ##
def infer_bkp_tds(metaclusters, bam, readSize):
    '''
    Determine precise breakpoint in sureselect data:
    - if supplementary cluster in metacluster, use supplementary events coordinates
    - elif, use MINUS-DISC cluster beg coordinate
    - elif, use PLUS-DISC cluster end coordinate. It must be calculated for mate clusters
    
    Input: 
    1. metaclusters: List of metaclusters
    2. bam: Bam file
    3. readSize: Bam read size
    
    Output:
    There is no output, but refLeftBkp and refRightBkp attributes are filled
    '''
        
    for metacluster in metaclusters:
        
        # create subclusters
        subclusters = metacluster.create_subclusters()
        clippingKeys = set(['LEFT-CLIPPING', 'RIGHT-CLIPPING']).intersection(subclusters.keys())
        
        # if there is a SUPPLEMENTARY cluster:
        if 'SUPPLEMENTARY' in subclusters.keys():
            
            bkp = subclusters['SUPPLEMENTARY'].inferBkp_shortReads()
            metacluster.bkpA, metacluster.bkpB = bkp, bkp
        
        # elif there is a CLIPPING cluster:
        elif bool(clippingKeys):
            
            bkp = subclusters[[*clippingKeys][0]].infer_breakpoint()
            metacluster.bkpA, metacluster.bkpB = bkp, bkp
        
        # elif there is a MINUS-DISCORDANT cluster:
        elif 'MINUS-DISCORDANT' in subclusters.keys():
            
            metacluster.bkpA, metacluster.bkpB = subclusters['MINUS-DISCORDANT'].beg, subclusters['MINUS-DISCORDANT'].beg
        
        # elif there is a PLUS-DISCORDANT cluster:
        elif 'PLUS-DISCORDANT' in subclusters.keys():
                               
                readNames = set([event.readName for event in subclusters['PLUS-DISCORDANT'].events])
                
                fullCLuster = clusters.discCluster_from_readNames(readNames, metacluster.ref, metacluster.beg, metacluster.beg + 3*readSize, bam, 'TUMOUR')
                
                if fullCLuster:
                    metacluster.bkpA, metacluster.bkpB = fullCLuster.end, fullCLuster.end
       