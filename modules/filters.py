'''
Module 'filters' - Contains functions for filtering clusters
'''
## External
import pysam
import statistics
import numpy as np
from scipy.stats import ks_2samp
from collections import Counter
import multiprocessing as mp
import logging


## Internal
from GAPI import gRanges
from GAPI import bamtools
from GAPI import stats
from GAPI import log

from modules import output
from modules import bkp

###############
## FUNCTIONS ##
###############

def filter_clusters(clusters, filters2Apply, bam, normalBam, confDict, clusterType, germlineDB=None):
    '''
    Function to apply filters all discordant clusters. 

    Input:
        1. clusters: list of clusters
        2. filters2Apply: list containing the filters to apply 
        3. bam: path to bam file. None if not needed
        4. normalBam: path to matched normal bam file. None if not needed        
        5. confDict
        6. clusterType: 'CLIPPING', 'DISCORDANT', 'META'

    Output:
        1. clustersPass: List of clusters passing all the filters
    '''
    clustersPass = []
    
    # Get logger with filter-specific handler. It outputs discarded clusters to a file
    logName = mp.current_process().name.replace('SpawnPoolWorker-', 'filter_process_')
    logFile = confDict['logDir'] + '/' + logName + '.log'
    logFormat = '%(message)s'
    filterLogger = log.setup_logger(logName, logFile, logFormat, level=logging.DEBUG, consoleLevel=logging.WARNING)

    # For each cluster
    for cluster in clusters:

        ## Apply filters
        if clusterType == 'DISCORDANT':
            failedFilters = filter_discordant(cluster, filters2Apply, bam, normalBam, confDict)
            
        elif clusterType == 'CLIPPING':
            failedFilters = filter_clipping(cluster, filters2Apply, confDict)
        
        elif clusterType == 'META':
            failedFilters = filter_metacluster(cluster, filters2Apply, confDict, bam, germlineDB, normalBam, mode='SR')
                  
        # Cluster pass all the filters
        if not failedFilters: 
            clustersPass.append(cluster)
            
        else:
            # TO DO: Output failed clusters only in debug mode. Activate in the future.
            # if confDict['debug']:
            call = output.write_failedCluster(cluster, failedFilters)
            log.filter_info(call)
            
    # close log files and remove logger
    # log.remove_logger(filterLogger)

    return clustersPass

def filter_clipping(clipping, filters2Apply, confDict):
    '''
    Apply selected filters to a clipping cluster provided as input

    Input:
        1. clipping: clipping cluster object
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        5. confDict:

    Output:
        1. failedFilters -> list containing those filters that the clipping cluster didn´t pass.
    '''        
    failedFilters = []

    ## 1. FILTER 1: Minimum number of reads per cluster
    if 'MIN-NBREADS' in filters2Apply: 
        
        if not filter_min_nb_reads(clipping, confDict['minNbCLIPPING'], confDict['minNormalReads']):
            failedFilters.append('MIN-NBREADS')
            if not confDict['debug']: return failedFilters
        
    ## 2. FILTER 2: Filter out those clusters with suppl outside target reference ##
    if 'SUPPL-REF' in filters2Apply: 

        if not filter_clipping_suppl_ref(clipping, confDict['targetRefs']):
            failedFilters.append('SUPPL-REF')
            if not confDict['debug']: return failedFilters

    ## 3. FILTER 3: Filter out those clusters whose supplementary alignments map over any source element downstream region 
    if 'SUPPL-SRC' in filters2Apply:

        if not filter_clipping_suppl_position(clipping, confDict['rangesDict'], 10000):
            failedFilters.append('SUPPL-SRC')
            if not confDict['debug']: return failedFilters
    
    ## 4. FILTER 4: Average mapping quality of supplementary alignments 
    if 'SUPPL-MAPQ' in filters2Apply:
    
        if not filter_suppl_MAPQ(clipping, 10):
            failedFilters.append('SUPPL-MAPQ')
            if not confDict['debug']: return failedFilters

    ## 5. FILTER 5: filter out clusters formed by tumour and normal reads. Discard germline variation
    if 'GERMLINE' in filters2Apply:

        if not filter_germline(clipping, confDict['minNormalReads']):
            failedFilters.append('GERMLINE')
            if not confDict['debug']: return failedFilters

    ## 6. FILTER 6: Filter out clusters based on duplicate percentage (Ex: 40%) 
    if 'READ-DUP' in filters2Apply:

        if not filter_highDup_clusters(clipping, 75):
            failedFilters.append('READ-DUP')
            if not confDict['debug']: return failedFilters
    
    ## 7. FILTER 7: Filter out clusters based on cluster coordinates ##
    if 'CLUSTER-RANGE' in filters2Apply:
        
        if not filter_clusterRange_clipping(clipping):
            failedFilters.append('CLUSTER-RANGE')
            if not confDict['debug']: return failedFilters

    return failedFilters

def filter_discordant(discordant, filters2Apply, bam, normalBam, confDict):
    '''
    Apply selected filters to a discordant cluster provided as input

    Input:
        1. discordant: discordant read pair cluster object
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. bam: path to bam file. None if not needed
        4. normalBam: path to matched normal bam file. None if not needed
        5. confDict

    Output:
        1. failedFilters -> list containing those filters that the discordant cluster doesn't pass.
    '''        
    failedFilters = []

    ## 1. FILTER 1: Minimum number of reads per cluster
    if 'MIN-NBREADS' in filters2Apply:
        
        if not filter_min_nb_reads(discordant, confDict['minNbDISCORDANT'], confDict['minNormalReads']):
            failedFilters.append('MIN-NBREADS')
            if not confDict['debug']: return failedFilters
            
    ## 2. FILTER 2: Filter out those clusters with mates not over NOT target reference ##
    if 'MATE-REF' in filters2Apply: 

        if not filter_discordant_mate_ref(discordant, confDict['targetRefs']):
            failedFilters.append('MATE-REF')
            if not confDict['debug']: return failedFilters

    ## 3. FILTER 3: Filter out those clusters whose mates aligns over any source element downstream region ##
    if 'MATE-SRC' in filters2Apply:

        if not filter_discordant_mate_position(discordant, confDict['rangesDict'], 5000):
            failedFilters.append('MATE-SRC')
            if not confDict['debug']: return failedFilters
        
    ## 4. FILTER 4: Filter out clusters based on average MAPQ for mate alignments ##
    if 'MATE-MAPQ' in filters2Apply:
    
        if not filter_discordant_mate_MAPQ(discordant, 10, bam, normalBam):
            failedFilters.append('MATE-MAPQ')
            if not confDict['debug']: return failedFilters
        
    ## 5. FILTER 5: filter out clusters formed by tumour and normal reads. Discard germline variation (TILL HERE) 
    if 'GERMLINE' in filters2Apply:

        if not filter_germline(discordant, confDict['minNormalReads']):
            failedFilters.append('GERMLINE')
            if not confDict['debug']: return failedFilters
            
    ## 6. FILTER 6: Filter out clusters in unspecific regions ##
    if 'UNSPECIFIC' in filters2Apply:

        if not filter_discordant_mate_unspecific(discordant, 0.95, bam):
            failedFilters.append('UNSPECIFIC')
            if not confDict['debug']: return failedFilters

    ## 7. FILTER 7: Filter out clusters based on duplicate percentage (Ex: 50%) ##
    if 'READ-DUP' in filters2Apply:

        if not filter_highDup_clusters(discordant, 75):
            failedFilters.append('READ-DUP')
            if not confDict['debug']: return failedFilters
            
    ## 8. FILTER 8: Filter out clusters based on mates beg coordinates ##
    if 'CLUSTER-RANGE' in filters2Apply:
        
        if not filter_clusterRange_discordant(discordant):
            failedFilters.append('CLUSTER-RANGE')
            if not confDict['debug']: return failedFilters
    
    return failedFilters

def filter_metacluster(metacluster, filters2Apply, confDict, bam, germlineDB, normalBam = None, mode='SR'):
    '''
    Apply selected filters to one metacluster.

    Input:
        1. metacluster: metacluster object
        2. filters2Apply: list containing the filters to apply (only those filters that make sense with the cluster type will be applied)
        3. confDict
        4. bam  
        5. germlineDB: germline DB
        6. normalBam

    Output:
        1. failedFilters -> list containing those filters that the metacluster doesn't pass.
    '''        
    failedFilters = []

    ## 1. FILTER 1: Minimum number of reads per cluster
    if 'MIN-NBREADS' in filters2Apply:
        
        if not filter_min_nb_reads(metacluster, confDict['minReads'], confDict['minNormalReads']):
            failedFilters.append('MIN-NBREADS')
            if not confDict['debug']: return failedFilters

    ## 2. FILTER 2: Maximum number of reads per cluster
    if 'MAX-NBREADS' in filters2Apply:
        
        if not filter_max_nb_reads(metacluster, confDict['maxClusterSize']):
            failedFilters.append('MAX-NBREADS')
            if not confDict['debug']: return failedFilters

    ## 3. FILTER 3: Maximum Coefficient of Variance per cluster
    if ('CV' in filters2Apply) and ('INS' in metacluster.subclusters): 
        
        if not filter_max_cv(metacluster, confDict['maxClusterCV']):
            failedFilters.append('CV')
            if not confDict['debug']: return failedFilters

    ## 4. FILTER 4: Whether a metacluster has a SV_type assigned or not
    if 'SV-TYPE' in filters2Apply:
        
        if not filter_SV_type(metacluster, confDict['targetSV']):
            failedFilters.append('SV-TYPE')
            if not confDict['debug']: return failedFilters

    ## 5. FILTER 5: Minimum percentage of inserted sequence resolved
    if ('PERC-RESOLVED' in filters2Apply) and (metacluster.SV_type == 'INS') and ('PERC_RESOLVED' in metacluster.SV_features): 

        if not filter_perc_resolved(metacluster, confDict['minPercResolved']):
            failedFilters.append('PERC-RESOLVED')
            if not confDict['debug']: return failedFilters
           
    ## 6. FILTER 6: Area mapping quality
    ## 7. FILTER 7: Area clipping SMS
    if 'AREAMAPQ' in filters2Apply or 'AREASMS' in filters2Apply:
        
        areaMeta = area(metacluster, confDict, bam)
        
        if 'AREAMAPQ' in filters2Apply and not areaMeta[0]:
            failedFilters.append('AREAMAPQ')
        if 'AREASMS' in filters2Apply and not areaMeta[1]:
            failedFilters.append('AREASMS')
             
        if not confDict['debug'] and failedFilters: return failedFilters
        
    ## 8. FILTER 8: Whether a metacluster has a SV_type assigned or not
    if 'IDENTITY' in filters2Apply: 
        
        if not identityFilter(metacluster, mode=mode):
            failedFilters.append('IDENTITY')
            if not confDict['debug']: return failedFilters
                
    ## 9. FILTER 9: Whether a metacluster is germline
    if 'GERMLINE' in filters2Apply:
        if not filter_germline_metaclusters(metacluster, confDict['minNormalReads']):
            failedFilters.append('GERMLINE')
            if not confDict['debug']: return failedFilters
              
    ## 10. FILTER 10: Reciprocal metaclusters range 
    if 'META-RANGE' in filters2Apply: 
        if not filter_clusterRange_reciprocalMeta(metacluster, 40):
            failedFilters.append('META-RANGE')
            if not confDict['debug']: return failedFilters
               
    ## 11. FILTER 11: Whether a cluster is on a L1PA element and has no pA support
    if 'ANNOTATION' in filters2Apply:
        if metacluster.orientation != "RECIPROCAL":
            if not filter_metacluster_subfamilyAnnot(metacluster, ['L1', 'L1', 'Alu', 'Alu', 'SVA', 'SVA', 'Simple_repeat', 'Simple_repeat', 'pA', 'pA'], ['L1PA', 'L1HS', 'AluS', 'AluY', 'SVA_D', 'SVA_F', 'T', 'A', 'T', 'A'], 100):
                failedFilters.append('ANNOTATION')
                if not confDict['debug']: return failedFilters
                
    ## 12. FILTER 12: Whether a cluster is on noisy region (calculated using the normal)
    if 'SVs-NORMAL' in filters2Apply:
        # maxPercSVs = 10
        # if not filter_percSVs_inNormal(metacluster, maxPercSVs, normalBam, confDict):
        #     failedFilters.append('SVs-NORMAL')
        #     if not confDict['debug']: return failedFilters
        if not filter_normal_VAFs(normalBam, metacluster, minREF = 5, maxVAF = 0.05, minMAPQ = 20):
            failedFilters.append('SVs-NORMAL')
            if not confDict['debug']: return failedFilters
                        
    ## 13. FILTER 13: Whether a transduction and the source element are in chr6 with no pA support
    if 'srcREF-TDs-ref6' in filters2Apply:
        if not filter_TDs_chr6_srcElement(metacluster):
            failedFilters.append('srcREF-TDs-ref6')
            if not confDict['debug']: return failedFilters
               
    ## 14. FILTER 14: If all mates point to the same region with no ME bridge, discard      
    if 'REGULAR_SV' in filters2Apply:
        if not filter_regularSV(metacluster):
            failedFilters.append('REGULAR_SV')
            if not confDict['debug']: return failedFilters
                       
    ## 15. FILTER 15: Whether a metacluster has more than a percentage of normal supp reads      
    if 'GERMLINE-PERC' in filters2Apply:
        maxPercNormal = 0.4
        if not filter_germline_perc_metaclusters(metacluster, maxPercNormal):
            failedFilters.append('GERMLINE-PERC')
            if not confDict['debug']: return failedFilters
    
    ## 16. FILTER 16: Whether a germline variation has been described for the same id in the region     
    if 'GERMLINE-MEI' in filters2Apply and germlineDB:
        if not filter_germline_MEI([metacluster], germlineDB, 150):
            failedFilters.append('GERMLINE-MEI')
            if not confDict['debug']: return failedFilters
    
    ## 17. FILTER 17: Filter out those clusters located over any source element downstream region ##
    if 'SRC-REGION' in filters2Apply:
        if not filter_clusters_src_regions(metacluster, confDict['rangesDict']):
            failedFilters.append('SRC-REGION')
            if not confDict['debug']: return failedFilters

    ## 18. FILTER 18: Filter out those clusters formed by stacked reads ##
    if 'PSEUDO-DUP' in filters2Apply:
        if not filter_minReads_diffBeg(metacluster, confDict['minClusterSize'], confDict['minReads']/2):
            failedFilters.append('PSEUDO-DUP')
            if not confDict['debug']: return failedFilters
            
    return failedFilters



## FILTERS ##

def filter_min_nb_reads(cluster, minReads, minNormalReads):
    '''
    Filter cluster by comparing the number of supporting events with a minimum treshold

    Input:
        1. cluster: cluster object
        2. minReads: min number of events threshold
        3. minReads: min number of events threshold for normal sample

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    if cluster.events:
        
        ## 1. Compute number of events supporting the cluster
        # TO DO: Clean
        if not hasattr(cluster, 'nbReadsTotal'):
            nbTotal, nbTumour, nbNormal = cluster.supportingReads()[0:3]
        elif None in {cluster.nbReadsTotal, cluster.nbReadsTumour, cluster.nbReadsNormal}:
            nbTotal, nbTumour, nbNormal = cluster.supportingReads()[0:3]
        else:
            nbTotal, nbTumour, nbNormal = cluster.nbReadsTotal, cluster.nbReadsTumour, cluster.nbReadsNormal 

        ## 2. Compare the number of events supporting the cluster against the minimum required
        # 2.1 Paired mode:
        if nbTumour != None:

            if nbTumour >= minReads and nbNormal >= minNormalReads:
                cluster.mutOrigin = 'germline'
                PASS = True

            elif nbTumour >= minReads and not nbNormal >= minNormalReads:
                cluster.mutOrigin = 'somatic-tumour'
                PASS = True

            elif nbNormal >= minReads:
                cluster.mutOrigin = 'somatic-normal'
                PASS = True

            else:
                PASS = False

        # 2.1 Single mode:
        else:
            # If running in single mode (do no set mutation origin because it doesn't make sense)
            if nbTotal >= minReads:
                PASS = True
                
            else:
                PASS = False
                
    else:
        PASS = False    
    
    return PASS

def filter_max_nb_reads(metacluster, maxNbEvents):
    '''
    Filter metacluster by comparing the number of cluster supporting events with a maximum treshold

    Input:
        1. metacluster: metacluster object
        2. maxNbEvents: maximum number of events threshold
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## 1. Compute number of events supporting the cluster 
    # TO DO: Clean
    if not hasattr(metacluster, 'nbTotal'):
        nbTotal = metacluster.nbEvents()[0]
    elif not metacluster.nbTotal:
        nbTotal = metacluster.nbEvents()[0]
    else:
        nbTotal = metacluster.nbTotal

    ## 2. Compare the number of events supporting the cluster against the maximum required
    if nbTotal <= maxNbEvents:
        PASS = True
    else:
        PASS = False
    
    return PASS

def filter_max_cv(metacluster, maxClusterCV):
    '''
    Filter metacluster by comparing its Coefficient of Variation with a maximum threshold.

    Input:
        1. metacluster: metacluster object
        2. maxClusterCV: maximum Coefficient of Variation threshold
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## 1. Compute CV of the cluster 
    cv = metacluster.subclusters['INS'].cv_len()[1]

    ## 2. Compare the cluster CV against the maximum required
    if cv <= maxClusterCV:
        PASS = True
    else:
        PASS = False

    return PASS

def filter_SV_type(metacluster, targetSV):
    '''
    Filter metacluster by checking its SV type

    Input:
        1. metacluster: metacluster object
        2. targetSV: list containing list of target SV types
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    if metacluster.SV_type in targetSV:
        PASS = True 
        
    else:
        PASS = False

    return PASS

def identityFilter(cluster, mode ='SR'):
    '''
    Filter metacluster by checking its SV type

    Input:
        1. metacluster: metacluster object
        2. mode: SR or LR (Short Reads or Long Reads). Default='SR'
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    if cluster.identity != None:
        PASS = True 
        
    else:
        PASS = False

    if mode == 'LR':

        if 'IDENTITY' in cluster.SV_features.keys():
            if cluster.SV_features['IDENTITY']:
                PASS = True 
            else:
                PASS = False
        else:
            PASS = False  
    
    return PASS

def filter_suppl_MAPQ(clipping, minMAPQ):
    '''
    Filter out clipping cluster based on the average mapping quality of its supplementary alignments

    Input:
        1. clipping: clipping cluster object
        2. minMAPQ: minimum mapping quality

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't    
    '''

    ## Compute average mapping quality for supplementary alignments
    MAPQs = [event.mapQ for event in clipping.supplCluster.events]
    meanMAPQ = statistics.mean(MAPQs)

    ## Apply filter
    if meanMAPQ >= minMAPQ:
        PASS = True

    else:
        PASS = False
    
    return PASS

def filter_clipping_suppl_ref(clipping, targetRefs):
    '''
    Filter out clipping cluster if its supplementary cluster not located over a target reference

    Input:
        1. clipping: clipping cluster
        2. targetRefs: List of target references

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Retrieve suppl. alignment reference 
    if clipping.supplCluster.ref in targetRefs:
        PASS = True 

    else:
        PASS = False

    return PASS

def filter_discordant_mate_ref(discordant, targetRefs):
    '''
    Filter out discordant cluster if its mate not located over a target reference

    Input:
        1. discordant: discordant cluster object
        2. targetRefs: List of target references

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Retrieve mates referene 
    matesRef = discordant.events[0].mateRef

    if matesRef in targetRefs:
        PASS = True 
        
    else:
        PASS = False

    return PASS

def filter_clipping_suppl_position(clipping, ranges, buffer):
    '''
    Filter out clipping cluster if suppl. alignment within one of the provided regions
    
    Input:
        1. clipping: clipping cluster object
        2. ranges: Dictionary with reference ids as keys and the list of ranges on each reference as values
        3. buffer: Extend each range at their begin and end coordinate by a number of nucleotides == buffer length

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Do not filter out clipping cluster if no input range on that particular reference 
    if clipping.supplCluster.ref not in ranges:
        PASS = True
        return PASS
        
    ## Assess overlap between mates interval and provided regions. 
    PASS = True

    for interval in ranges[clipping.supplCluster.ref]:
        rangeBeg, rangeEnd = interval

        # Add buffer
        rangeBeg = rangeBeg - buffer
        rangeEnd = rangeEnd + buffer

        # Assess overlap
        overlap, overlapLen = gRanges.overlap(clipping.supplCluster.beg, clipping.supplCluster.end, rangeBeg, rangeEnd)

        if overlap:
            PASS = False

    return PASS

def filter_discordant_mate_position(discordant, ranges, buffer):
    '''
    Filter out discordant cluster if mates align within one of the provided regions

    Input:
        1. discordant: discordant cluster object
        2. ranges: Dictionary with reference ids as keys and the list of ranges on each reference as values
        3. buffer: Extend each range at their begin and end coordinate by a number of nucleotides == buffer length

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## Retrieve mates position and interval
    matesRef = discordant.events[0].mateRef
    matesBeg, matesEnd = discordant.mates_start_interval()

    ## Do not filter out discordant cluster if no input range on that particular reference 
    if matesRef not in ranges:
        PASS = True
        return PASS
        
    ## Assess overlap between mates interval and provided regions. 
    PASS = True

    for interval in ranges[matesRef]:
        rangeBeg, rangeEnd = interval

        # Add buffer
        rangeBeg = rangeBeg - buffer
        rangeEnd = rangeEnd + buffer

        # Assess overlap
        overlap, overlapLen = gRanges.overlap(matesBeg, matesEnd, rangeBeg, rangeEnd)

        if overlap:
            PASS = False

    return PASS

def filter_discordant_mate_MAPQ(discordant, minMAPQ, bam, normalBam):
    '''
    Filter out discordant clusters based on average MAPQ for mate alignments

    Input:
        1. discordant: discordant cluster object
        2. minMAPQ: minimum average of mapping quality for mate alignments
        3. bam: path to bam file containing alignments for discordant cluster supporting reads and their mate
        4. normalBam: path to the matched normal bam file. If running in single mode, set to 'None' 

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Open BAM files for reading
    bamFile = pysam.AlignmentFile(bam, "rb")

    if normalBam != None:
        bamFile_normal = pysam.AlignmentFile(normalBam, "rb")
        
    ## Define interval to search for mate alignment objects
    matesBeg, matesEnd = discordant.mates_start_interval()

    intervalRef = discordant.events[0].mateRef 
    intervalBeg = matesBeg - 500
    intervalEnd = matesEnd + 500

    ## Collect cluster supporting reads
    nbTotal, nbTumour, nbNormal, readIds, readIdsTumour, readIdsNormal = discordant.supportingReads()

    ## Compute average mapping quality for mates of cluster supporting reads
    # if running in single mode or running in paired but no reads from the normal are found in this interval
    if (normalBam == None or (normalBam != None and readIdsNormal == [])):
            
        avMAPQ = bamtools.average_MAPQ_interval(intervalRef, intervalBeg, intervalEnd, readIds, bamFile)
                    
        if avMAPQ >= minMAPQ:
            PASS = True

        else:
            PASS = False
                
    # if running in paired mode and there is reads in the interval belonging to the normal bam
    else:
            
        avMAPQ = bamtools.average_MAPQ_interval(intervalRef, intervalBeg, intervalEnd, readIdsTumour, bamFile)
        avMAPQ_normal = bamtools.average_MAPQ_interval(intervalRef, intervalBeg, intervalEnd, readIdsNormal, bamFile_normal)
            
        # tumor and normal MAPQ average
        avMAPQ_pair = (avMAPQ * len(readIdsTumour) + avMAPQ_normal * len(readIdsNormal))/len(readIdsTumour + readIdsNormal)          
             
        if avMAPQ_pair >= minMAPQ:
            PASS = True

        else:
            PASS = False

    ## Close 
    bamFile.close()

    if normalBam != None:
        bamFile_normal.close()
        
    return PASS

def filter_germline(cluster, minNormal):
    '''
    Filter out those clusters formed by tumour and normal reads
    
    Input:
        1. cluster: cluster object
        2. minNormal: minimum number of reads supporting a SV in normal sample
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    count = 0
        
    for event in cluster.events:
        
        if event.sample == 'NORMAL':
            count += 1
                
    if count < minNormal:
        PASS = True
        
    else:
        PASS = False
        
    return PASS

def filter_germline_metaclusters(metacluster, minNormal):
    '''
    Filter out those clusters formed by tumour and normal reads
    
    Input:
        1. cluster: cluster object
        2. minNormal: minimum number of reads supporting a SV in normal sample
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    PASS = True
    
    nbNormal = metacluster.supportingReads()[2]
    
    if nbNormal >= minNormal:
        PASS = False
        
    return PASS

def filter_germline_perc_metaclusters(metacluster, maxPercNormal):
    '''
    Filter out those clusters formed by tumour and normal reads if the perc of normal reads is greater than maxPercNormal
    
    Input:
        1. cluster: cluster object
        2. maxPercNormal: maximum percentage of reads supporting a SV in normal sample
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## 1. Compute number of events supporting the cluster
    nbTotal, nbTumour, nbNormal = metacluster.supportingReads()[0:3]
    
    ## 2. If percentage of normal reads is smaller than maxPercNormal, PASS is True
    PASS = False if nbNormal/nbTotal >= maxPercNormal else True
    
    ## fill metacluster attrib
    metacluster.germPerc = nbNormal/nbTotal
    metacluster.tumourPerc = nbTumour/nbTotal
    
    return PASS

def filter_discordant_mate_unspecific(discordant, threshold, bam):
    '''
    Filter out discordant whose mates are located in regions captured unspecifically
    Insertion points where there is more than discordant reads are likely to be false positives
    Example of recurrent false positive filtered: chr6:29765954 (hg19)
    
    Input:
        1. discordant: discordant cluster instance
        2. threshold: ratio of nbDiscordant reads between nbTotal reads
        3. bam: path to bam file

    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    # Open BAM file for reading
    bamFile = pysam.AlignmentFile(bam, "rb")
    
    nbProperPair = 0
    nbDiscordants, discordantList = discordant.nbReads()
        
    buffer = 200
    ref = discordant.events[0].mateRef
    beg, end = discordant.mates_start_interval()
        
    # Extract alignments
    iterator = bamFile.fetch(ref, beg - buffer, end + buffer)
        
    # Count properly paired reads in region
    properPairs = []
    for alignmentObj in iterator:
            
        if alignmentObj.is_proper_pair and not alignmentObj.is_supplementary:
                
                properPairs.append(alignmentObj.query_name)
                
    nbProperPair = len(set(properPairs))
        
    # if there are properly paired reads around insertion
    if nbProperPair > 0:
               
        if nbDiscordants/nbProperPair > threshold:        
            PASS = True

        else:  
            PASS = False

    else:
         
        PASS = True
            
    return PASS

def area(cluster,confDict,bam):
    '''
    Apply filters to cluster (SR bam) based on the characteristics of its region.

    Input:
        1. cluster: cluster object
        2. confDict
        3. bam
    Output:
        1. percMAPQFilter -> boolean: True if the cluster pass the filter, False if it doesn't
        2. percSMSReadsFilter -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    ## Set region coordinates
    if cluster.beg == cluster.end:
        if cluster.beg > 100:
            binBeg = cluster.beg - 100
        else:
            binBeg = cluster.beg
        binEnd = cluster.beg + 100
    else:
        if cluster.beg > 50:
            binBeg = cluster.beg - 50
        else:
            binBeg = cluster.beg
        binEnd = cluster.end + 50

    ref = cluster.ref

    ## Extract filter parameters from config dict
    minReadsRegionMQ = confDict['minReadsRegionMQ']
    maxRegionlowMQ = confDict['maxRegionlowMQ']
    maxRegionSMS = confDict['maxRegionSMS']

    # Set counts to 0
    lowMAPQ = 0
    SMSReads = 0
    nbReads = 0

    ## Open BAM file for reading
    bamFile = pysam.AlignmentFile(bam, "rb")

    ## Extract alignments
    iterator = bamFile.fetch(ref, binBeg, binEnd)

    for alignmentObj in iterator:
        
        if alignmentObj.cigartuples != None:
        
            # Check if aligment pass minimum mapq for reads within the cluster region
            passMAPQ = areaMAPQ(alignmentObj, minReadsRegionMQ)

            # If it doesnt pass, add 1 to the counts of low mapping quality reads within the cluster region
            if passMAPQ == False:
                lowMAPQ += 1

            # Check if aligment is mapped this way: Soft Match Soft (SMS)
            SMSRead = areaSMS(alignmentObj)
            
            # If it is mapped SMS, add 1 to the counts of SMS reads within the cluster region
            if SMSRead == True:
                SMSReads += 1
                
            # Count total number of reads in the region
            nbReads += 1
    
    ## Calculate percentages
    percMAPQ = stats.fraction(lowMAPQ, nbReads)
    percSMSReads = stats.fraction(SMSReads, nbReads)
    
    ## Fill metacluster attrib
    if hasattr(cluster, 'areaMAPQ'):
        cluster.areaMAPQ = percMAPQ
        cluster.areaSMS = percSMSReads

    ## If the percentage of low MQ reads is lower than the threshold pass the filter.
    if percMAPQ != None:
        if percMAPQ < float(maxRegionlowMQ):
            percMAPQFilter = True
        else:
            percMAPQFilter = False
    else:
        percMAPQFilter = False

    ## If the percentage of SMS reads is lower than the threshold pass the filter.
    if percSMSReads != None:
        if percSMSReads < float(maxRegionSMS):
            percSMSReadsFilter = True
        else:
            percSMSReadsFilter = False
    else:
        percSMSReadsFilter = False
    
    return percMAPQFilter, percSMSReadsFilter

def areaMAPQ(alignmentObj, minReadsRegionMQ):
    '''
    Check if the MAPQ of a read pass the minReadsRegionMQ threshold

    Input:
        1. alignmentObj
        2. minReadsRegionMQ
    Output:
        1. percMAPQFilter -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    MAPQ = int(alignmentObj.mapping_quality)

    if MAPQ > int(minReadsRegionMQ):
        passMAPQ = True
    else:
        passMAPQ = False

    return passMAPQ

def areaSMS(alignmentObj):
    '''
    Check if aligment is mapped this way: Soft Match Soft (SMS)

    Input:
        1. alignmentObj
        2. maxRegionSMS
    Output:
        1. percSMSReadsFilter -> boolean: True if the cluster pass the filter, False if it doesnt
    '''

    SMSRead = False

    # Select first and last operation from cigar to search for clipping
    firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
    lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]
    ## Clipping >= X bp at the left
    #  Note: soft (Operation=4) or hard clipped (Operation=5)     
    if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):
        SMSRead = True
        
    #if ((lastOperation == 4) or (lastOperation == 5)) and ((firstOperation != 4) and (firstOperation != 5)):
        #SMSRead = True

    return SMSRead

def filter_highDup_clusters(cluster, maxDupPerc):
    '''
    Filter out those clusters formed by more than a percentage of duplicates
    
    Input:
        1. cluster: list of discordant clusters formed by DISCORDANT events
        2. maxDupPerc: Maximun % of duplicates allowed (not included)
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    dupPerc = cluster.dupPercentage()
        
    if dupPerc < maxDupPerc:
            
        PASS = True

    else:
        PASS = False
                
    return PASS

def filter_clusterRange_discordant(cluster):
    '''
    Filter out those discordant clusters in which all reads are piled up.
    This filter is only applied to clusters formed by more than a discordant alignment
    
    ----------****>                       ---------******>
       -------*******>                    ---------******>
     ---------*****>                      ---------******>
         -----*********>                  ---------******>
    |         |                          |         |   
    beg      end                         beg      end
    ----------       clusterRange         ----------
         -----      min(alignRanges)      ----------
       True             PASS                 False      
    
    Input:
        1. cluster: cluster formed by DISCORDANT events
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    PASS = True
        
    # if there is more than a discordant alignment
    if len(cluster.events) > 1:
               
        # define cluster range
        clusterRange = cluster.end - cluster.beg
        
        # define minimum alignment range of all reads supporting the cluster
        readRanges = []
        
        for event in cluster.events:
            beg, end = event.readCoordinates()
            readRange = abs(abs(end) - abs(beg))
            readRanges.append(readRange)
        
        alignRange = min(readRanges)
        
        # if the cluster range is smaller or equal to the minimum alignment range
        if (clusterRange <= alignRange):
            
            # discard the cluster
            PASS = False
    
    return PASS


def filter_clusterRange_reciprocalMeta(metacluster, maxPercOverlap):
    '''
    Filter metacluster reciprocal clusters if plus clusters and minus clusters overlap more than buffer
    Filter out:
         ------->
    <-------
     ------->
      <-------
             <-------
   end         beg
   
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    PASS = True
    
    # if reciprocal cluster
    if metacluster.orientation == "RECIPROCAL":
        
        # collect beg and end coordinates depending on cluster orientation. Store them in numpy arrays
        plusEnds = np.array([event.beg for event in metacluster.events if event.orientation == "PLUS" and event.type == "DISCORDANT"])
        minusEnds = np.array([event.beg for event in metacluster.events if event.orientation == "MINUS" and event.type == "DISCORDANT"])
        
        # TO DO: Review how this filter works. plusEnds and minusEnds of SA
        # two-sample Kolmogorov-Smirnov test
        p = ks_2samp(plusEnds, minusEnds).pvalue
        
        # fill metacluster attrib
        metacluster.metaRange = p
        
        if p > 0.05:
            PASS = False
        
    return PASS

def filter_metacluster_subfamilyAnnot(metacluster, rtFamilies, rtSubtypes, minDist):
    '''
    Filter metacluster if located at less than minDist in specific ME subtypes
    
    Input:
    1. metacluster
    2. family: Annotation family. Ex: 'L1'
    3. subtype: Annotation subfamily. Ex: 'L1PA'
    4. minDist: minimum distance from metacluster to annotated repeat. Ex: 150
    
    Output: 
    1. PASS: Boolean indicating whether it should be filtered out or not
    '''
    PASS = True
    
    # if metacluster is in a repeat and its identity is in rtFamilies
    if metacluster.repeatAnnot and metacluster.identity in rtFamilies:
        
        # get annotated repeats families
        repeats = metacluster.repeatAnnot
        families = [repeat['family'] for repeat in repeats]
        subfamilies = [repeat['subfamily'] for repeat in repeats] 
        distances = [repeat['distance'] for repeat in repeats]
        
        # for each repeat
        for idx, family in enumerate(families):
            
            for idx2, rtFamily in enumerate(rtFamilies):
                
                if (family == rtFamily) and (rtSubtypes[idx2] in subfamilies[idx]) and (distances[idx] <= minDist):
                    
                    PASS = False
                    break
        
    return(PASS)

def check_matchRepeats(metacluster, minDist):
    '''
    Check if metacluster is located at less than minDist in specific ME subtypes
    
    Input:
    1. metacluster
    4. minDist: minimum distance from metacluster to annotated repeat. Ex: 150
    
    Output: 
    1. metacluster.matchRepeat and metacluster.miliDiv are filled
    '''
    
    # if metacluster is in a repeat
    if metacluster.repeatAnnot:
        
        # get annotated repeats families
        repeats = metacluster.repeatAnnot
        families = [repeat['family'] for repeat in repeats]
        milliDivs = [repeat['milliDiv'] for repeat in repeats] 
        distances = [repeat['distance'] for repeat in repeats]
        
        # for each repeat
        for idx, family in enumerate(families):
            
            # if the same family as cluster identity
            if family in metacluster.identity:
                
                # if near enough
                if distances[idx] < minDist:
                    
                    metacluster.matchRepeat = True
                    
                    # from all repeats, find the min millidivergence
                    if int(milliDivs[idx]) < metacluster.miliDiv:
                        metacluster.miliDiv = int(milliDivs[idx])               


# def filter_percSVs_inNormal(metacluster, maxPercSVs, normalBam, confDict):
#     '''
#     Filter metacluster if region in normal has a percentage of discordants greater than maxPercDisc
    
#     Input:
#         1. metacluster
#         2. maxPercSVs: maximum percentage of discordants in region 
#         3. normalBam
#         4. confDict
    
#     Output:
#         1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
#     '''
#     PASS = True
    
#     # define beg and end coordinates
#     beg = metacluster.bkpA if metacluster.bkpA is not None else metacluster.beg
#     end = metacluster.bkpB if metacluster.bkpB is not None else metacluster.end
#     buffer = 50
    
#     # count nb of reads pointing to SVs
#     confDict['minMAPQ'] = 0
#     confDict['targetEvents'] = ['DISCORDANT', 'CLIPPING']
#     confDict['readFilters'] = None
#     confDict['minCLIPPINGlen'] = 1
#     eventsDict = bamtools.collectSV(metacluster.ref, beg-buffer, end+buffer, normalBam, confDict, None, supplementary = True)
    
#     nbSVs = 0
#     if 'DISCORDANT' in eventsDict.keys():
#         nbSVs += len(eventsDict['DISCORDANT'])
#     if 'LEFT-CLIPPING' in eventsDict.keys():
#         nbSVs += len(eventsDict['LEFT-CLIPPING'])
#     if 'RIGHT-CLIPPING' in eventsDict.keys():
#         nbSVs += len(eventsDict['RIGHT-CLIPPING'])
    
#     # count total number of reads in region 
#     bamFile = pysam.AlignmentFile(normalBam, "rb")
#     nbReads = bamFile.count(metacluster.ref, beg-buffer, end+buffer, read_callback = 'all')
    
#     if nbReads > 0:
#         percSVs = nbSVs/nbReads * 100
        
#         if percSVs > maxPercSVs:
#             PASS = False

#         # fill in metacluster attrib
#         metacluster.svsNormal = percSVs
            
#     else:
#         metacluster.svsNormal = 0
        
#     ## Second part
#     clippings = eventsDict['RIGHT-CLIPPING'] + eventsDict['LEFT-CLIPPING'] # collect clippings in region
#     bkps = [event.beg for event in clippings] # estimate precise bkps
#     counts = Counter(bkps) # count support for each bkp
#     bkpCounts = list(counts.values()) # estimate excess (repetition of bkp positions). If random, it should be excess = 0
#     excess = sum(bkpCounts)-len(bkpCounts) 
#     metacluster.stackClipsNormal = excess # fill in attrib
       
#     return(PASS)




def REF_reads(beg, end, iterator, TSD, minMAPQ):
    '''
    Get reference supporting read ids from bam iterator
    Input:
        1. beg: TSD first bkp
        2. end: TSD second bkp
        3. iterator: Bam file iterator from TSD region
        4. TSD: Boolean indicating if MEI has TSD or not
        5. minMAPQ: MAPQ threshold
    Output:
        1. REF: List of reference supporting read names
    '''
    # Initialize counts
    REF = []

    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue
        
        ## Aligments with MAPQ < threshold
        MAPQ = int(alignmentObj.mapping_quality)
        if MAPQ < minMAPQ:
            continue
        
        # Discard SMS reads (reads with CIGAR #S#M#S)
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation= alignmentObj.cigartuples[-1][0]
        if ((firstOperation == 4) or (firstOperation == 5)) or ((lastOperation == 4) or (lastOperation == 5)):
            continue
        
        ## Select proper pairs
        if not alignmentObj.is_proper_pair:
            continue
        
        # If there is TSD:
        if TSD:
            
            ## Discard fwd reads that start after the start of the TSD. You can not tell whether they are REF or ALT
            if not alignmentObj.is_reverse and alignmentObj.reference_start > beg:
                continue
            
            ## Discard reverse reads that end before the end of the TSD
            if alignmentObj.is_reverse and alignmentObj.reference_end < end:
                continue
        
        ## Append alignment
        REF.append(alignmentObj.query_name)
    
    # Get unique values
    REF = list(set(REF))
    
    return REF


def ALT_reads(beg, end, iterator, TSD):
    '''
    Get variant supporting (ALT) read ids from bam iterator
    Input:
        1. beg: TSD first bkp
        2. end: TSD second bkp
        3. iterator: Bam file iterator from TSD region
    Output:
        1. ALT_FWD: List of ALT read names supporting variant forward end
        2. ALT_RV: List of ALT read names supporting variant reverse end
    '''
    # Initialize counts
    ALT_RV = []
    ALT_FWD = []
    buffer = 2 # nt

    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue
        
        # Discard SMS reads (reads with CIGAR #S#M#S)
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation= alignmentObj.cigartuples[-1][0]      
        if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):
            continue
        
        ## Sort split reads in FWD and RV according to where they have the clipping
        clipping = (firstOperation == 4 or firstOperation == 5 or lastOperation == 4 or lastOperation == 5)
        
        ## Discard proper pairs without clipping
        if alignmentObj.is_proper_pair and not clipping:
            continue
        
        # if clipping 
        if clipping:
            
            # if left clipping
            if (firstOperation == 4 or firstOperation == 5):
                
                # check it's in interval
                if beg-buffer <= alignmentObj.reference_start <= end+buffer:
                    ALT_RV.append(alignmentObj.query_name)
                
            # if right clipping
            elif (lastOperation == 4 or lastOperation == 5):
                
                # check it's in interval
                if beg-buffer <= alignmentObj.reference_end <= end+buffer:
                    ALT_FWD.append(alignmentObj.query_name)

        # if discordants
        else:
            
            if TSD:
                
                # if forward read and read end <= TSD end
                # forward discordant: -------->| (end)
                if not alignmentObj.is_reverse and alignmentObj.reference_end <= end:
                    ALT_FWD.append(alignmentObj.query_name)
                    
                # if reverse read and read start >= TSD start
                # reverse discordant: (start)| <-------- 
                elif alignmentObj.is_reverse and alignmentObj.reference_start >= beg:
                    ALT_RV.append(alignmentObj.query_name)
            
            else:
                
                # if forward read and read end <= TSD end
                # forward discordant: -------->| (end)
                if not alignmentObj.is_reverse and alignmentObj.reference_end <= beg:
                    ALT_FWD.append(alignmentObj.query_name)
                    
                # if reverse read and read start >= TSD start
                # reverse discordant: (start)| <-------- 
                elif alignmentObj.is_reverse and alignmentObj.reference_start >= end:
                    ALT_RV.append(alignmentObj.query_name)                
                
    # Get unique values
    ALT_FWD = list(set(ALT_FWD))
    ALT_RV = list(set(ALT_RV))
    
    return ALT_FWD, ALT_RV


def filter_normal_VAFs(normalBam, metacluster, minREF = 5, maxVAF = 0.05, minMAPQ = 20):
    '''
    Estimate REF, ALT_FWD and ALT_RV counts from a given list of regions and write them to a file
    
    Input:
    1. bam: tumour bam file
    2. targetBins: bed file with insertion coordinated (exact TSD coordinates needed)
    3. outDir: output directory 
    4. minMAPQ: min mapq threshold for REF reads
    
    Output:
    No output but a file outDir + '/illumina_vafs.tsv'
    '''
    
    PASS = True
    
    ## Define bkps
    ref = metacluster.ref
    bkpA, bkpB = metacluster.bkpA, metacluster.bkpB
    TSD = metacluster.TSD
    
    # if no bkp identified, discard cluster
    if not bkpA and not bkpB: 
        PASS = False
        return(PASS)

    # if not both bkps haven't been defined, fill in the other
    if bkpA and not bkpB: bkpB = bkpA
    if bkpB and not bkpA: bkpA = bkpB
    
    # if both bkps are equal, add 1 to be able to parse the window
    if bkpA == bkpB: bkpB = bkpB+1
    
    ## Open normal BAM file for reading
    bamFile = pysam.AlignmentFile(normalBam, "rb")
    
    ## Get REF read ids
    iterator = bamFile.fetch(ref, bkpA, bkpB)
    REF = REF_reads(bkpA, bkpB, iterator, TSD, minMAPQ)
    
    ## Get ALT read ids
    if TSD:
        iterator = bamFile.fetch(ref, bkpA, bkpB)
        
    else:
        iterator = bamFile.fetch(ref, bkpA-2, bkpB+2)
        
    ALT_FWD, ALT_RV = ALT_reads(bkpA, bkpB, iterator, TSD)
    
    # Get counts
    cREF = len(REF)
    cALT_FWD = len(ALT_FWD)
    cALT_RV = len(ALT_RV)
    
    # if there is no more than minREF reads in normal
    if cREF < minREF:
        PASS = False
    
    # if ALT_FWD > maxVAF, discard
    elif cALT_FWD / (cREF + cALT_FWD) > maxVAF:
        PASS = False
    
    # if ALT_RV > maxVAF, discard
    elif cALT_RV / (cREF + cALT_RV) > maxVAF:
        PASS = False
    
    return(PASS)


def filter_TDs_chr6_srcElement(metacluster):
    '''
    Filter metacluster if there is a transduction in chr 6 and the source element is located in chr 6 as well.
    It is a source of FPs. Metalusters will only be discarded if no polyA support is found.
    
    Input:
    1. metacluster
    
    Output: 
    1. PASS: Boolean indicating whether it should be filtered out or not
    '''
    PASS = True
    
    # If it's a transduction in chr6
    if metacluster.ref == '6' and metacluster.identity in ['TD1', 'TD2', 'partnered', 'orphan']:
        
        # Extract src_id ref
        src_id = metacluster.src_id.replace('p', 'q')
        src_ref = src_id.split('q')[0]
        
        # If scr_ref is equal to transduction ref and there is no pA support
        if metacluster.ref == src_ref and not metacluster.pA:
            
            PASS = False
        
    return(PASS)

def check_tdSrc_isRef(metacluster):
    '''
    Check metacluster if tranduction and source element are located 
    in the same ref, and not polyA support is found.
    
    Input:
    1. metacluster
    
    Output: 
    1. metacluster.tdSrc_isRef attribute is filled
    '''
    metacluster.tdSrc_isRef = False
    
    # If it's a transduction
    if metacluster.identity in ['TD1', 'TD2', 'partnered', 'orphan']:
        
        # Extract src_id ref
        src_id = metacluster.src_id.replace('p', 'q')
        src_ref = src_id.split('q')[0]
        
        # If scr_ref is equal to transduction ref and there is no pA support
        if metacluster.ref == src_ref and not metacluster.pA:
            
            metacluster.tdSrc_isRef = True
            
def filter_regularSV(metacluster):
    '''
    Filter out those discordant clusters that point to a DEL or TRA with no ME bridge. 
    If all mates map to the same region and has no transduction identity, discard cluster.
    
    Input:
        1. metacluster: cluster formed by DISCORDANT events
    
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    
    PASS = True
        
    # if there is a single cluster pointing to a Rg and it does not involve a transduced sequence
    if metacluster.ins_type == 'RG' and metacluster.src_id == None:
        
        # select discordant events. Discard supp alignments
        discordants = [event for event in metacluster.events if event.type == 'DISCORDANT']
        
        # define mates refs
        refs = [event.mateRef for event in discordants]
        
        refs_max = Counter(refs).most_common(1)[0]
        
        # if most mates map to the same chr
        if refs_max[1]/len(refs) >= 0.70:
            
            # define mates coordinates
            starts = [event.mateStart for event in discordants if event.mateRef == refs_max[0]]
            beg, end = min(starts), max(starts)
            dist = abs(beg-end)
            
            # fill in metacluster attrib
            metacluster.regularSV = dist
            
            # if there is no range greater than 2 kb
            if dist < 2000:
                
                PASS = False
    
    return PASS


def filter_germline_MEI(events, MEIDb, buffer):
    '''
    For each input event assess if overlaps with an already known germline MEI polymorphism

    Input: 
        1. events: list containing input events to be annotated. Events should be objects containing ref, beg and end attributes.
        2. MEIDb: dictionary containing known germline MEI organized per chromosome (keys) into genomic bin databases (values)
        3. buffer: number of base pairs to extend begin and end coordinates for each event prior assessing overlap

    Output:
        List of events not overlapping any known germline MEI polymorphism
    '''
    
    filteredEvents = []
    
    ## Assess for each input event if it overlaps with an germline MEI repeat
    for event in events:

        # Skip event if family not available or no MEI on the same reference
        if not event.identity or (event.ref not in MEIDb):
            continue

        ### Select bin database for the corresponding reference 
        binDb = MEIDb[event.ref]

        ### Retrieve overlapping known germline MEI if any
        overlaps = binDb.collect_interval(event.beg - buffer, event.end + buffer, [event.identity]) 

        ### If known germline MEI overlapping the event, discard
        if not overlaps:
            filteredEvents.append(event)
    
    return(filteredEvents)


def filter_clusters_src_regions(cluster, ranges):
    '''
    Filter out clusters in srcElements boundaries

    Input:
        1. cluster: cluster cluster object
        2. ranges: Dictionary with reference ids as keys and the list of ranges on each reference as values
        
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    ## Do not filter out cluster if no input range on that particular reference 
    if cluster.ref not in ranges:
        PASS = True
        return PASS
        
    ## Assess overlap with provided regions
    PASS = True

    for interval in ranges[cluster.ref]:
        rangeBeg, rangeEnd = interval

        # Assess overlap
        overlap, overlapLen = gRanges.overlap(cluster.beg, cluster.end, rangeBeg, rangeEnd)

        if overlap:
            PASS = False

    return PASS


def filter_minReads_diffBeg(metacluster, minClusterSize, minReads):
    '''
    Filter metacluster if plus or minus clusters are stacked reads sharing the same coordinates (Pseudoduplicates)
    and they don't reach the minimun number of reads 
    
    ----------****>                       ---------******>
       -------*******>                    ---------******>
     ---------*****>                      ---------******>
          True                                 False     
   
    Output:
        1. PASS -> boolean: True if the cluster pass the filter, False if it doesn't
    '''
    PASS = True
    
    ## A. Count distinct beg coordinates depending on cluster orientation
    diff_plusBegs = len(set([event.beg for event in metacluster.events if (event.orientation == "PLUS" and event.sample != 'NORMAL')]))
    diff_minusBegs = len(set([event.end for event in metacluster.events if (event.orientation == "MINUS" and event.sample != 'NORMAL')]))
    nb_total = diff_plusBegs + diff_minusBegs
    
    # if reciprocal
    if metacluster.orientation == "RECIPROCAL":
        # if there is less distinct beg coordinates than minReads, filter out metacluster
        if (diff_plusBegs <= minClusterSize) or (diff_minusBegs <= minClusterSize) or (nb_total <= minReads):
            PASS = False
    # if RG
    else:
        if ((diff_plusBegs <= minClusterSize) and (diff_minusBegs <= minClusterSize)) or (nb_total <= minReads):
            PASS = False
    
    # fill in attributes
    metacluster.diff_plusBegs = diff_plusBegs
    metacluster.diff_minusBegs = diff_minusBegs
    
    ## B. Calculate excess of repeated beggings
    readNames_plus, readNames_minus = [], []
    plusBegs_counts, minusBegs_counts = [], []
    
    # filter repeated events, and events that belong to normal sample
    for event in metacluster.events:
        
        if (event.orientation == "PLUS" and event.sample != 'NORMAL') and event.readName not in readNames_plus:
            plusBegs_counts.append(event.beg)
            readNames_plus.append(event.readName)
        
        if (event.orientation == "MINUS" and event.sample != 'NORMAL') and event.readName not in readNames_minus:
            minusBegs_counts.append(event.end)
            readNames_minus.append(event.readName)
    
    # create a dictionary of counts        
    plusBegs_counts = Counter(plusBegs_counts)
    minusBegs_counts = Counter(minusBegs_counts)
    
    # # extract list of counts
    # plusBegs_counts = list(plusBegs_counts.values())
    # minusBegs_counts = list(minusBegs_counts.values())
    
    # # estimate excess of repeated positions
    # excessPlus = sum(plusBegs_counts)-len(plusBegs_counts)
    # excessMinus = sum(minusBegs_counts)-len(minusBegs_counts)
    
    # Estimate maximum times a coordinate repeats and fill metacluster attributes with it
    if plusBegs_counts: metacluster.beg_excessPlus = plusBegs_counts.most_common()[0][1]
    if minusBegs_counts: metacluster.beg_excessMinus = minusBegs_counts.most_common()[0][1]
    
    return PASS
