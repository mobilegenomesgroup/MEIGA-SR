'''
Module 'clustering' - Contains functions for clustering sets of objects based on different criteria
'''

## DEPENDENCIES ##
# External
import operator

# Internal
from GAPI import gRanges
from GAPI import events as events_GAPI 

from modules import clusters as clusters_MS


def cluster_DISCORDANTS(events, confDict):
    '''
    Cluster DISCORDANT events into DISCORDANT clusters
    
    Input:
        1. events: List of discordant events to be clusterized
        2. confDict:  
            * minClusterSize: minimum number of reads composing a root cluster
            * equalOrientBuffer: Maximum distance bewteen two adjacent discordants

    Output:
        1. clusters: List of DISCORDANT clusters formed
    ''' 
    
    clusters = reciprocal_overlap_clustering(events, 1, confDict['minClusterSize'], 'DISCORDANT', confDict['equalOrientBuffer'])    
            
    return clusters


def cluster_DISCORDANTS_byIdentity(discordantsIdentity, discordantsPolyA, confDict, orientations = ['PLUS', 'MINUS']):
    '''
    Cluster DISCORDANT events into clusters by identity. Events pointing to pA can cluster with any other identity. 
    Orientation is taken into account to cluster the events
    
    Input:
    1. discordantsIdentity: dictionary containing lists of discordant events organized taking into account their orientation and 
                            if the mate aligns in a given list of annotations. This info is encoded in the dictionary keys as follows. 
                            Keys are composed by 3 elements separated by '_':    
                                - Orientation: read orientation (PLUS or MINUS)
                                - Event type: DISCORDANT   
                                - Type: identity type. It can be retrotransposon family (L1, Alu, SVA), source element and gene name
    2. discordantsPolyA: dictionary that follows the structure of discordantsIdentity, but the annotation DB used to annotate the events
                         is related to Simple_repeats. The keys can hence be ['PLUS_DISCORDANT_Simple_repeat', 'MINUS_DISCORDANT_Simple_repeat'].
    3. confDict:
            * minClusterSize: minimum number of reads composing a root cluster
            * equalOrientBuffer: Maximum distance bewteen two adjacent discordants
    4. orientations: List of possible orientations
    
    Output:
    1. discordantClusters: List of discordant clusters formed
    '''
    
    discordantClusters = []

    for orientation in orientations:
        
        # split dictionaries by orientation 
        discordantsPolyA_subset = discordantsPolyA.get(orientation + '_DISCORDANT_Simple_repeat')
        discordantsIdentity_subset = {key: value for key, value in discordantsIdentity.items() if orientation in key}
        
        # for each identity
        for eventType in discordantsIdentity_subset.keys():
            
            # select discordants pointing to eventType plus those pointing to a 'Simple_repeat'
            discordantEvents = discordantsIdentity_subset[eventType] + discordantsPolyA_subset if discordantsPolyA_subset else discordantsIdentity_subset[eventType]
            
            # create discordant clusters
            discordants = cluster_DISCORDANTS(discordantEvents, confDict)
            discordantClusters += discordants

            # delete discordant events pointing to 'Simple_repeats' that have been clustered
            if discordants and discordantsPolyA_subset:
                
                eventsClusterized = [event for cluster in discordants for event in cluster.events]                                
                [discordantsPolyA_subset.remove(event) for event in discordantsPolyA_subset if event in eventsClusterized]
                discordants = None
        
        # create discordant clusters of the reads pointing to single repeat that haven't been clusterized
        if discordantsPolyA_subset:
            discordants = cluster_DISCORDANTS(discordantsPolyA_subset, confDict)
            discordantClusters += discordants          
      
    return discordantClusters


def create_metaclusters(clusters, buffer = 200):
    '''    
    Group SV cluster events into metaclusters

    Input:
        1. clusters: a list of clusters
        2. buffer: buffer to extend cluster coordinates

    Output:
        1. metaclusters: list containing newly created metaclusters
    '''
    metaclusters = reciprocal_overlap_clustering(clusters, 1, 1, 'META', buffer)

    return metaclusters


def reciprocal_overlap_clustering(allEvents, minPercOverlap, minClusterSize, clusterType, buffer):
    '''
    Group events/clusters based on reciprocal overlap into clusters/metaclusters

    Input:
        1. allEvents: List of events to be clusterized
        2. minClusterSize: minimum number of events clustering together for creating a cluster
        3. clusterType: type of clusters to be created (If "META", metaclustering will be performed)
        4. buffer: number of nucleotides used to extend cluster begin and end coordinates prior evaluating reciprocal overlap 

    Output:
        1. clustersList: list of created clusters/metaclusters
    ''' 
    
    ## Initialize variables
    clustersDict = {}
    
    ## Organize events by chromosome
    eventsDict = events_GAPI.events2Dict(allEvents)
    
    ## For each chromosome
    for ref, events in eventsDict.items():
        
        ### 1. Cluster events based on reciprocal overlap 
        ## Initialize variables
        idxLast = -1
           
        ## Sort events
        events.sort(key=lambda event:(event.ref, event.beg))
    
        ## For each event A
        for idx, eventA in enumerate(events):
            
            # If the event has been tested to overlap A in the B events loop, skip
            if idx <= idxLast:
                continue
            
            ## 1.1. Generate a list containing clusters or events overlapping A, including A: 
            eventsOverlapA = [idx]
            
            begA = eventA.beg - buffer
            endA = eventA.end + buffer
            
            # Test if the subsequent events (B events) overlap A
            for idxB in range(idx + 1, len(events)):
                
                eventB = events[idxB]
                begB = eventB.beg - buffer
                endB = eventB.end + buffer
                
                overlap, overlapLen = gRanges.rcplOverlap(begA, endA, begB, endB, minPercOverlap)
                
                # A) Event B overlaps A
                if overlap:
                    
                    eventsOverlapA.append(idxB)
                    # update A end coordinates
                    endA = endB if endB > endA else endA
                    # get last event index
                    idxLast = idxB

                # B) Event B NOT overlap A. As they are sorted, B+1 won't overlap, break the loop for eventB in events[idx + 1:]
                else:
                    break

            ## Clusterize all events that have been seen to overlap A
            # if clusterizing cluster into metaclusters
            if clusterType == "META":
                
                # Select events to cluster from idxs
                clusters2merge = [events[idx] for idx in eventsOverlapA]
                
                ## Create merged cluster                    
                mergedCluster = merge_clusters(clusters2merge, clusterType)
                    
                ## Add merged cluster to the clusters dictionary
                clustersDict[mergedCluster.id] = mergedCluster
            
            # if clusterinzing events into clusters
            else:
                
                # A + overlapping events are >= minClusterSize:
                if len(eventsOverlapA) >= minClusterSize:
                    
                    # Select events to cluster from idxs
                    events2Cluster = list(operator.itemgetter(*eventsOverlapA)(events))
                    
                    # Create cluster
                    cluster = clusters_MS.create_cluster(events2Cluster, clusterType)

                    # Add cluster to the dict of clusters
                    clustersDict[cluster.id] = cluster
            
    clustersList = list(clustersDict.values())
    
    return clustersList

def merge_clusters(clusters2merge, clusterType):
    '''
    Merge a set of clusters/metaclusters into a single cluster/metacluster instance

    Input:
        1. clusters2merge: list of clusters/metaclusters that will be merged
        2. clusterType: type of cluster to be formed (DISCORDANT, CLIPPING, META)

    Output:
        1. cluster: merged cluster/metacluster instance
    '''
    # A) Merge metaclusters
    if hasattr(clusters2merge[0], 'rawSubclusters'):
        
        subclusters = []
        for metacluster in clusters2merge:
            subclusters += list(metacluster.rawSubclusters)
            
        mergedCluster = clusters_MS.create_cluster(subclusters, clusterType)
        
        # Update subclusters ids
        for metacluster in clusters2merge:
            
            for clusterNew in metacluster.rawSubclusters:
                clusterNew.clusterId = mergedCluster.id
                
    # B) Merge standard clusters into metaclusters
    elif clusterType == 'META':
        
        mergedCluster = clusters_MS.create_cluster(clusters2merge, clusterType)

    # C) Merge standard clusters
    else:
        events = [cluster.events for cluster in clusters2merge]
        mergedCluster = clusters_MS.create_cluster(events, clusterType)

    return mergedCluster
