# MEIGA-SR

`MEIGA-SR` is a method for inferring the VAF of somatic retrotransposition events using short-read sequencing data (e.g., Illumina paired-end data) developed within the MEIGA framework.

- [Overview](#overview)
- [System Requirements](#system-requirements)
- [Installation Guide](#installation-guide)
- [Documentation](#documentation)
- [License](#license)
- [Issues](https://gitlab.com/mobilegenomesgroup/MEIGA-SR/-/issues)



# Overview
While tools like [SVclone](https://github.com/mcmero/SVclone) are available for inferring allele frequencies of classical SVs in short-read sequencing data, they encounter limitations when dealing with retrotransposition events. Unlike classical SVs, retrotransposon insertions do not exhibit the characteristic formation of two well-defined clusters of discordant pairs—one on each side of the SV junction. Instead, these events are supported by discordant pairs where one read of the pair accurately clusters near the insertion breakpoint, while their mates disperse throughout the genome, mapping to multiple locations where a retrotransposon of the same class is present. 

Here, we leveraged the coding developed for MEIGA and created a targeted genotyping strategy, named MEIGA-SR, specifically designed to analyse short-read sequencing data and accurately estimate the VAFs of previously identified somatic retrotransposition events. The standard input for this approach is Illumina paired-end sequencing data using 150 bp reads.



# System Requirements

`MEIGA-SR` can potentially be installed on any *Linux* cluster. The package has been tested on the following system:
+ Linux: Rocky Linux 8.4

The genotyper mode of `MEIGA-SR` is not particularly demanding in terms of time or memory resources. Running `MEIGA-SR` for 1000 retrotransposition insertions (1 core, 10 GB) takes less than 30 minutes."



# Installation Guide

## Meet MEIGA dependencies

`MEIGA-SR` relies on the following dependencies:
1. samtools (version 1.19), which can be downloaded from https://github.com/samtools.
2. bedtools (version 2.31.0), which can be downloaded from https://github.com/arq5x/bedtools2.
3. bwa (version 0.7.17), which can be downloaded from https://github.com/lh3/bwa.
4. ncbi-blast (version 2.14.1+), which can be downloaded from https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+.
5. python (version 3.7.8+). Python packages included in file **requirements.txt** can be installed using a pip based environment as explained below.


## Download MEIGA-SR
```bash
git clone https://gitlab.com/mobilegenomesgroup/MEIGA-SR.git
cp -r MEIGA-SR/GAPI MEIGA-SR/scripts/
```

## Set up MEIGA-SR environment
```bash
python -m venv MEIGA-SR/.venv
source MEIGA-SR/.venv/bin/activate
pip install --upgrade pip
pip install wheel
pip install -r MEIGA-SR/requirements.txt --no-cache-dir
```


# Documentation

## Test installation
A test dataset has been included in MEIGA-SR and can be found at [MEIGA-SR Demo](https://gitlab.com/mobilegenomesgroup/MEIGA-SR/-/tree/master/demo). This comprises a tumour BAM file containing 23 different somatic retrotranspositions of various types, including 10 L1 solo insertions, 4 L1 orphan insertions, 3 L1 partnered insertions and 5 L1 solo deletions. All have been simulated at 0.5 VAF. You can easily launch the demo by executing the `launch_meiga-sr.sh` file.


## Run MEIGA-SR for RT insertions

```bash
# A) Load environment
source MEIGA-SR/.venv/bin/activate
export PYTHONHASHSEED=0

# B) Set input
bam=MEIGA-SR/demo/simulationRegions.sorted.bam
calls_ins=MEIGA-SR/demo/targetRegions_INS.bed
outDir=MEIGA-SR/demo/output
mkdir -p $outDir

# C) Launch MEIGA-SR for RT insertions
python MEIGA-SR/scripts/MEIs_vaf.py $bam $calls_ins $outDir

# D) Launch MEIGA-SR for RT-mediated rearrangements
python MEIGA-SR/scripts/RG_vaf.py $bam $calls_rg $outDir
```

`MEIGA-SR` outputs `illumina_vafs.tsv` file. This file contains all somatic retrotransposon insertions that could be genotyped. The columns include:

+ Genomic coordinates (e.g., ref, beg, end, bkpA, bkpB)
+ TSD: Whether a target site duplication has been identified
+ MAPQ: Region MAPQ
+ VAF: Estimated Variant Allele Frequency
+ REF: Number of reference reads identified
+ ALT: Number of alternate reads identified
+ ALT_FWD: Number of alternate reads in forward orientation
+ ALT_RV: Number of alternate reads in reverse orientation
+ Read identifiers (e.g., REF_ids, ALT_FWD_ids, ALT_RV_ids)

The same information is reported for a matched normal BAM file if provided (--normalBam).

An example of these outputs can be found at [MEIGA Demo](https://gitlab.com/mobilegenomesgroup/MEIGA-SR/-/tree/master/demo). 



## Run MEIGA-SR for RT-mediated rearrangements

```bash
# A) Load environment
source MEIGA-SR/.venv/bin/activate
export PYTHONHASHSEED=0

# B) Set input
bam=MEIGA-SR/demo/simulationRegions.sorted.bam
calls_rg=MEIGA-SR/demo/targetRegions_RG.bed
outDir=MEIGA-SR/demo/output
mkdir -p $outDir

# C) Launch MEIGA-SR for RT-mediated rearrangements
python MEIGA-SR/scripts/RG_vaf.py $bam $calls_rg $outDir
```

`MEIGA-SR` outputs `illumina_vafs_RG.tsv` file for reporting RT-mediated rearrangements. This file contains all somatic retrotransposon rearrangements that could be genotyped. The columns include:

+ Genomic coordinates in BEDPE format (e.g., chrom1, start1, end1, chrom2, start2, end2, name, strand1, strand2)

Additionally, for each breakend, the following columns are included:
+ mVAF: Estimated Variant Allele Frequency
+ MAPQ: Region MAPQ
+ VAF: Estimated Variant Allele Frequency
+ REF: Number of reference reads identified
+ ALT: Number of alternate reads identified
+ ALT_FWD: Number of alternate reads in forward orientation
+ ALT_RV: Number of alternate reads in reverse orientation
+ Read identifiers (e.g., REF_ids, ALT_FWD_ids, ALT_RV_ids)

The same information is reported for a matched normal BAM file if provided (--normalBam).

An example of these outputs can be found at [MEIGA Demo](https://gitlab.com/mobilegenomesgroup/MEIGA-SR/-/tree/master/demo). 



# License

This project is covered under the **GNU GENERAL PUBLIC LICENSE**.
