#!/bin/python

'''
Script to generate a bed file containing the regions that can be transduced.
It takes a bed file indicating the coordinates of the source elements, 
a list with transduction ends, and a path to output the resulting bed file.

Buffer used 10 kb (regionSize)
'''

##  DEPENDENCIES ##
# External
import argparse
import sys
sys.path.append('../GAPI')

# Internal
from GAPI import databases


## 1. Define parser
######################
parser = argparse.ArgumentParser()
parser.add_argument('srcBed', help='Source element coordinates')
parser.add_argument('tdEnds', help='Tranduction ends. Ex: 5,3')
parser.add_argument('outDir', help='Output directory')


## 2. Parse user's input
##########################
args = parser.parse_args()
srcBed = args.srcBed
tdEnds = [tdEnd.strip() for tdEnd in args.tdEnds.split(',')]
outDir = args.outDir


## 3. Define fucntions
##########################
# A. Create bed
# buffer equals -75 to avoid the end of the src 
regionSize = 10000
transducedPath = databases.create_transduced_bed(srcBed, tdEnds, regionSize, -75, outDir)

# B. Echo output
print("New bed with tdRegions: ")
print(transducedPath)