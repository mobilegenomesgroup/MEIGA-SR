#!/bin/python

'''
Script to estimate VAFs of ME-RGs. Counts REF and ALT in all entries of a bed file, given a bam file. RG strand should be indicated on bed
Output is written in outDir + '/illumina_vafs.tsv'

'''

##  DEPENDENCIES ##
# External
import argparse

from numpy.testing._private.utils import build_err_msg
import pysam
import numpy as np
import sys
from collections import Counter
sys.path.append('../GAPI')

# Internal
from GAPI import formats
from GAPI import gRanges
from GAPI import unix


## 1. Define parser
######################
parser = argparse.ArgumentParser()
parser.add_argument('bam', help='Bam file')
parser.add_argument('targetBins', help='Bed file with Trafic TSD bkps')
parser.add_argument('outDir', help='Output directory')
parser.add_argument('--normalBam', default=None, dest='normalBam', help='Matched normal bam file. If provided MEIGA will run in PAIRED mode')


## 2. Parse user's input
##########################
args = parser.parse_args()
targetBins = args.targetBins
bam = args.bam
normalBam = args.normalBam
outDir = args.outDir

unix.mkdir(outDir) # create output dir if doesn't exist

## 3. Define functions
##########################
def normal_noise(ref, beg, end, normalBam, buffer = 100):
    '''
    Estimate noisy reads ratio in region
    Input:
        1. ref
        2. beg
        3. end
        4. normalBam
        5. buffer  

    Output:
        1. noise: Ratio of noisy alignments / total alignments. SMS, clippings and discordant events are considered noisy
    '''
    # Initialize counts
    noisy_reads = 0
    total_reads = 0
    noise = None
    
    ## Open BAM file for reading in region
    bamFile = pysam.AlignmentFile(normalBam, "rb")
    iterator = bamFile.fetch(ref, beg-buffer, end+buffer)
    
    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue    

        ## Count total reads
        total_reads += 1

        ## Count SMS reads and clippings
        firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
        lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]
        
        if (firstOperation == 4) or (firstOperation == 5) or (lastOperation == 4) or (lastOperation == 5):
            if firstOperationLen >= 3 or lastOperationLen >= 3:
                noisy_reads += 1
        
        ## Count not proper pairs
        elif not alignmentObj.is_proper_pair:
            noisy_reads += 1

    ## Estimate noise percentage:
    if total_reads > 0:
        noise = noisy_reads/total_reads

    return noise


def determine_bkps(ref, beg, end, strand, bam, buffer = 30):
    '''
    Search for bkp in a given region in bam
    Input:
        1. ref
        2. beg
        3. end
        4. strand
        5. bam
        6. buffer  

    Output:
        1. ref
        2. bkp
    '''
    # Initialize counts
    left_clipping = []
    right_clipping = []
    plus_bkp = None
    minus_bkp = None
    
    ## Open BAM file for reading in region
    bamFile = pysam.AlignmentFile(bam, "rb")
    iterator = bamFile.fetch(ref, beg-buffer, end+buffer)
    
    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue    

        ## Discard SMS reads (reads with CIGAR #S#M#S)
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation = alignmentObj.cigartuples[-1][0]
        if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):
            continue

        ## Determine clipping
        if (firstOperation == 4) or (firstOperation == 5):
            left_clipping.append(alignmentObj.reference_start)

        elif (lastOperation == 4) or (lastOperation == 5):
            right_clipping.append(alignmentObj.reference_end)

    if right_clipping and strand == "+":        
        bkp = Counter(right_clipping).most_common()[0][0]
    
    elif left_clipping and strand == "-":
        bkp = Counter(left_clipping).most_common()[0][0]
    
    else:
        bkp = None
    
    return ref, bkp

def ref_reads(beg, end, bkp, iterator, strand, minMAPQ, minCLIPPING = 5, minAS = 120):
    '''
    Get reference supporting read ids from bam iterator
    Input:
        1. beg: TSD first bkp
        2. end: TSD second bkp
        3. iterator: Bam file iterator from TSD region
        4. TSD: Boolean indicating if MEI has TSD or not
        5. minMAPQ: MAPQ threshold
    Output:
        1. REF: List of reference supporting read names
    '''
    # Initialize counts
    REF = []
    FAIL = [] # Reads finishing on TSD (You cannot tell whether they are REF or ALT), SMS, low MAPQ... 

    refDict = {}

    # For each read alignment
    for alignmentObj in iterator:
        
        ## If mate already marked as FAIL
        if alignmentObj.query_name in FAIL:
            continue
        
        ## Unmapped or query sequence not available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            FAIL.append(alignmentObj.query_name)
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            FAIL.append(alignmentObj.query_name)
            continue
        
        ## Discard aligments with MAPQ or Alignment Score < threshold 
        MAPQ = int(alignmentObj.mapping_quality)
        AS = int(alignmentObj.get_tag('AS'))
        if MAPQ < minMAPQ or AS < minAS:
            FAIL.append(alignmentObj.query_name)
            continue
        
        # Discard SMS reads (reads with CIGAR #S#M#S) and clippings
        firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
        lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]    
        if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):

            if (firstOperationLen or lastOperationLen) >= minCLIPPING:
                FAIL.append(alignmentObj.query_name)
                continue

        ## Select proper pairs
        if not alignmentObj.is_proper_pair:
            FAIL.append(alignmentObj.query_name)
            continue

        ## Discard fwd reads that start after the bkp. You can not tell whether they are REF or ALT
        # buffer of 1 added since clippings of 1 are not marked as clippings
        #      |     |
        #       <----* # not marked
        if not alignmentObj.is_reverse and alignmentObj.reference_start >= bkp-minCLIPPING:
            FAIL.append(alignmentObj.query_name)
            continue
        
        ## Discard reverse reads that end before the bkp
        if alignmentObj.is_reverse and alignmentObj.reference_end <= bkp+minCLIPPING:
            FAIL.append(alignmentObj.query_name)
            continue

        # if overlap
        boolean, overlapLen = gRanges.overlap(bkp-1, bkp+1, alignmentObj.reference_start, alignmentObj.reference_end)
        
        ## Append alignment
        if boolean:
            REF.append(alignmentObj.query_name)

        ## Collect pairs intervals to check later in the overpass the bkp
        if alignmentObj.is_reverse:
            refDict[alignmentObj.query_name] = [alignmentObj.next_reference_start, alignmentObj.reference_end]
        
        else:
            refDict[alignmentObj.query_name] = [alignmentObj.reference_start, alignmentObj.next_reference_start + 151]

    for k,v in refDict.items():

        boolean, overlapLen = gRanges.overlap(bkp-1, bkp+1, v[0], v[1])

        if boolean:
            REF.append(k)

    # Get unique values
    REF = list(set(REF)-set(FAIL))

    # # Get unique values
    # REF = list(set(REF)-set(FAIL))
    
    return REF

def alt_reads(beg, end, bkp, iterator, strand, minCLIPPING = 5, buffer = 2):
    '''
    Get variant supporting (ALT) read ids from bam iterator
    Input:
        1. beg: TSD first bkp
        2. end: TSD second bkp
        3. iterator: Bam file iterator from TSD region
    Output:
        1. ALT: List of ALT read names supporting variant 
    '''
    # Initialize counts
    ALT = []

    # For each read alignment
    for alignmentObj in iterator:

        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:            
            continue
        
        # Discard SMS reads (reads with CIGAR #S#M#S)
        firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
        lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]    
        if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):

            if (firstOperationLen or lastOperationLen) >= minCLIPPING:
                continue
                
        ## Discard proper pairs without clipping
        clipping = (firstOperation == 4 or firstOperation == 5 or lastOperation == 4 or lastOperation == 5) and ((firstOperationLen or lastOperationLen) >= minCLIPPING)
        if alignmentObj.is_proper_pair and not clipping:
            continue

        # if clipping 
        if clipping:
            
            # if left clipping
            if (firstOperation == 4 or firstOperation == 5) and (firstOperationLen >= minCLIPPING) and (strand == "-"):
                
                # check it's in interval
                if bkp-buffer <= alignmentObj.reference_start <= bkp+buffer:
                    ALT.append(alignmentObj.query_name)
                
            # if right clipping
            elif (lastOperation == 4 or lastOperation == 5) and (lastOperationLen >= minCLIPPING) and (strand == "+"):

                # check it's in interval
                if bkp-buffer <= alignmentObj.reference_end <= bkp+buffer:
                    ALT.append(alignmentObj.query_name)
        
        # if discordant
        else:
            
            ## Select forward or reverse reads depending on RG strand
            if (strand == "+") and alignmentObj.is_reverse:
                continue

            if (strand == "-") and not alignmentObj.is_reverse:
                continue

            # if overlap
            boolean, overlapLen = gRanges.overlap(beg, end, alignmentObj.reference_start, alignmentObj.reference_end)

            # print(strand, alignmentObj.is_reverse, boolean, alignmentObj.query_name, overlapLen, alignmentObj.reference_start, alignmentObj.reference_end, overlapLen/(alignmentObj.reference_end-alignmentObj.reference_start))

            ## Append alignment
            if boolean:
                ALT.append(alignmentObj.query_name)

    # Get unique values
    ALT = list(set(ALT))
    
    return ALT

def output_VAFs(bam, normalBam, targetBins, outDir):
    '''
    Estimate REF and ALT counts from a given list of regions and write them to a file
    
    Input:
    1. bam: tumour bam file
    2. normalBam: matched normal bam file
    3. targetBins: bed file with insertion coordinated (exact TSD coordinates needed)
    4. outDir: output directory 
    
    Output:
    No output but a file outDir + '/illumina_vafs.tsv'
    '''

    # Open output file
    outFilePath = outDir + '/illumina_vafs_RG.tsv'
    outFile = open(outFilePath, 'w')
    row = "#chrom1\tstart1\tend1\tchrom2\tstart2\tend2\tname\tstrand1\tstrand2\tmVAF1\tmVAF2\tmALT\tALT_delta\tREF1\tREF2\tbkp\tnoise\tMAPQ\tVAF\tREF\tALT\tMAPQ_normal\tREF_normal\tALT_normal\tREF_ids\tALT_ids\tREF_ids_normal\tALT_ids_normal\tbkp2\tnoise2\tMAPQ2\tVAF2\tREF2\tALT2\tMAPQ_normal2\tREF_normal2\tALT_normal2\tREF_ids2\tALT_ids2\tREF_ids_normal2\tALT_ids_normal2\n"
    outFile.write(row)
    # Read input bins
    BED = formats.BED()
    BED.read(targetBins, 'List', None)

    for line in BED.lines:
        
        ## Get variables
        ref = line.ref
        beg = line.beg if line.beg != line.end else line.beg-1
        end = line.end if line.beg != line.end else line.end+1

        ref2 = line.optional['chrom2']
        beg2 = int(line.optional['start2'])
        end2 = int(line.optional['end2'])

        id = line.optional['name']

        strand = line.optional['strand1']
        strand2 = line.optional['strand2']

        rowBeg = "\t".join([str(i) for i in [ref, beg, end, ref2, beg2, end2, id, strand, strand2]])
        
        ## Define bkps
        ref, bkp = determine_bkps(ref, beg, end, strand, bam)
        ref2, bkp2 = determine_bkps(ref2, beg2, end2, strand2, bam)

        if bkp:

            ## Get tumour counts
            counts = counts_bin(ref, bkp, strand, bam)
            counts.get_readCounts()

            ## Get normal counts
            if normalBam:
                
                countsNormal = counts_bin(ref, bkp, strand, normalBam)
                countsNormal.get_readCounts()
                region_noise = normal_noise(ref, bkp, bkp, normalBam)

        if bkp2:

            ## Get tumour counts
            counts2 = counts_bin(ref2, bkp2, strand2, bam)
            counts2.get_readCounts()

            ## Get normal counts
            if normalBam:
                
                countsNormal2 = counts_bin(ref2, bkp2, strand2, normalBam)
                countsNormal2.get_readCounts()
                region_noise2 = normal_noise(ref2, bkp2, bkp2, normalBam)

        if bkp and bkp2:
            
            # correct VAF using max ALT value
            nbALT1 = counts.cALT
            nbALT2 = counts2.cALT
            nbALT_delta = abs(nbALT1 - nbALT2)
            mALT = max([nbALT1, nbALT2])

            cVAF1 = mALT/(counts.cREF + mALT)
            cVAF2 = mALT/(counts2.cREF + mALT)

            ## when DUP A B > L1 > B C --> AB and BC intersections mantained
            # the RG creates REF reads too. So counts.cREF is indead counts.cREF - ALT reads. Total reads would be then counts.cREF - mALT + mALT = counts.cREF
            if id == 'DUP':
                if counts.cREF > 0:
                    cVAF1 = mALT/(counts.cREF) if mALT/(counts.cREF) < 1 else 1

                if counts2.cREF > 0:
                    cVAF2 = mALT/(counts2.cREF) if mALT/(counts2.cREF) < 1 else 1

            vafs = [str(round(cVAF1, 4)), str(round(cVAF2, 4)), str(mALT), str(nbALT_delta), str(counts.cREF), str(counts2.cREF)]

            if normalBam:

                # Write to output file
                call = [rowBeg] + vafs + [str(round(region_noise, 2)), str(round(counts.mapq, 2)), str(round(counts.VAF, 4)), str(counts.cREF), str(counts.cALT), str(round(countsNormal.mapq, 2)), str(countsNormal.cREF), str(countsNormal.cALT), str(counts.REF_ids), str(counts.ALT_ids), str(countsNormal.REF_ids), str(countsNormal.ALT_ids), str(bkp2), str(round(region_noise2, 2)), str(round(counts2.mapq, 2)), str(round(counts2.VAF, 4)), str(counts2.cREF), str(counts2.cALT), str(round(countsNormal2.mapq, 2)), str(countsNormal2.cREF), str(countsNormal2.cALT), str(counts2.REF_ids), str(counts2.ALT_ids), str(countsNormal2.REF_ids), str(countsNormal2.ALT_ids)]
                row = "\t".join(call) + "\n"
                outFile.write(row)

            else:
                
                # Write to output file
                call = [rowBeg] + vafs + [str(None), str(round(counts.mapq, 2)), str(round(counts.VAF, 4)), str(counts.cREF), str(counts.cALT), str(None), str(None), str(None), str(counts.REF_ids), str(counts.ALT_ids), str(None), str(None), str(bkp2), str(None), str(round(counts2.mapq, 2)), str(round(counts2.VAF, 4)), str(counts2.cREF), str(counts2.cALT), str(None), str(None), str(None), str(counts2.REF_ids), str(counts2.ALT_ids), str(None), str(None)]
                row = "\t".join(call) + "\n"
                outFile.write(row)

        else:
            
            # Write to output file
            call = [rowBeg, str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None)]
            row = "\t".join(call) + "\n"
            outFile.write(row)

    # close file
    outFile.close()


## 4. Define classes
##########################
class counts_bin():
    '''
    Contain a set of events of the same type in a genomic bin
    '''
    def __init__(self, ref, bkp, strand, bam, buffer = 200):

        ## General:
        self.ref = ref
        self.strand = strand
        self.bkp = bkp

        if strand == "+":
            self.beg = bkp - buffer 
            self.end = bkp
        
        elif strand == "-":
            self.beg = bkp 
            self.end = bkp + buffer

        self.bam = bam

        ## ALT/REF counts:
        self.VAF = None
        self.cREF = None
        self.cALT = None

        ## ALT/REF read ids:
        self.REF_ids = None
        
        ## Bin MAPQ:
        self.mapq = self.mean_mapq()

    def get_readCounts(self, minMAPQ = 30, offset = 325):
        '''
        Offset = buffer for discordants and REF pairs outside TSD. Median fragment size ~ 450 bp +/- 100 bp / 2 ~ 325 bp 
        '''

        ## Open BAM file for reading
        bamFile = pysam.AlignmentFile(self.bam, "rb")

        ## Get REF read ids
        iterator = bamFile.fetch(self.ref, self.beg, self.end+offset)
        REF = ref_reads(self.beg, self.end, self.bkp, iterator, self.strand, minMAPQ)
        
        ## Get ALT read ids
        iterator = bamFile.fetch(self.ref, self.beg, self.end+offset)
        ALT = alt_reads(self.beg, self.end, self.bkp, iterator, self.strand)
        
        # Get ALT counts
        self.cALT = len(list(set(ALT)))
        
        # Get REF counts
        REF = list(set(REF)-set(ALT)) # discard from REF all reads that have been found as ALT by any mean
        self.cREF = len(REF)
        
        # Estimate VAF
        total = self.cREF + self.cALT
        self.VAF = self.cALT / total if total else 0
        
        # read ids to string
        self.REF_ids = ','.join(REF)
        self.ALT_ids = ','.join(ALT)
                    
    def mean_mapq(self, buffer = 100):
        '''
        Estimate mean MAPQ in a genomic interval

        Input:
            1. ref: target reference
            2. beg: interval begin position
            3. end: interval end position
            4. bam: pysam filehandler for bam file 

        Output:
            1. bin_mapq: mean mapping quality in region
        '''
        ## 1. Collect alignments in the input interval
        bamFile = pysam.AlignmentFile(self.bam, "rb")
        iterator = bamFile.fetch(self.ref, self.beg-buffer, self.end+buffer)

        ## 2. Select only those alignments corresponding to the target reads 
        mapqs = []
        [mapqs.append(alignment.mapping_quality) for alignment in iterator]
        bin_mapq = np.mean(mapqs) if mapqs != [] else "NA"
        
        return bin_mapq


## 5. Run program
######################
output_VAFs(bam, normalBam, targetBins, outDir)


