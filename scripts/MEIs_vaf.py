#!/bin/python

'''
Script to estimate VAFs of MEIs. Counts REF, ALT_FWD and ALT_RV in all entries of a bed file, given a bam file.
Output is written in outDir + '/illumina_vafs.tsv'

'''

##  DEPENDENCIES ##
# External
import argparse

from numpy.testing._private.utils import build_err_msg
import pysam
import numpy as np
import sys
from collections import Counter
sys.path.append('../GAPI')

# Internal
from GAPI import formats


## 1. Define parser
######################
parser = argparse.ArgumentParser()
parser.add_argument('bam', help='Bam file')
parser.add_argument('targetBins', help='Bed file with Trafic TSD bkps')
parser.add_argument('outDir', help='Output directory')
parser.add_argument('--normalBam', default=None, dest='normalBam', help='Matched normal bam file. If provided MEIGA will run in PAIRED mode')


## 2. Parse user's input
##########################
args = parser.parse_args()
targetBins = args.targetBins
bam = args.bam
normalBam = args.normalBam
outDir = args.outDir


## 3. Define functions
##########################
def normal_noise(ref, beg, end, normalBam, buffer = 100):
    '''
    Estimate noisy reads ratio in region
    Input:
        1. ref
        2. beg
        3. end
        4. normalBam
        5. buffer  

    Output:
        1. noise: Ratio of noisy alignments / total alignments. SMS, clippings and discordant events are considered noisy
    '''
    # Initialize counts
    noisy_reads = 0
    total_reads = 0
    noise = None
    
    ## Open BAM file for reading in region
    bamFile = pysam.AlignmentFile(normalBam, "rb")
    iterator = bamFile.fetch(ref, beg-buffer, end+buffer)
    
    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue    

        ## Count total reads
        total_reads += 1

        ## Count SMS reads and clippings
        firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
        lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]
        
        if (firstOperation == 4) or (firstOperation == 5) or (lastOperation == 4) or (lastOperation == 5):
            if firstOperationLen >= 3 or lastOperationLen >= 3:
                noisy_reads += 1
        
        ## Count not proper pairs
        elif not alignmentObj.is_proper_pair:
            noisy_reads += 1

    ## Estimate noise percentage:
    if total_reads > 0:
        noise = noisy_reads/total_reads

    return noise


def determine_bkps(ref, beg, end, bam, buffer = 30):
    '''
    Search for bkp in a given region in bam
    Input:
        1. ref
        2. beg
        3. end
        4. bam
        5. buffer  

    Output:
        1. ref
        2. plus_bkp
        3. minus_bkp
        4. TSD
    '''
    # Initialize counts
    left_clipping = []
    right_clipping = []
    plus_bkp = None
    minus_bkp = None
    
    ## Open BAM file for reading in region
    bamFile = pysam.AlignmentFile(bam, "rb")
    iterator = bamFile.fetch(ref, beg-buffer, end+buffer)
    
    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue    

        ## Discard SMS reads (reads with CIGAR #S#M#S)
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation = alignmentObj.cigartuples[-1][0]
        if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):
            continue

        ## Determine clipping
        if (firstOperation == 4) or (firstOperation == 5):
            left_clipping.append(alignmentObj.reference_start)

        elif (lastOperation == 4) or (lastOperation == 5):
            right_clipping.append(alignmentObj.reference_end)

    if right_clipping:        
        plus_bkp = Counter(right_clipping).most_common()[0][0]
    
    if left_clipping:
        minus_bkp = Counter(left_clipping).most_common()[0][0]

    ## Determine if there is TSD
    if plus_bkp and minus_bkp:
        
        TSD = True if plus_bkp > minus_bkp else False
        bkpA = minus_bkp if TSD else plus_bkp
        bkpB = plus_bkp if TSD else minus_bkp
        
        if bkpA == bkpB: bkpB = bkpB+1

    else:
        
        TSD = None
        
        if beg == end: 
            bkpA = beg - 1
            bkpB = end + 1
            
        else:
            bkpA = beg
            bkpB = end
    
    return ref, bkpA, bkpB, TSD

def ref_reads(beg, end, iterator, TSD, minMAPQ, minAS = 120):
    '''
    Get reference supporting read ids from bam iterator
    Input:
        1. beg: TSD first bkp
        2. end: TSD second bkp
        3. iterator: Bam file iterator from TSD region
        4. TSD: Boolean indicating if MEI has TSD or not
        5. minMAPQ: MAPQ threshold
    Output:
        1. REF: List of reference supporting read names
    '''
    # Initialize counts
    REF = []
    FAIL = [] # Reads finishing on TSD (You cannot tell whether they are REF or ALT), SMS, low MAPQ... 

    # For each read alignment
    for alignmentObj in iterator:
        
        ## If mate already marked as FAIL
        if alignmentObj.query_name in FAIL:
            continue
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            FAIL.append(alignmentObj.query_name)
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            FAIL.append(alignmentObj.query_name)
            continue
        
        ## Discard aligments with MAPQ or Alignment Score < threshold 
        MAPQ = int(alignmentObj.mapping_quality)
        AS = int(alignmentObj.get_tag('AS'))
        if MAPQ < minMAPQ or AS < minAS:
            FAIL.append(alignmentObj.query_name)
            continue
        
        # Discard SMS reads (reads with CIGAR #S#M#S) and clippings
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation= alignmentObj.cigartuples[-1][0]
        if ((firstOperation == 4) or (firstOperation == 5)) or ((lastOperation == 4) or (lastOperation == 5)):
            FAIL.append(alignmentObj.query_name)
            continue
        
        ## Select proper pairs
        if not alignmentObj.is_proper_pair:
            FAIL.append(alignmentObj.query_name)
            continue
        
        # If there is TSD:
        if TSD:
            
            ## Discard fwd reads that start after the start of the TSD. You can not tell whether they are REF or ALT
            # buffer of 1 added since clippings of 1 are not marked as clippings
            #      |     |
            #       <----* # not marked
            if not alignmentObj.is_reverse and alignmentObj.reference_start >= beg-1:
                FAIL.append(alignmentObj.query_name)
                continue
            
            ## Discard reverse reads that end before the end of the TSD
            if alignmentObj.is_reverse and alignmentObj.reference_end <= end+1:
                FAIL.append(alignmentObj.query_name)
                continue
            
        else:
            
            ## Discard fwd reads that start after the start of the TSD. You can not tell whether they are REF or ALT
            # buffer of 1 added since clippings of 1 are not marked as clippings
            #      |     |
            #       <----* # not marked
            if not alignmentObj.is_reverse and alignmentObj.reference_start >= end-1:
                FAIL.append(alignmentObj.query_name)
                continue
            
            ## Discard reverse reads that end before the end of the TSD
            if alignmentObj.is_reverse and alignmentObj.reference_end <= beg+1:
                FAIL.append(alignmentObj.query_name)
                continue
                    
        ## Append alignment
        REF.append(alignmentObj.query_name)
    
    # Get unique values
    REF = list(set(REF)-set(FAIL))
    
    return REF

def alt_reads(beg, end, iterator, TSD, buffer = 2):
    '''
    Get variant supporting (ALT) read ids from bam iterator
    Input:
        1. beg: TSD first bkp
        2. end: TSD second bkp
        3. iterator: Bam file iterator from TSD region
    Output:
        1. ALT_FWD: List of ALT read names supporting variant forward end
        2. ALT_RV: List of ALT read names supporting variant reverse end
    '''
    # Initialize counts
    ALT_RV = []
    ALT_FWD = []

    # For each read alignment
    for alignmentObj in iterator:
        
        ## Unmapped or query sequence available
        if alignmentObj.is_unmapped or alignmentObj.query_sequence == None:
            continue
        
        ## Duplicates
        if alignmentObj.is_duplicate == True:
            continue
        
        # Discard SMS reads (reads with CIGAR #S#M#S)
        firstOperation = alignmentObj.cigartuples[0][0]
        lastOperation = alignmentObj.cigartuples[-1][0]      
        if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):
            continue
        
        ## Sort split reads in FWD and RV according to where they have the clipping
        clipping = (firstOperation == 4 or firstOperation == 5 or lastOperation == 4 or lastOperation == 5)
        
        ## Discard proper pairs without clipping
        if alignmentObj.is_proper_pair and not clipping:
            continue
        
        # if clipping 
        if clipping:
            
            # if left clipping
            if (firstOperation == 4 or firstOperation == 5):
                
                # check it's in interval
                if beg-buffer <= alignmentObj.reference_start <= end+buffer:
                    ALT_RV.append(alignmentObj.query_name)
                
            # if right clipping
            elif (lastOperation == 4 or lastOperation == 5):
                
                # check it's in interval
                if beg-buffer <= alignmentObj.reference_end <= end+buffer:
                    ALT_FWD.append(alignmentObj.query_name)

        # if discordants
        else:
            
            if TSD:
                
                # if forward read and read end <= TSD end
                # forward discordant: -------->| (end)
                if not alignmentObj.is_reverse and alignmentObj.reference_end <= end:
                    ALT_FWD.append(alignmentObj.query_name)
                    
                # if reverse read and read start >= TSD start
                # reverse discordant: (start)| <-------- 
                elif alignmentObj.is_reverse and alignmentObj.reference_start >= beg:
                    ALT_RV.append(alignmentObj.query_name)
            
            else:
                
                # if forward read and read end <= TSD end
                # forward discordant: -------->| (end)
                if not alignmentObj.is_reverse and alignmentObj.reference_end <= beg:
                    ALT_FWD.append(alignmentObj.query_name)
                    
                # if reverse read and read start >= TSD start
                # reverse discordant: (start)| <-------- 
                elif alignmentObj.is_reverse and alignmentObj.reference_start >= end:
                    ALT_RV.append(alignmentObj.query_name)                
                
    # Get unique values
    ALT_FWD = list(set(ALT_FWD))
    ALT_RV = list(set(ALT_RV))
    
    return ALT_FWD, ALT_RV

def output_VAFs(bam, normalBam, targetBins, outDir):
    '''
    Estimate REF, ALT_FWD and ALT_RV counts from a given list of regions and write them to a file
    
    Input:
    1. bam: tumour bam file
    2. normalBam: matched normal bam file
    3. targetBins: bed file with insertion coordinated (exact TSD coordinates needed)
    4. outDir: output directory 
    
    Output:
    No output but a file outDir + '/illumina_vafs.tsv'
    '''

    # Open output file
    outFilePath = outDir + '/illumina_vafs.tsv'
    outFile = open(outFilePath, 'w')
    row = "#ref\tbeg\tend\tbkpA\tbkpB\tnoise\tTSD\tMAPQ\tVAF\tREF\tALT\tALT_FWD\tALT_RV\tMAPQ_normal\tREF_normal\tALT_FWD_normal\tALT_RV_normal\tREF_ids\tALT_FWD_ids\tALT_RV_ids\tREF_ids_normal\tALT_FWD_ids_normal\tALT_RV_ids_normal\n"
    outFile.write(row)
    # Read input bins
    BED = formats.BED()
    BED.read(targetBins, 'List', None)

    for line in BED.lines:
        
        ## Get variables
        ref = line.ref
        beg = line.beg if line.beg != line.end else line.beg-1
        end = line.end if line.beg != line.end else line.end+1
        
        ## Define bkps
        ref, bkpA, bkpB, TSD = determine_bkps(ref, beg, end, bam)

        if bkpA and bkpB:

            ## Get tumour counts
            counts = counts_bin(ref, bkpA, bkpB, TSD, bam)
            counts.get_readCounts()

            ## Get normal counts
            if normalBam:
                
                countsNormal = counts_bin(ref, bkpA, bkpB, TSD, normalBam)
                countsNormal.get_readCounts()
                region_noise = normal_noise(ref, bkpA, bkpB, normalBam)
            
                # Write to output file
                call = [str(ref), str(line.beg), str(line.end), str(bkpA), str(bkpB), str(region_noise), str(TSD), str(counts.mapq), str(counts.VAF), str(counts.cREF), str(counts.cALT), str(counts.cALT_FWD), str(counts.cALT_RV), str(countsNormal.mapq), str(countsNormal.cREF), str(countsNormal.cALT_FWD), str(countsNormal.cALT_RV), str(counts.REF_ids), str(counts.ALT_FWD_ids), str(counts.ALT_RV_ids), str(countsNormal.REF_ids), str(countsNormal.ALT_FWD_ids), str(countsNormal.ALT_RV_ids)]
                row = "\t".join(call) + "\n"
                outFile.write(row)

            else:
                
                # Write to output file
                call = [str(ref), str(line.beg), str(line.end), str(bkpA), str(bkpB), str(None), str(TSD), str(counts.mapq), str(counts.VAF), str(counts.cREF), str(counts.cALT), str(counts.cALT_FWD), str(counts.cALT_RV), str(None), str(None), str(None), str(counts.REF_ids), str(counts.ALT_FWD_ids), str(counts.ALT_RV_ids), str(None), str(None), str(None), str(None)]
                row = "\t".join(call) + "\n"
                outFile.write(row)

        else:
            
            # Write to output file
            call = [str(ref), str(line.beg), str(line.end), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None), str(None)]
            row = "\t".join(call) + "\n"
            outFile.write(row)

    # close file
    outFile.close()


## 4. Define classes
##########################
class counts_bin():
    '''
    Contain a set of events of the same type in a genomic bin
    '''
    def __init__(self, ref, beg, end, TSD, bam):

        ## General:
        self.ref = ref
        self.beg = beg
        self.end = end
        self.TSD = TSD
        self.bam = bam

        ## ALT/REF counts:
        self.VAF = None
        self.cREF = None
        self.cALT = None
        self.cALT_FWD = None
        self.cALT_RV = None

        ## ALT/REF read ids:
        self.REF_ids = None
        self.ALT_FWD_ids = None
        self.ALT_RV_ids = None
        
        ## Bin MAPQ:
        self.mapq = self.mean_mapq()

    def get_readCounts(self, minMAPQ = 30, offset = 325):
        '''
        Offset = buffer for discordants and REF pairs outside TSD. Median fragment size ~ 450 bp +/- 100 bp / 2 ~ 325 bp 
        
        '''
        ## Open BAM file for reading
        bamFile = pysam.AlignmentFile(self.bam, "rb")

        ## Get REF read ids
        iterator = bamFile.fetch(self.ref, self.beg-offset, self.end+offset)
        REF = ref_reads(self.beg, self.end, iterator, self.TSD, minMAPQ)
        
        ## Get ALT read ids
        iterator = bamFile.fetch(self.ref, self.beg-offset, self.end+offset)
        ALT_FWD, ALT_RV = alt_reads(self.beg, self.end, iterator, self.TSD)
        
        # Get ALT counts
        self.cALT_FWD = len(ALT_FWD)
        self.cALT_RV = len(ALT_RV)
        self.cALT = max(self.cALT_FWD, self.cALT_RV)
        
        # Get REF counts
        REF = list(set(REF)-set(ALT_FWD+ALT_RV)) # discard from REF all reads that have been found as ALT by any mean
        self.cREF = len(REF)
        
        # Estimate VAF
        total = self.cREF + self.cALT
        self.VAF = self.cALT / total if total else 0
        
        # read ids to string
        self.REF_ids = ','.join(REF)
        self.ALT_FWD_ids = ','.join(ALT_FWD)
        self.ALT_RV_ids = ','.join(ALT_RV)
                    
    def mean_mapq(self, buffer = 100):
        '''
        Estimate mean MAPQ in a genomic interval

        Input:
            1. ref: target reference
            2. beg: interval begin position
            3. end: interval end position
            4. bam: pysam filehandler for bam file 

        Output:
            1. bin_mapq: mean mapping quality in region
        '''
        ## 1. Collect alignments in the input interval
        bamFile = pysam.AlignmentFile(self.bam, "rb")
        iterator = bamFile.fetch(self.ref, self.beg-buffer, self.end+buffer)

        ## 2. Select only those alignments corresponding to the target reads 
        mapqs = []
        [mapqs.append(alignment.mapping_quality) for alignment in iterator]
        bin_mapq = np.mean(mapqs) if mapqs != [] else "NA"
        
        return bin_mapq


## 5. Run program
######################
output_VAFs(bam, normalBam, targetBins, outDir)


