#!/bin/python

'''
Script to estimate percentage of low MAPQ reads and SMS reads in all entries of a bed file, given a bam file.
Output is written in outDir + '/targetBins_MAPQ_SMS.tsv'

buffer used 100 bp
'''

##  DEPENDENCIES ##
# Internal
from GAPI import formats
from GAPI import stats

# External
import argparse
import pysam


## 1. Define parser
######################
parser = argparse.ArgumentParser()
parser.add_argument('targetBins', help='Genomic windows to be inspected')
parser.add_argument('bam', help='Bam file')
parser.add_argument('outDir', help='Output directory')


## 2. Parse user's input
##########################
args = parser.parse_args()
targetBins = args.targetBins
bam = args.bam
outDir = args.outDir


## 3. Define fucntions
##########################
def area_BED(targetBins, bam, outDir):
    '''
    Estimate percentage of low MAPQ reads and SMS reads in all entries of a bed file
    '''
    # Open output file
    outFilePath = outDir + '/targetBins_MAPQ_SMS.tsv'
    outFile = open(outFilePath, 'w')
    row = "#ref \t beg \t end \t percLowMAPQ \t percSMS \n"
    outFile.write(row)
    
    # Read input bins
    BED = formats.BED()
    BED.read(targetBins, 'List', None)
    
    for line in BED.lines:
        
        # Estimate MAPQ and SMS read percentage
        percMAPQ, percSMSReads = area(line.ref, line.beg, line.end, bam)
        
        # Write to output file
        call = [str(line.ref), str(line.beg), str(line.end), str(percMAPQ), str(percSMSReads)]
        row = "\t".join(call) + "\n"
        outFile.write(row)
        
    outFile.close()


def area(ref, beg, end, bam):
    '''
    Estimate percentage of low MAPQ reads and SMS reads in a given region
    '''
    
    minReadsRegionMQ = 1
    
    ## Set region coordinates
    buffer = 25
    binBeg = beg - buffer
    binEnd = end + buffer

    # Set counts to 0
    lowMAPQ = 0
    SMSReads = 0
    nbReads = 0

    ## Open BAM file for reading
    bamFile = pysam.AlignmentFile(bam, "rb")

    ## Extract alignments
    iterator = bamFile.fetch(ref, binBeg, binEnd)

    for alignmentObj in iterator:
        
        if alignmentObj.cigartuples != None:
        
            # Check if aligment pass minimum mapq for reads within the cluster region
            passMAPQ = areaMAPQ(alignmentObj, minReadsRegionMQ)

            # If it doesnt pass, add 1 to the counts of low mapping quality reads within the cluster region
            if passMAPQ == False:
                lowMAPQ += 1

            # Check if aligment is mapped this way: Soft Match Soft (SMS)
            SMSRead = areaSMS(alignmentObj)
            
            # If it is mapped SMS, add 1 to the counts of SMS reads within the cluster region
            if SMSRead == True:
                SMSReads += 1
                
            # Count total number of reads in the region
            nbReads += 1
    
    ## Calculate percentages
    percMAPQ = stats.fraction(lowMAPQ, nbReads)
    percSMSReads = stats.fraction(SMSReads, nbReads)
    
    return percMAPQ, percSMSReads


def areaMAPQ(alignmentObj, minReadsRegionMQ):
    '''
    Check if the MAPQ of a read pass the minReadsRegionMQ threshold

    Input:
        1. alignmentObj
        2. minReadsRegionMQ
    Output:
        1. percMAPQFilter -> boolean: True if the cluster pass the filter, False if it doesn't
    '''

    MAPQ = int(alignmentObj.mapping_quality)

    if MAPQ > int(minReadsRegionMQ):
        passMAPQ = True
    else:
        passMAPQ = False

    return passMAPQ


def areaSMS(alignmentObj):
    '''
    Check if aligment is mapped this way: Soft Match Soft (SMS)

    Input:
        1. alignmentObj
        2. maxRegionSMS
    Output:
        1. percSMSReadsFilter -> boolean: True if the cluster pass the filter, False if it doesnt
    '''

    SMSRead = False

    # Select first and last operation from cigar to search for clipping
    firstOperation, firstOperationLen = alignmentObj.cigartuples[0]
    lastOperation, lastOperationLen = alignmentObj.cigartuples[-1]
    ## Clipping >= X bp at the left
    #  Note: soft (Operation=4) or hard clipped (Operation=5)     
    if ((firstOperation == 4) or (firstOperation == 5)) and ((lastOperation == 4) or (lastOperation == 5)):
        SMSRead = True
        
    #if ((lastOperation == 4) or (lastOperation == 5)) and ((firstOperation != 4) and (firstOperation != 5)):
        #SMSRead = True

    return SMSRead


## 4. Run program
######################
area_BED(targetBins, bam, outDir)

